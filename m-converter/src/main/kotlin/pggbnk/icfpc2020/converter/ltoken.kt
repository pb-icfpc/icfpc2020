package pggbnk.icfpc2020.converter

fun parseLTokens(source: String): List<LToken> {
    val tokensSrcStrings = source.split("\\s+".toRegex())
    return tokensSrcStrings.map { LToken.parse(it) }.toList()
}

sealed class LToken(val srcToken: String) {
    companion object {
        fun parse(srcToken: String): LToken {
            collectedObjects.forEach {
                if (it.srcToken == srcToken) {
                    return@parse it
                }
            }

            if (srcToken.matches("^-?\\d++$".toRegex())) {
                return Numeral(srcToken)
            }

            return Literal(srcToken)
        }

        private lateinit var aCollectedObjects: List<LToken>

        private val collectedObjects: List<LToken>
            get() {
                if (::aCollectedObjects.isInitialized) {
                    return aCollectedObjects
                }
                aCollectedObjects = LToken::class.nestedClasses.mapNotNull {
                    return@mapNotNull it.objectInstance as? LToken
                }
                return aCollectedObjects
            }
    }

    class Literal(value: String): LToken(value)

    class Numeral(value: String): LToken(value)

    object Assignment : LToken("=")

    abstract class Function(name: String): LToken(name)

    object F_AP : Function("ap")

    object F_INC : Function("inc")

    object F_DEC : Function("dec")

    object F_NEG : Function("neg")

    object F_PWR2 : Function("pwr2")

    object F_ADD : Function("add")

    object F_MUL : Function("mul")

    object F_DIV : Function("div")

    object F_EQ : Function("eq")

    object F_LT : Function("lt")

    object F_IF0 : Function("if0")

    object F_MOD : Function("mod")

    object F_DEM : Function("dem")

    object F_SEND : Function("send")

    object F_INTERACT : Function("interact")

    object F_DRAW : Function("draw")

    object F_CHECKERBOARD : Function("checkerboard")

    object F_MULTIPLEDRAW : Function("multipledraw")

    object F_S : Function("s")

    object F_C : Function("c")

    object F_B : Function("b")

    object F_T : Function("t")

    object F_F : Function("f")

    object F_I : Function("i")

    object F_CONS : Function("cons")

    object F_VEC : Function("vec")

    object F_CAR : Function("car")

    object F_CDR : Function("cdr")

    object F_TOLIST : Function("pggbnk.tohlist")

    //TODO: this can be a literal and a function??
    object NIL : LToken("nil")

    object F_ISNIL : Function("isnil")

    override fun toString(): String {
        return "[${this::class.simpleName}, $srcToken]"
    }
}
