@file:Suppress("ClassName")

package pggbnk.icfpc2020.converter

import java.io.BufferedReader
import java.io.File
import java.lang.StringBuilder
import java.math.BigInteger
import java.util.concurrent.SynchronousQueue

const val TEST_SERVER = "https://icfpc2020-api.testkontur.ru"

const val PICTURE_NAME = "last-1"

@Suppress("SpellCheckingInspection")
const val PGGBNK_API_KEY = "afa320d4fc534d97adfc32c2734ecd3c"

object GlobalRuntime {
    private var initialized = false

    lateinit var playerKey: String
        private set

    lateinit var http: HttpIO
        private set

    fun initRuntime(urlString: String, apiKey: String, playerKey: String) {
        if (initialized) {
            error("Already initialized")
        }
        this.initialized = true
        this.playerKey = playerKey
        this.http = HttpIO(urlString, apiKey)
    }

    fun initTestRuntime() {
        initRuntime(TEST_SERVER, PGGBNK_API_KEY, "NO_PLAYER_KEY")
    }
}

sealed class MainCommand {
    class InterpretLine(val line: String) : MainCommand()

    class ClickGalaxy(val x: Int, val y: Int) : MainCommand()
}

val Q = SynchronousQueue<MainCommand>(true)
val Q_IREPLY = SynchronousQueue<Unit>(true)
val Q_CGREPLY = SynchronousQueue<Unit>(true)

fun main(args: Array<String>) {
    if (args.size >= 2) {
        val server = args[0]
        val playerKey = args[1]
        GlobalRuntime.initRuntime(server, PGGBNK_API_KEY, playerKey)
    } else {
        GlobalRuntime.initTestRuntime()
    }

    val galaxyPath = "galaxy/galaxy.txt"
    //val galaxyPath = "galaxy/galaxy3.txt"
    val galaxyFile = File(galaxyPath)

    val varsStorage = VarsStorageReal()

    showUI()

    readGalaxyFile(varsStorage, galaxyFile)
    interpretLine(varsStorage, "$I_LCMDR_VAR = galaxy")

    startInterpreter()

    dispatchMainCommands(varsStorage)
}

private fun dispatchMainCommands(varsStorage: VarsStorageReal) {
    wait_commands@ while(true) {
        when (val cmd = Q.take()) {
            is MainCommand.InterpretLine -> {
                val result = interpretLine(varsStorage, cmd.line)
                Q_IREPLY.put(Unit)
                when (result) {
                    InterpretResult.Quit -> break@wait_commands
                    else -> continue@wait_commands
                }
            }
            is MainCommand.ClickGalaxy -> {
                val x = cmd.x
                val y = cmd.y
                clickGalaxy(varsStorage, x, y)
                Q_CGREPLY.put(Unit)
            }
        }
    }
}

fun onGalaxyClick(x: Int, y: Int) {
    Q.put(MainCommand.ClickGalaxy(x, y))
    Q_CGREPLY.take()
}

private fun startInterpreter() {
    fun loop() {
        val reader = BufferedReader(System.`in`.reader())
        while (true) {
            print(">>>> ")
            val inputLine = reader.readLine()
            Q.put(MainCommand.InterpretLine(inputLine))
            Q_IREPLY.take() // waiting for confirmation
        }
    }
    Thread {
        loop()
    }.start()
}

private fun clickGalaxy(varsStorage: VarsStorageReal, x: Int, y: Int) {
    println("Clicking galaxy at ($x, $y) ...")
    val line = "ap ap call_gx ap car $I_LCMDR_VAR ap ap vec $x $y"
    try {
        interpretLine(varsStorage, line)
    } catch (t: Throwable) {
        System.err.println("Error while clicking galaxy")
        t.printStackTrace()
    }
    println("Galaxy clicked!!!!!!")
}

enum class InterpretResult {
    Nothing,
    Quit
}

const val I_LCMDR_VAR = ":LCMDR"

fun interpretLine(varsStorage: VarsStorageReal, line: String): InterpretResult {
    if ("qqq" == line) {
        return InterpretResult.Quit
    }
    val node = try {
        parseLine(varsStorage, line)
    } catch (t: Throwable) {
        System.err.println("Error while parsing line '$line'")
        t.printStackTrace()
        return InterpretResult.Nothing
    }
    try {
        val result = node.evalToTheMax()
        varsStorage.setVar(I_LCMDR_VAR, result)
    } catch (t: Throwable) {
        System.err.println("Error while evaluating line '$line'")
        t.printStackTrace()
        return InterpretResult.Nothing
    }
    return InterpretResult.Nothing
}

fun readGalaxyFile(varsStorage: VarsStorageReal, file: File) {
    println("Reading galaxy '${file.absolutePath}'")
    val lines = file.readLines()
    lines.forEachIndexed { lineNumber, line ->
        val humanLineNumber = lineNumber + 1
        println("Converting line $humanLineNumber")
        if (line.isBlank()) {
            println("Blank line")
        } else {
            parseLine(varsStorage, line, debug = true)
        }
        println()
    }
    println("Processed ${lines.size} lines.")
    println()
    println()

    val galaxy = varsStorage.findVar("galaxy")
    galaxy.evalToTheMax(print = true)
}

fun parseLine(varsStorage: VarsStorageReal, rawLine: String, debug: Boolean = false): ENode {
    val line = rawLine.trim()
    if (line.isEmpty()) {
        return ENode.NIL
    }

    val lTokens = parseLTokens(line)

    val tokenAssignToVar: LToken.Literal?
    val tokensExpression: List<LToken>
    if (lTokens.size >= 3 && lTokens[1] is LToken.Assignment) {
        tokenAssignToVar = (lTokens[0] as? LToken.Literal) ?: error("Bad assignment format")
        tokensExpression = lTokens.subList(2, lTokens.size)
    } else {
        tokenAssignToVar = null
        tokensExpression = lTokens
    }

    if (debug) {
        if (tokenAssignToVar != null) {
            println("VAR '${tokenAssignToVar.srcToken}'")
        }
        println(tokensExpression)
    }

    val expressionNode = parseExpression(varsStorage, tokensExpression)
    if (debug) {
        println(expressionNode.description)
    }
    return if (tokenAssignToVar != null) {
        varsStorage.setVar(tokenAssignToVar.srcToken, expressionNode)
        ENode.VAR(varsStorage, tokenAssignToVar.srcToken)
    } else {
        expressionNode
    }
}

fun parseExpression(varsStorage: VarsStorage, tokens: List<LToken>): ENode {
    val stack = mutableListOf<ENode>()
    fun pop(): ENode {
        if (stack.isEmpty()) {
            error("No nodes in parse stack")
        }
        return stack.removeAt(stack.lastIndex)
    }
    fun push(node: ENode) {
        stack.add(node)
    }

    for (i in tokens.lastIndex downTo 0) {
        val node = when (val currentToken = tokens[i]) {
            is LToken.Literal -> {
                ENode.VAR(varsStorage, currentToken.srcToken)
            }
            is LToken.Numeral -> {
                ENode.NUM(currentToken.srcToken)
            }
            is LToken.NIL -> {
                ENode.NIL
            }
            is LToken.Assignment -> {
                error("No node for assignment in expression")
            }
            is LToken.F_AP -> {
                val f = pop()
                val x = pop()
                ENode.AP(f, x)
            }
            is LToken.F_NEG -> {
                ENode.NEG
            }
            is LToken.F_INC -> {
                ENode.INC
            }
            is LToken.F_DEC -> {
                ENode.DEC
            }
            is LToken.F_PWR2 -> {
                ENode.PWR2
            }
            is LToken.F_ADD -> {
                ENode.ADD
            }
            is LToken.F_MUL -> {
                ENode.MUL
            }
            is LToken.F_DIV -> {
                ENode.DIV
            }
            is LToken.F_EQ -> {
                ENode.EQ
            }
            is LToken.F_LT -> {
                ENode.LT
            }
            is LToken.F_IF0 -> {
                ENode.IF0
            }
            is LToken.F_ISNIL -> {
                ENode.ISNIL
            }
            is LToken.F_C -> {
                ENode.C_COMBINATOR
            }
            is LToken.F_B -> {
                ENode.B_COMBINATOR
            }
            is LToken.F_S -> {
                ENode.S_COMBINATOR
            }
            is LToken.F_I -> {
                ENode.I_COMBINATOR
            }
            is LToken.F_CAR -> {
                ENode.CAR
            }
            is LToken.F_CDR -> {
                ENode.CDR
            }
            is LToken.F_CONS -> {
                ENode.CONS
            }
            is LToken.F_VEC -> {
                ENode.VEC
            }
            is LToken.F_T -> {
                ENode.T
            }
            is LToken.F_F -> {
                ENode.F
            }
            is LToken.F_MOD -> {
                ENode.MOD
            }
            is LToken.F_DEM -> {
                ENode.DEM
            }
            is LToken.F_SEND -> {
                ENode.SEND
            }
            is LToken.F_INTERACT -> {
                ENode.INTERACT
            }
            is LToken.F_TOLIST -> {
                ENode.TOHLIST
            }
            else -> {
                error("Unsupported LToken ${currentToken.srcToken}")
            }
        }
        push(node)
    }

    if (stack.size != 1) {
        error("Parsed expression should leave exactly one node")
    }

    return stack.first()
}

fun ENode.evalToTheMax(print: Boolean = false): ENode {
    if (print) {
        println("----EVALUATING NODE----")
        println("")
    }

    var evalNode = this
    while (true) {
        if (print) {
            println(evalNode.description)
            println(evalNode.toSource())
            println()
        }
        if (evalNode !is ENode.AP && evalNode !is ENode.VAR && evalNode !is ENode.AnonymousVar) {
            break
        }
        evalNode = evalNode.eval()
    }

    if (print) {
        println("-----------------------")
        println("")
    }

    return evalNode
}

sealed class ENode {
    companion object {
        private val BIG_CACHE = mutableMapOf<String, BigInteger>()

        private fun getBig(value: String): BigInteger {
            var cached = BIG_CACHE[value]
            if (cached == null) {
                cached = BigInteger(value)
                BIG_CACHE[value] = cached
            }
            return cached
        }
    }

    abstract class Function : ENode() {
        abstract val needsArgs: Int

        abstract fun call(args: List<ENode>): ENode
    }

    class AP(private val f: ENode, private val x: ENode) : ENode() {
        private var fEvalCache: Function? = null

        override fun eval(): ENode {
            if (fEvalCache == null) {
                fEvalCache = (f.evalToTheMax() as? Function) ?:
                    error("AP first arg did not evaluate to function\n--\n${f.evalToTheMax().description}\n--\n")
            }
            val fEval = fEvalCache!!
            return when {
                fEval.needsArgs <= 0 -> error("AP can't have functions with less then one arg")
                fEval.needsArgs == 1 -> {
                    fEval.call(listOf(x))
                }
                else -> {
                    Curried(fEval, x)
                }
            }
        }

        override val description: String
            get() = "[AP ${f.description} ${x.description}]"

        override fun toSource(): String = "${LToken.F_AP.srcToken} ${f.toSource()} ${x.toSource()}"

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onChildNode(f)
            f.visit(visitor)
            visitor.onChildNode(x)
            x.visit(visitor)
            visitor.onExitNode(this)
        }
    }

    class Curried(private val f: Function, private val arg: ENode) : Function() {
        private val argCache = ArrayList<ENode>(f.needsArgs).apply {
            add(arg)
        }

        private val ownArgs = 1

        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = f.needsArgs - ownArgs

        override fun call(args: List<ENode>): ENode {
            while (argCache.size > ownArgs) {
                argCache.removeAt(argCache.lastIndex)
            }
            for (i in args) {
                argCache.add(i)
            }
            if (f.needsArgs != argCache.size) {
                error("Bad number of args")
            }
            return f.call(argCache)
        }

        override val description: String
            get() = "[AP ${f.description} ${arg.description}]"

        override fun toSource(): String = "${LToken.F_AP.srcToken} ${f.toSource()} ${arg.toSource()}"

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onChildNode(f)
            f.visit(visitor)
            visitor.onChildNode(arg)
            arg.visit(visitor)
            visitor.onExitNode(this)
        }
    }

    object NEG : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val result = a0.bigValue.negate()
            return NUM(result)
        }

        override val description: String
            get() = "[NEG]"

        override fun toSource(): String = LToken.F_NEG.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object INC : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val result = a0.bigValue.add(BigInteger.ONE)
            return NUM(result)
        }

        override val description: String
            get() = "[INC]"

        override fun toSource(): String = LToken.F_INC.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object DEC : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val result = a0.bigValue.subtract(BigInteger.ONE)
            return NUM(result)
        }

        override val description: String
            get() = "[DEC]"

        override fun toSource(): String = LToken.F_DEC.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object PWR2 : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val result = BIG2.pow(a0.bigValue)
            return NUM(result)
        }

        override val description: String
            get() = "[PWR2]"

        override fun toSource(): String = LToken.F_PWR2.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object ADD : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 2

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val a1 = (args[1].evalToTheMax() as? NUM) ?: error("a1 must be num")
            val result = a0.bigValue.add(a1.bigValue)
            return NUM(result)
        }

        override val description: String
            get() = "[ADD]"

        override fun toSource(): String = LToken.F_ADD.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object MUL : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 2

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val a1 = (args[1].evalToTheMax() as? NUM) ?: error("a1 must be num")
            val result = a0.bigValue.multiply(a1.bigValue)
            return NUM(result)
        }

        override val description: String
            get() = "[MUL]"

        override fun toSource(): String = LToken.F_MUL.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object DIV : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 2

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val a1 = (args[1].evalToTheMax() as? NUM) ?: error("a1 must be num")
            val result = a0.bigValue.divide(a1.bigValue)
            return NUM(result)
        }

        override val description: String
            get() = "[DIV]"

        override fun toSource(): String = LToken.F_DIV.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object EQ : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 2

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val a1 = (args[1].evalToTheMax() as? NUM) ?: error("a1 must be num")
            return if (a0.bigValue == a1.bigValue) T else F
        }

        override val description: String
            get() = "[EQ]"

        override fun toSource(): String = LToken.F_EQ.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object LT : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 2

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val a1 = (args[1].evalToTheMax() as? NUM) ?: error("a1 must be num")
            return if (a0.bigValue < a1.bigValue) T else F
        }

        override val description: String
            get() = "[LT]"

        override fun toSource(): String = LToken.F_LT.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object IF0 : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 3

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = (args[0].evalToTheMax() as? NUM) ?: error("a0 must be num")
            val a1 = args[1]
            val a2 = args[2]
            return if (a0.bigValue == BigInteger.ZERO) a1 else a2
        }

        override val description: String
            get() = "[IF0]"

        override fun toSource(): String = LToken.F_IF0.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object ISNIL : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0].evalToTheMax()

            return if (a0 == NIL) T else F
        }

        override val description: String
            get() = "[ISNIL]"

        override fun toSource(): String = LToken.F_ISNIL.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object C_COMBINATOR : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 3

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = args[0]
            val a1 = args[1]
            val a2 = args[2]

            // ap ap ap c x0 x1 x2   =   (ap (ap x0 x2) x1)
            return AP(AP(a0, a2), a1)
        }

        override val description: String
            get() = "[C_CMB]"

        override fun toSource(): String = LToken.F_C.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object B_COMBINATOR : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 3

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]
            val a1 = args[1]
            val a2 = args[2]

            // ap ap ap b x0 x1 x2   =   (ap x0 (ap x1 x2))
            return AP(a0, AP(a1, a2))
        }

        override val description: String
            get() = "[B_CMB]"

        override fun toSource(): String = LToken.F_B.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object S_COMBINATOR : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 3

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]
            val a1 = args[1]
            val a2 = args[2]
            val a2Var = AnonymousVar(a2)

            // ap ap ap s x0 x1 x2 = (ap (ap x0 x2) (ap x1 x2))
            return AP(AP(a0, a2Var), AP(a1, a2Var))
        }

        override val description: String
            get() = "[S_CMB]"

        override fun toSource(): String = LToken.F_S.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object I_COMBINATOR : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]

            return a0
        }

        override val description: String
            get() = "[I_CMB]"

        override fun toSource(): String = LToken.F_I.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object CAR : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]

            //ap car x2  =  ap x2 t
            return AP(a0, T)
        }

        override val description: String
            get() = "[CAR]"

        override fun toSource(): String = LToken.F_CAR.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object CDR : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]

            //ap cdr x2   =   ap x2 f
            return AP(a0, F)
        }

        override val description: String
            get() = "[CDR]"

        override fun toSource(): String = LToken.F_CDR.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object T : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 2

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]
            val a1 = args[1]

            return a0
        }

        override val description: String
            get() = "[T]"

        override fun toSource(): String = LToken.F_T.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object F : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 2

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]
            val a1 = args[1]

            return a1
        }

        override val description: String
            get() = "[F]"

        override fun toSource(): String = LToken.F_F.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object CONS : ConsBase(LToken.F_CONS)

    object VEC : ConsBase(LToken.F_VEC)

    abstract class ConsBase(private val token: LToken) : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 3

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = args[0]
            val a1 = args[1]
            val a2 = args[2]

            //ap ap ap cons x0 x1 x2   =   (ap (ap x2 x0) x1)
            return AP(AP(a2, a0), a1)
        }

        override val description: String
            get() = "[${token.srcToken.toUpperCase()}]"

        override fun toSource(): String = token.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object NIL : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }
            val a0 = args[0]

            return T
        }

        override val description: String
            get() = "[NIL]"

        override fun toSource(): String = LToken.NIL.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    class ModulatedData(val value: String, private val source: String) : Data() {
        companion object {
            const val HEADER_SIZE = 2
            const val NWM = 4
        }

        override fun eval(): ENode {
            return this
        }

        override val description: String
            get() = "{MD/$source/$value}"

        override fun toSource(): String = "{$value}"

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object MOD : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val modSignalBuilder = StringBuilder()

            val a0Source = args[0].evalToTheMax().toSource()
            val a0LTokens = parseLTokens(a0Source)
            var apCount = 0
            a0LTokens.forEach {
                when (it) {
                    LToken.F_AP -> {
                        apCount++
                    }
                    LToken.F_CONS -> {
                        if (apCount != 2) {
                            error("apCount before cons must be 2")
                        }
                        apCount = 0
                        modSignalBuilder.append("11")
                    }
                    LToken.NIL -> {
                        modSignalBuilder.append("00")
                    }
                    is LToken.Numeral -> {
                        modSignalBuilder.append(modulateNumber(it))
                    }
                    else -> {
                        error("Don't know how to modulate $it")
                    }
                }
            }

            return ModulatedData(modSignalBuilder.toString(), a0Source)
        }

        private fun modulateNumber(number: LToken.Numeral): String {
            var bigNumber = BigInteger(number.srcToken)

            val modulatedNumberBuilder = StringBuilder()

            if (bigNumber < BigInteger.ZERO) {
                modulatedNumberBuilder.append("10")
                bigNumber = bigNumber.negate()
            } else {
                modulatedNumberBuilder.append("01")
            }

            val numberBin = if (BigInteger.ZERO == bigNumber) "" else bigNumber.toString(2)
            var extraZeroesNumber = 0
            while (0 != ((numberBin.length + extraZeroesNumber) % ModulatedData.NWM)) {
                extraZeroesNumber++
            }

            val bits = (numberBin.length + extraZeroesNumber) / ModulatedData.NWM
            for (i in 0 until bits) {
                modulatedNumberBuilder.append("1")
            }
            modulatedNumberBuilder.append("0")

            for (i in 0 until extraZeroesNumber) {
                modulatedNumberBuilder.append("0")
            }
            modulatedNumberBuilder.append(numberBin)

            return modulatedNumberBuilder.toString()
        }

        override val description: String
            get() = "[MOD]"

        override fun toSource(): String = LToken.F_MOD.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object DEM : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = (args[0].evalToTheMax() as? ModulatedData) ?: error("a0 must be ModulatedData")
            val a0Source = demToSource(a0.value)
            val a0LTokens = parseLTokens(a0Source)
            val a0DemNode = parseExpression(VarsStorageFAKE, a0LTokens)

            return a0DemNode
        }

        private fun demToSource(input: String): String {
            var currentIndex = 0

            fun isEnd() = currentIndex >= input.length

            fun remainingSize() = input.length - currentIndex

            fun isPositiveNumber(): Boolean {
                if (isEnd() || remainingSize() < ModulatedData.HEADER_SIZE) {
                    error("WWWWWW")
                }
                return ('0' == input[currentIndex] && '1' == input[currentIndex + 1])
            }

            fun isNegativeNumber(): Boolean {
                if (isEnd() || remainingSize() < ModulatedData.HEADER_SIZE) {
                    error("WWWWWW")
                }
                return ('1' == input[currentIndex] && '0' == input[currentIndex + 1])
            }

            fun isNumber(): Boolean {
                return isPositiveNumber() || isNegativeNumber()
            }

            fun isList(): Boolean {
                if (isEnd() || remainingSize() < ModulatedData.HEADER_SIZE) {
                    error("WWWWWW")
                }
                return ('1' == input[currentIndex] && '1' == input[currentIndex + 1])
            }

            fun isNil(): Boolean {
                if (isEnd() || remainingSize() < ModulatedData.HEADER_SIZE) {
                    error("WWWWWW")
                }
                return ('0' == input[currentIndex] && '0' == input[currentIndex + 1])
            }

            fun demNumber(): BigInteger {
                if (!isNumber()) {
                    error("WWWWWW")
                }

                var result = BigInteger.ZERO

                val negative = isNegativeNumber()
                currentIndex += ModulatedData.HEADER_SIZE

                var bitsNumber = 0
                while ('1' == input[currentIndex + bitsNumber]) {
                    bitsNumber++
                }
                currentIndex += bitsNumber + 1

                bitsNumber *= ModulatedData.NWM
                for (i in 0 until bitsNumber) {
                    result = result.multiply(BIG2)
                    val x = (input[currentIndex + i] - '0').toLong()
                    result = result.add(BigInteger.valueOf(x))
                }
                currentIndex += bitsNumber

                if (negative) {
                    result = result.negate()
                }

                return result
            }

            val demBuilder = StringBuilder()
            while (!isEnd()) {
                if (0 != currentIndex) {
                    demBuilder.append(" ")
                }

                if (isNil()) {
                    demBuilder.append("nil")
                    currentIndex += ModulatedData.HEADER_SIZE
                } else if (isList()) {
                    demBuilder.append("ap ap cons")
                    currentIndex += ModulatedData.HEADER_SIZE
                } else {
                    demBuilder.append(demNumber())
                }
            }
            return demBuilder.toString()
        }

        override val description: String
            get() = "[DEM]"

        override fun toSource(): String = LToken.F_DEM.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object SEND : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            val a0 = args[0]
            val modulatedA0 = (AP(MOD, a0).evalToTheMax() as? ModulatedData) ?: error("Modulation failed")

            println("- Performing http [SEND]")
            val modulatedResponse = GlobalRuntime.http.send(modulatedA0.value)
            println("- Completed http [SEND]")

            return AP(DEM, ModulatedData(modulatedResponse, "~FROM~ALIENS~"))
        }

        override val description: String
            get() = "[SEND]"

        override fun toSource(): String = LToken.F_SEND.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    // ap ap ap interact x0 nil ap ap vec 0 0
    object INTERACT : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 3

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            println("-------- PERFORMING INTERACTION")

            val protocol = args[0]
            val initialState = args[1]
            val initialPoint = args[2]

            val painter = Painter()

            var lastState = initialState
            var lastPoint = initialPoint

            var lastData: ENode? = null

            fun processResult(resultNode: ENode): Boolean {
                println("------ PROCESSING CALL RESULT")
                val flag = (AP(CAR, resultNode).evalToTheMax() as? NUM) ?: error("Flag not num")
                val eof = BigInteger.ZERO == flag.bigValue
                val tail1 = AP(CDR, resultNode)
                lastState = AP(CAR, tail1).evalToTheMax()
                val tail2 = AP(CDR, tail1)
                lastData = AP(CAR, tail2).evalToTheMax()

                //println("---- INTERACTION STEP RESULT")
                //println(flag.evalToTheMax().toSource())
                //println(lastState.evalToTheMax().toSource())
                //println(AP(TOHLIST, lastData!!).evalToTheMax().toSource())
                //println("----\n")

                painter.draw(lastData!!)

                if (!eof) {
                    val newPoint = AP(SEND, lastData!!).evalToTheMax()
                    lastPoint = newPoint
                }

                println("------ PROCESSED CALL RESULT")

                return eof
            }

            fun callNode() = AP(AP(protocol, lastState), lastPoint)

            while (true) {
                println("------ EVALUATING PROTOCOL CALL")
                val interactionStepResult = callNode().evalToTheMax()
                println("------ EVALUATED PROTOCOL CALL")
                val eof = processResult(interactionStepResult)
                if (eof) {
                    break
                }
            }

            if (lastData == null) {
                error("No data")
            }

            println("---- WRITING INTERACTION PICTURE")
            val out = File("galaxy/${PICTURE_NAME}.png")
            updatePainter(painter)
            painter.export(out)
            println("---- WROTE INTERACTION RESULT\n")

            println("---- PRECOMPUTING INTERACTION RETURN")
            val result = AP(AP(CONS, lastState), lastData!!).evalToTheMax()
            println("---- PRECOMPUTED INTERACTION RETURN\n")

            return result
        }

        override val description: String
            get() = "[INTERACT]"

        override fun toSource(): String = LToken.F_INTERACT.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    abstract class Data : ENode()

    class NUM(private val value: String) : Data() {
        constructor(bigValue: BigInteger) : this(bigValue.toString()) {
            _bigValue = bigValue
        }

        val rawValue: String
            get() = value

        private var _bigValue: BigInteger? = null

        val bigValue: BigInteger
            get() {
                if (_bigValue == null) {
                    _bigValue = getBig(value)
                }
                return _bigValue!!
            }

        override fun eval(): ENode {
            return this
        }

        override val description: String
            get() = "[NUM(${value})]"

        override fun toSource(): String = value

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    class HumanList(private val value: String) : Data() {
        override fun eval(): ENode {
            return this
        }

        override val description: String
            get() = "[HList($value)]"

        override fun toSource(): String = value

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    object TOHLIST : Function() {
        override fun eval(): ENode {
            return this
        }

        override val needsArgs: Int
            get() = 1

        override fun call(args: List<ENode>): ENode {
            if (needsArgs != args.size) {
                error("Must have $needsArgs args")
            }

            fun toHList(node: ENode): String {
                if (node is NIL) {
                    return "nil"
                } else if (node is NUM) {
                    return node.rawValue
                }

                val textBuilder = StringBuilder()
                textBuilder.append("(")

                var currentList = node
                var isFirst = true
                while (true) {
                    if (NIL == currentList) {
                        break
                    }
                    if (isFirst) {
                        isFirst = false
                    } else {
                        textBuilder.append(", ")
                    }
                    // special workaround for `ap ap cons 1 2` lists
                    if (currentList is NUM) {
                        textBuilder.append(toHList(currentList))
                        break
                    }
                    val item = AP(CAR, currentList).evalToTheMax()
                    textBuilder.append(toHList(item))
                    currentList = AP(CDR, currentList).evalToTheMax()
                }
                textBuilder.append(")")

                return textBuilder.toString()
            }

            return HumanList(toHList(args[0].evalToTheMax()))
        }

        override val description: String
            get() = "[TOHLIST]"

        override fun toSource(): String = LToken.F_TOLIST.srcToken

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    class AnonymousVar(private val raw: ENode) : Data() {
        private var evaluated: ENode? = null

        override fun eval(): ENode {
            if (evaluated != null) {
                return evaluated!!
            }
            evaluated = raw.evalToTheMax()
            return evaluated!!
        }

        override val description: String
            get() = "[ANON_VAR]"

        override fun toSource(): String {
            return "\$avar"
        }

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    class VAR(private val storage: VarsStorage, private val name: String) : Data() {
        override fun eval(): ENode {
            val varValueNode = storage.findVar(name)
            val updated = storage.isUpdated(name)
            var startTime: Long = 0
            if (!updated) {
                startTime = System.currentTimeMillis()
                println("Evaluating var '$name'")
            }
            val evalled = varValueNode.evalToTheMax()
            if (!updated) {
                val time = System.currentTimeMillis() - startTime
                println("Evaluated var '$name' - ${time}ms")
            }
            storage.updateVar(name, evalled)
            return evalled
        }

        override val description: String
            get() {
                val value = if (storage.isSet(name)) {
                    val varValue = storage.findVar(name)
                    "${varValue::class.simpleName}#${Integer.toHexString(varValue.hashCode())}"
                } else {
                    "unset"
                }
                return "[VAR(${name})=${value}]"
            }

        override fun toSource(): String = name

        override fun visit(visitor: EVisitor) {
            visitor.onEnterNode(this)
            visitor.onExitNode(this)
        }
    }

    abstract fun eval(): ENode

    abstract val description: String

    abstract fun toSource(): String

    abstract fun visit(visitor: EVisitor)
}


interface EVisitor {
    fun onEnterNode(node: ENode)
    fun onChildNode(node: ENode)
    fun onExitNode(node: ENode)
}
