package pggbnk.icfpc2020.converter

import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.request
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLBuilder
import io.ktor.http.content.TextContent
import io.ktor.http.isSuccess
import io.ktor.http.takeFrom
import io.ktor.utils.io.jvm.javaio.copyTo
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.io.ByteArrayOutputStream
import java.math.BigInteger

val BIG2 = BigInteger.valueOf(2)!!

interface VarsStorage {
    fun isSet(name: String): Boolean

    fun findVar(name: String): ENode

    fun updateVar(name: String, node: ENode)

    fun isUpdated(name: String): Boolean
}

class VarsStorageReal : VarsStorage {
    private val map = mutableMapOf<String, ENode>()

    private val updated = mutableMapOf<String, Boolean>()

    override fun isSet(name: String): Boolean {
        return map[name] != null
    }

    override fun findVar(name: String): ENode {
        return map[name] ?: error("Var '$name' not set")
    }

    fun setVar(name: String, node: ENode) {
        map[name] = node
        updated[name] = false
    }

    override fun updateVar(name: String, node: ENode) {
        if (!isSet(name)) {
            error("NO VAR")
        }
        map[name] = node
        updated[name] = true
    }

    override fun isUpdated(name: String): Boolean {
        return updated[name]!!
    }
}

object VarsStorageFAKE : VarsStorage {
    override fun isSet(name: String): Boolean {
        error("nope")
    }

    override fun findVar(name: String): ENode {
        error("nope")
    }

    override fun updateVar(name: String, node: ENode) {
        error("nope")
    }

    override fun isUpdated(name: String): Boolean {
        error("nope")
    }
}

fun HttpIO.testSend() {
    println("-- MAKING HTTP CALL")
    val result = send("1101000")
    println("HTTP RESULT: '$result'")
    println()
}

class  HttpIO(urlString: String, private val apiKey: String) {
    private val host: String

    init {
        val url = URLBuilder(urlString)
        host = url.host
    }

    private val client = HttpClient(Apache) {
    }

    fun send(text: String): String {
        val result = runBlocking {
            val response = doSend(text)

            var waiting200Response = response
            while (true) {
                if (waiting200Response.status.isSuccess()) {
                    //success
                    break
                } else if (HttpStatusCode.Found == waiting200Response.status) {
                    val location = waiting200Response.headers["Location"]!!
                    println("Received 302, Location: '$location'")
                    delay(1000)
                    waiting200Response = pollSendResult(location)
                } else {
                    //failure
                    break
                }
            }
            val finalResponse = waiting200Response
            if (!finalResponse.status.isSuccess()) {
                error("Not success ${finalResponse.status}")
            }
            val reader = ByteArrayOutputStream()
            finalResponse.content.copyTo(reader)
            return@runBlocking reader.toString("UTF-8")
        }
        return result
    }

    private suspend fun doSend(text: String): HttpResponse {
        return client.request<HttpResponse> {
            url {
                host = this@HttpIO.host
                encodedPath = "/aliens/send"
                parameters.append("apiKey", apiKey)
            }
            method = HttpMethod.Post
            body = TextContent(text, ContentType.Text.Plain)
        }
    }

    private suspend fun pollSendResult(location: String): HttpResponse {
        return client.request<HttpResponse> {
            url {
                takeFrom(location)
            }
            method = HttpMethod.Post
            body = TextContent("1101000", ContentType.Text.Plain)
        }
    }
}

fun BigInteger.pow(targetExp: BigInteger): BigInteger {
    var result = BigInteger.ONE
    var base = this
    var exponent = targetExp
    while (exponent.signum() > 0) {
        if (exponent.testBit(0)) {
            result = result.multiply(base)
        }
        base = base.multiply(base)
        exponent = exponent.shiftRight(1)
    }
    return result
}
