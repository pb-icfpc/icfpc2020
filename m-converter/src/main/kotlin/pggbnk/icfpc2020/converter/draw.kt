package pggbnk.icfpc2020.converter

import java.awt.Color
import java.awt.Graphics
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.max
import kotlin.math.min

class Painter {
    private val centerColor = Color.RED

    private val palette: List<Color>

    init {
        fun Color.withAlpha(alpha: Int) = Color(red, green, blue, alpha)
        val tmp = listOf(Color.BLUE, Color.CYAN, Color.YELLOW, Color.MAGENTA, Color.GREEN, Color.RED).map {
            it.withAlpha(128)
        }
        val white = Color.WHITE.withAlpha(200)
        palette = listOf(listOf(white), tmp).flatten()
    }

    private val layers = mutableListOf<List<Int>>()

    fun testLayer() {
        layers.add(listOf(
            -10, 1,
            0, 5,
            100, 25,
            100, -1))
    }

    private fun isEmpty(): Boolean {
        return layers.all { it.isEmpty() }
    }

    fun draw(pixelsNode: ENode) {
        println("-- EXTRACTING pixels")
        val layers = pixelsNode.extractPixels()
        println("-- EXTRACTED pixels\n")
        this.layers.addAll(layers)
    }

    fun bounds(): Pair<Pair<Int, Int>, Pair<Int, Int>> {
        if (isEmpty()) {
            error("No bounds for empty painter")
        }
        var minX = Int.MAX_VALUE
        var minY = Int.MAX_VALUE
        var maxX = Int.MIN_VALUE
        var maxY = Int.MIN_VALUE
        for (layer in layers) {
            for (pixel in 0 until layer.size / 2) {
                val x = layer[pixel * 2]
                val y = layer[pixel * 2 + 1]
                minX = min(minX, x)
                minY = min(minY, y)
                maxX = max(maxX, x)
                maxY = max(maxY, y)
            }
        }
        return Pair(Pair(minX, minY), Pair(maxX, maxY))
    }

    fun size(): Pair<Int, Int> {
        val bounds = bounds()
        val minX = bounds.first.first
        val minY = bounds.first.second
        val maxX = bounds.second.first
        val maxY = bounds.second.second
        val width = maxX - minX + 1
        val height = maxY - minY + 1
        return Pair(width, height)
    }

    fun convertFromDrawn(x: Int, y: Int): Pair<Int, Int>? {
        val bounds = bounds()
        val minX = bounds.first.first
        val minY = bounds.first.second
        val maxX = bounds.second.first
        val maxY = bounds.second.second
        val width = maxX - minX + 1
        val height = maxY - minY + 1

        val tx = minX + x
        val ty = minY + y
        /*
        val tx = minX + (width - x) - 1
        val ty = minY + (height - y) - 1
         */

        if (tx < minX || ty < minY ||
            tx > maxX || ty > maxY) {
            return null
        }
        return Pair(tx, ty)
    }

    fun drawOnG(canvas: Graphics) {
        if (isEmpty()) {
            return
        }
        val bounds = bounds()
        val minX = bounds.first.first
        val minY = bounds.first.second
        val maxX = bounds.second.first
        val maxY = bounds.second.second
        val width = maxX - minX + 1
        val height = maxY - minY + 1

        fun drawPixel(x: Int, y: Int) {
            val tx = x - minX
            val ty = y - minY
            /*
            val tx = width - (x - minX) - 1
            val ty = height - (y - minY) - 1
             */
            canvas.drawLine(tx, ty, tx, ty)
        }

        for ((layerIndex, layer) in layers.reversed().withIndex()) {
            val colorIndex = min(layers.size - layerIndex - 1, palette.lastIndex)
            canvas.color = palette[colorIndex]
            for (pixel in 0 until layer.size / 2) {
                val x = layer[pixel * 2]
                val y = layer[pixel * 2 + 1]
                drawPixel(x, y)
            }
        }

        canvas.color = centerColor
        //drawPixel(0, 0)
    }

    fun export(outFile: File) {
        if (isEmpty()) {
            return
        }
        val size = size()
        val width = size.first
        val height = size.second

        val image = BufferedImage(width, height, BufferedImage.TYPE_USHORT_565_RGB)
        val canvas = image.createGraphics()

        drawOnG(canvas)

        canvas.dispose()
        ImageIO.write(image, "png", outFile)
        println("IMAGE SIZE: $size")
    }
}

private fun ENode.extractPixels(): List<List<Int>> {
    val result = mutableListOf<List<Int>>()

    var layers: ENode = ENode.AnonymousVar(this)
    while (true) {
        if (ENode.NIL == layers.evalToTheMax()) {
            break
        }

        val layerPixels = mutableListOf<Int>()

        var layer: ENode = ENode.AnonymousVar(ENode.AP(ENode.CAR, layers))
        ll@while (true) {
            when (layer.evalToTheMax()) {
                is ENode.NIL, is ENode.NUM -> break@ll
            }

            val pixel = ENode.AnonymousVar(ENode.AP(ENode.CAR, layer))
            val a = (ENode.AP(ENode.CAR, pixel).evalToTheMax() as? ENode.NUM) ?: error("a")
            val b = (ENode.AP(ENode.CDR, pixel).evalToTheMax() as? ENode.NUM) ?: error("b")
            layerPixels.add(a.bigValue.toInt())
            layerPixels.add(b.bigValue.toInt())

            layer = ENode.AnonymousVar(ENode.AP(ENode.CDR, layer))
        }

        result.add(layerPixels)

        layers = ENode.AnonymousVar(ENode.AP(ENode.CDR, layers))
    }

    return result
}
