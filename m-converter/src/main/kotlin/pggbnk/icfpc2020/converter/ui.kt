package pggbnk.icfpc2020.converter

import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.EventQueue
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.Point
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.event.MouseWheelEvent
import java.awt.geom.AffineTransform
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.UIManager
import javax.swing.plaf.metal.MetalLookAndFeel

private lateinit var tPane: ZoomPane

fun updatePainter(painter: Painter) {
    EventQueue.invokeLater {
        tPane.updatePainter(painter)
    }
}

fun showUI() {
    EventQueue.invokeAndWait {
        doDhowUI()
    }
}

private fun doDhowUI() {
    UIManager.setLookAndFeel(MetalLookAndFeel())

    val frame = JFrame("GALAXY PAD")
    frame.defaultCloseOperation = JFrame.DO_NOTHING_ON_CLOSE
    frame.setSize(512, 512)

    val pane = frame.contentPane

    val button1 = JButton("Button 1")
    pane.add(button1, BorderLayout.PAGE_START)

    val centerContent = buildContent()
    pane.add(centerContent, BorderLayout.CENTER)

    frame.isVisible = true
}

private fun buildContent(): JComponent {
    tPane = ZoomPane()
    return JScrollPane(
        tPane,
        JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS)
}

private class ZoomPane : JPanel() {
    private var scale = 10.0

    private var painter: Painter = Painter().apply { testLayer() }

    fun updatePainter(painter: Painter) {
        this.painter = painter
        revalidate()
        repaint()
    }

    override fun getPreferredSize(): Dimension {
        val size = painter.size()
        val sWidth = size.first * scale
        val sHeight = size.second * scale
        return Dimension(sWidth.toInt(), sHeight.toInt())
    }

    val TX = 1
    val TY = 1

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)

        val g2d = g.create() as Graphics2D

        val at = AffineTransform()
        at.scale(scale, scale)
        at.translate(TX.toDouble(), TY.toDouble())
        g2d.transform = at

        val size = painter.size()
        g2d.color = Color.BLACK
        g2d.fillRect(-TX, -TY, size.first + TX, size.second + TY)

        painter.drawOnG(g2d)

        g2d.dispose()
    }

    init {
        addMouseWheelListener(object : MouseAdapter() {
            override fun mouseWheelMoved(e: MouseWheelEvent) {
                val delta = 0.05 * e.preciseWheelRotation
                scale += delta
                revalidate()
                repaint()
            }
        })
        addMouseListener(object : MouseListener {
            override fun mouseClicked(e: MouseEvent) {
                val point = Point(e.point)

                val at = AffineTransform()
                at.scale(scale, scale)
                at.translate(TX.toDouble(), TY.toDouble())
                at.inverseTransform(point, point)

                val (x, y) = painter.convertFromDrawn(point.x, point.y) ?: return
                onGalaxyClick(x, y)
            }

            override fun mousePressed(e: MouseEvent?) {
            }

            override fun mouseReleased(e: MouseEvent?) {
            }

            override fun mouseEntered(e: MouseEvent?) {
            }

            override fun mouseExited(e: MouseEvent?) {
            }
        })
    }
}
