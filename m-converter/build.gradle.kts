
val ktor_version = "1.3.2"

buildscript {
    repositories {
        jcenter()
        mavenCentral()
    }

    dependencies {
    }
}

repositories {
    jcenter()
    mavenCentral()
}

version = "1.0.0"
group = "pggbnk.icfpc2020.converter"

plugins {
    application
    kotlin("jvm") version "1.3.72"
}

application {
    mainClassName = "pggbnk.icfpc2020.converter.MainKt"
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-apache:$ktor_version")
}


