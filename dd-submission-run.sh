#!/usr/bin/env bash

# needs 2 args
# 1 - server url
# 2 - player key

docker run -it --rm pggbnk.icfpc2020.submission:latest "$@"
