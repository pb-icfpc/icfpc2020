#!/usr/bin/env bash

export M_INTERACTOR="${ICFPC_CHECKOUT_ROOT}/m-interactor"
export M_INTERACTOR_BUILD="${ICFPC_BUILD_ROOT}/m-interactor"

pushd "${M_INTERACTOR}/make"
./make.sh ${M_INTERACTOR_BUILD}
