# Piggybank Software solution for ICFPC 2020

We proudly present our team of 6 people: 1 from Novosibirsk, RUS, and 5 from Saint Petersburg, RUS.

During the contest we had several activities and developed several modules:
- Produced two interpreters of the galaxy language in Kotlin and C++: you know, parsers, lexers, terms, evaluators, lambdas, etc.
- Coded at least 10 string representations of lists.
- Developed a really nice interactive visualizer for the galaxy.
- Discovered loads of hidden knowledge through Galaxy Pad UI.
- Tried lots of strategies for battles.
- Crafted a modeling tool for galaxy physics with ships visualization.
- Had a great time filled with mixed feelings and fun.

## Attacker strategy, in brief
- Split up to 4 ships.
- Have a single main attacker with a big gun and more fuel and better cooling technology.
- Explode when the enemy is near, but our ships are far enough.
- Try to stay on orbit using the minimum fuel required.
- Shoot at enemies according to the diagram from the galaxy and only when a ship is cool enough.

## Defender strategy, in brief
- Split up to 4 ships.
- Have a single main tough guy with lots of fuel to maneuver. 
- Use more fuel instead of using guns.
- Try to stay on orbit with the minimum required fuel.

## Some pictures
![1](pictures/last-1.png)

![2](pictures/last-2.png)

![3](pictures/last-3.png)
