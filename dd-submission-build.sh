#!/usr/bin/env bash

#TODO: need some shell magic to ensure workdir is set to script's location

# need to cleanup files from out-of-container build, so cmake won't get confused
rm -rf m-interactor/make/temp_build
rm -rf m-interactor/target/bin

docker build \
    -t pggbnk.icfpc2020.submission:latest \
    -f d-dockerfiles/submission/Dockerfile \
    .
