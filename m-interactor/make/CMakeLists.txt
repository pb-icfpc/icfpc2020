cmake_minimum_required(VERSION 3.9)

project(interactor)

# Print info.
message("cmake version: ${CMAKE_VERSION}")

# Set directories.
get_filename_component(PROJECT_DIR "${CMAKE_CURRENT_SOURCE_DIR}/.." ABSOLUTE)
set(SRC_DIR "${PROJECT_DIR}/src")

# Set C++ options.
#set(CMAKE_CXX_STANDARD 20)
#set(CMAKE_CXX_STANDARD_REQUIRED True)
# No time to update cmake and gcc.
set(ENABLE_CXX20 "-std=c++2a")
set(SILENCE_GTEST_WARNINGS "-Wno-deprecated-copy")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ENABLE_CXX20} ${SILENCE_GTEST_WARNINGS}")

include_directories("/usr/local/include")
link_directories("/usr/local/lib")

include_directories(${SRC_DIR}/viz)

# Viz
option(BUILD_VIZ "Build viz?" OFF)
if (${BUILD_VIZ})
# install on ubuntu:
# sudo apt-get install libglm-dev libxcb1 libxcb-util-dev libxcb-icccm4-dev libvulkan-dev

    find_package(Vulkan)
    find_package(glm REQUIRED)

    function(sources_in_dir_recurse directory result)
        file(GLOB_RECURSE output
            "${directory}/*.cpp"
            "${directory}/*.h"
            "${directory}/*.hpp"
        )
        set(${result} ${output} PARENT_SCOPE)
    endfunction()

    sources_in_dir_recurse(${SRC_DIR}/viz/ge viz_sources)
    add_library(viz ${viz_sources})
    target_compile_definitions(viz PRIVATE -DVK_USE_PLATFORM_XCB_KHR=1)
    target_link_libraries(viz
        PUBLIC
            # glm
#            Vulkan::Vulkan
            ${Vulkan_LIBRARIES}
            xcb xcb-util xcb-icccm
            Threads::Threads
            dl
            interactor_lib
    )
    target_include_directories(viz
        SYSTEM PUBLIC
            ${SRC_DIR}/viz
    )
    target_compile_definitions(viz PUBLIC "-DWITH_VIZ")

    add_executable(example_2d_grid ${SRC_DIR}/viz/examples/grid/main.cpp)
    target_link_libraries(example_2d_grid viz)

    add_executable(example_2d_points_list ${SRC_DIR}/viz/examples/points/main.cpp)
    target_link_libraries(example_2d_points_list viz)

    add_executable(example_singleton ${SRC_DIR}/viz/examples/singleton/main.cpp)
    target_link_libraries(example_singleton viz)

    add_executable(draw_log ${SRC_DIR}/viz/examples/draw_log/main.cpp)
    target_link_libraries(draw_log viz)

    install(
        TARGETS
            example_2d_grid
            example_2d_points_list
            example_singleton
            draw_log
        DESTINATION
            bin
    )

    set(LINK_VIZ "viz")
else()
    set(LINK_VIZ "")
endif()


# Collect library sources.
file(GLOB_RECURSE INTERACTOR_LIB_SOURCES "${SRC_DIR}/*.h" "${SRC_DIR}/*.hpp" "${SRC_DIR}/*.cpp")
list(FILTER INTERACTOR_LIB_SOURCES EXCLUDE REGEX ".*viz/|.*third_party/googletest.*|.*tests/.*|.*main.cpp$")

add_library(interactor_lib ${INTERACTOR_LIB_SOURCES})
target_include_directories(interactor_lib PUBLIC "${SRC_DIR}")
target_link_libraries(interactor_lib PUBLIC ${LINK_VIZ})

# Interactor
add_executable(interactor "${SRC_DIR}/main.cpp")
target_link_libraries(interactor PRIVATE interactor_lib gmpxx gmp curl)
target_include_directories(interactor PRIVATE "${SRC_DIR}")

# Galaxy UI
add_executable(galaxyui "${SRC_DIR}/MainGalaxy.cpp")
target_link_libraries(galaxyui PRIVATE interactor_lib gmpxx gmp curl)
target_include_directories(galaxyui PRIVATE "${SRC_DIR}")

# Physics
add_executable(physics "${SRC_DIR}/MainPhysics.cpp")
target_link_libraries(physics PRIVATE interactor_lib gmpxx gmp curl)
target_include_directories(physics PRIVATE "${SRC_DIR}")

# Tests.
add_subdirectory("${SRC_DIR}/third_party/googletest" "${CMAKE_BINARY_DIR}/gtest")
set(TESTS_SRC_DIR "${PROJECT_DIR}/src/tests")
file(GLOB_RECURSE INTERACTOR_TESTS_SOURCES "${TESTS_SRC_DIR}/*.h" "${TESTS_SRC_DIR}/*.hpp" "${TESTS_SRC_DIR}/*.cpp")

find_package(Threads REQUIRED)

add_executable(tests ${ICFPC_LIB_SOURCES} ${INTERACTOR_TESTS_SOURCES})
target_include_directories(tests PRIVATE ${SRC_DIR})
target_link_libraries(tests gtest_main gtest Threads::Threads interactor_lib gmpxx gmp curl)


file(COPY "${SRC_DIR}/galaxy.txt" DESTINATION ".")

# Install artifacts.
install(TARGETS interactor galaxyui physics DESTINATION bin)
install(TARGETS tests DESTINATION bin)
