#!/usr/bin/env bash

export BUILD_DIR="temp_build"
export INSTALL_DIR="../../target"

if [ -z "$1" ]
then
    echo "No base dir, using defaults"
else
    echo "Using provided base dir: '$1'"
    export BUILD_DIR="$1/build"
    export INSTALL_DIR="$1/target"
fi
echo "Build dir: '${BUILD_DIR}', Install dir: '${INSTALL_DIR}'"

#TODO: bad
export CMAKE_PROJECT_ROOT=`pwd`

mkdir -p "${BUILD_DIR}"
pushd "${BUILD_DIR}"
mkdir -p "${INSTALL_DIR}"
cmake "${CMAKE_PROJECT_ROOT}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}"
cmake --build .
make install
