#include "viz/ge/drawer/Drawer.h"

#include "game/Control.h"
#include "game/Planet.h"

#include <iostream>

int main(int argc, char* argv[])
{
    #ifdef WITH_VIZ

    using namespace game;


    std::vector<Ship> ships;

    {
        Ship ship;
        ship.pos = Vec{-48, -48};
        ship.vel = {0, 0};
        ship.id = "4";
        ship.petrol = 0; // Counts used petrol in this main, not leftover

        ships.push_back(ship);
    }

    {
        Ship ship;
        ship.pos = Vec{-44, -48};
        ship.vel = {0, 0};
        ship.id = "5";
        ship.petrol = 0;

        ships.push_back(ship);
    }

    {
        Ship ship;
        ship.pos = Vec{-40, -48};
        ship.vel = {0, 0};
        ship.id = "6";
        ship.petrol = 0;

        ships.push_back(ship);
    }

    {
        Ship ship;
        ship.pos = Vec{-17, -20};
        ship.vel = {0, 0};
        ship.id = "7";
        ship.petrol = 0;

        ships.push_back(ship);
    }

    {
        Ship ship;
        ship.pos = Vec{-48, -1};
        ship.vel = {0, 0};
        ship.id = "0";
        ship.petrol = 0;

        ships.push_back(ship);
    }

   {
       Ship ship;
       ship.pos = Vec{40, -15};
       ship.vel = {1, 1};
       ship.id = "1";
       ship.petrol = 0;

       ships.push_back(ship);
   }

   {
       Ship ship;
       ship.pos = Vec{-40, 15};
       ship.vel = {1, 1};
       ship.id = "2";
       ship.petrol = 0;

       ships.push_back(ship);
   }

   {
       Ship ship;
       ship.pos = Vec{-40, 15};
       ship.vel = {-1, -1};
       ship.id = "3";

       ships.push_back(ship);
   }

    viz::getDrawerSingleton().enableRandomColor(false);
    viz::getDrawerSingleton().clear();
    std::this_thread::sleep_for(std::chrono::seconds(2));

    // std::cin.get();
    int tick = 0;
    while (true)
    {
        viz::getDrawerSingleton().clear();

        Points toDraw = {{0, 0}
            ,{-PLANET_RADIUS, -PLANET_RADIUS}, {PLANET_RADIUS, -PLANET_RADIUS}, {PLANET_RADIUS, PLANET_RADIUS}, {-PLANET_RADIUS, PLANET_RADIUS}
            ,{-50, -50}, {50, -50}, {50, 50}, {-50, 50}
            ,{-UNSAFE_ORBIT, -UNSAFE_ORBIT}, {UNSAFE_ORBIT, -UNSAFE_ORBIT}, {UNSAFE_ORBIT, UNSAFE_ORBIT}, {-UNSAFE_ORBIT, UNSAFE_ORBIT}
        };
        tick += 1;
        for (auto& ship : ships)
        {
            Vec accel = acceleration(ship, tick);
            // std::cerr << accel << '\n';
            ship.vel -= accel;
            if (linf(accel) != 0) {
                ship.petrol += 1;
                std::cerr << ship.id << ": " << ship.petrol << '\n';
            }
            ship.vel += planetAcceleration(ship.pos);
            ship.pos += ship.vel;

            toDraw.push_back(Point{ship.pos.x, ship.pos.y});

            if (linf(ship.pos) < PLANET_RADIUS)
            {
                exit(1);
            }
        }

        viz::getDrawerSingleton().draw(toDraw);
        // std::cin.get();
        std::this_thread::sleep_for(std::chrono::milliseconds(30));
    }

    #endif
    return 0;
}
