#include "framework/BigInt.h"

#include <gtest/gtest.h>

TEST(BigInt, pow2)
{
    BigInt result = pow2(333);
    EXPECT_EQ("17498005798264095394980017816940970922825355447145699491406164851279623993595007385788105416184430592", result.get_str());
}
