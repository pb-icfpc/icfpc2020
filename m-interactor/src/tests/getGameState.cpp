#include "game/analyseResponse.h"

#include <gtest/gtest.h>

#include <string>

TEST(getGameState, getGameState)
{
    using namespace game;

    static const std::string STATE = "ap ap cons 1 ap ap cons 1 ap ap cons ap ap cons 256 ap ap cons 1 ap ap cons ap ap cons 448 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons nil nil ap ap cons ap ap cons 8 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -21 -13 ap ap cons ap ap cons 1 7 ap ap cons ap ap cons 32 ap ap cons 16 ap ap cons 8 ap ap cons 4 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 23 47 ap ap cons ap ap cons 1 -1 ap ap cons ap ap cons 318 ap ap cons 0 ap ap cons 10 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 2 nil ap ap cons ap ap cons ap ap cons 0 ap ap cons ap ap cons -1 0 nil nil nil nil nil nil";

    const GameState state = getGameState(STATE);
    const ShipsAndCommands ourShips = getOurShipsAndCommands(STATE);

    EXPECT_TRUE(true);
}
