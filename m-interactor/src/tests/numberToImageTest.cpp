#include <gtest/gtest.h>

#include "utils.h"

TEST(numberToImageTest, checkPositive)
{
    EXPECT_EQ(
        numberToImage(0).data,
        std::vector<char> ({
            0, 1,
            1, 0
        })
    );

    EXPECT_EQ
    (
        numberToImage(1).data,
        std::vector<char> ({
            0, 1,
            1, 1
        })
    );

    EXPECT_EQ
    (
        numberToImage(3).data,
        std::vector<char> ({
            0, 1, 1,
            1, 1, 1,
            1, 0, 0
        })
    );

    EXPECT_EQ
    (
        numberToImage(50).data,
        std::vector<char> ({
            0, 1, 1, 1,
            1, 0, 1, 0,
            1, 0, 1, 1,
            1, 0, 0, 0
        })
    );
}

TEST(numberToImageTest, checkNegative)
{
    EXPECT_EQ(
        numberToImage(-0).data,
        std::vector<char> ({
                               0, 1,
                               1, 0,
                           })
    );

    EXPECT_EQ
    (
        numberToImage(-1).data,
        std::vector<char> ({
                               0, 1,
                               1, 1,
                               1, 0
                           })
    );

    EXPECT_EQ
    (
        numberToImage(-3).data,
        std::vector<char> ({
                               0, 1, 1,
                               1, 1, 1,
                               1, 0, 0,
                               1, 0, 0
                           })
    );

    EXPECT_EQ
    (
        numberToImage(-50).data,
        std::vector<char> ({
                               0, 1, 1, 1,
                               1, 0, 1, 0,
                               1, 0, 1, 1,
                               1, 0, 0, 0,
                               1, 0, 0, 0
                           })
    );
}