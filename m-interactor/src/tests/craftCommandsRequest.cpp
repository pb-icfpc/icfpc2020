#include "game/craftRequests.h"
#include "game/GlobalGameState.h"

#include "lang/Evaluator.h"
#include "lang/ListUtils.h"

#include <gtest/gtest.h>

TEST(craftCommandsRequest, craftCommandsRequest)
{
    game::GlobalGameState globalGameState;

    static const std::string STATE = "ap ap cons 1 ap ap cons 1 ap ap cons ap ap cons 256 ap ap cons 1 ap ap cons ap ap cons 448 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons nil nil ap ap cons ap ap cons 8 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -21 -13 ap ap cons ap ap cons 1 7 ap ap cons ap ap cons 32 ap ap cons 16 ap ap cons 8 ap ap cons 4 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 23 47 ap ap cons ap ap cons 1 -1 ap ap cons ap ap cons 318 ap ap cons 0 ap ap cons 10 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 2 nil ap ap cons ap ap cons ap ap cons 0 ap ap cons ap ap cons -1 0 nil nil nil nil nil nil";
    static const std::string STATE_ATTACKER = "ap ap cons 1 ap ap cons 1 ap ap cons ap ap cons 256 ap ap cons 0 ap ap cons ap ap cons 512 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons 1 ap ap cons 1 ap ap cons 1 ap ap cons 1 nil nil ap ap cons ap ap cons 0 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -48 -28 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 1 ap ap cons 1 ap ap cons 1 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 48 28 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 326 ap ap cons 0 ap ap cons 10 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil";

    EXPECT_EQ
    (
        "ap ap cons 4 ap ap cons 654007 ap ap cons ap ap cons ap ap cons 0 ap ap cons 0 ap ap cons ap ap cons 1 -1 nil nil nil"
        , game::craftCommandsRequest("654007", STATE, 14, globalGameState)
    );

    EXPECT_EQ
    (
        "ap ap cons 4 ap ap cons 654007 ap ap cons ap ap cons ap ap cons 3 ap ap cons 0 ap ap cons ap ap cons 128 ap ap cons 0 ap ap cons 5 ap ap cons 2 nil nil nil nil"
        , game::craftCommandsRequest("654007", STATE, 0, globalGameState)
    );

    EXPECT_EQ
    (
        "ap ap cons 4 ap ap cons 654007 ap ap cons ap ap cons ap ap cons 3 ap ap cons 0 ap ap cons ap ap cons 32 ap ap cons 0 ap ap cons 1 ap ap cons 1 nil nil nil nil"
        , game::craftCommandsRequest("654007", STATE, 6, globalGameState)
    );

//    std::cout << "!!! " << lang::list::prettyPrint(lang::file_evaluator::evaluateBlock("galaxy = " + STATE)) << std::endl;
    std::cout << "!!! " << lang::list::prettyPrint(lang::file_evaluator::evaluateBlock(std::string{"galaxy = "} + "ap ap cons 4 ap ap cons 654007 ap ap cons ap ap cons ap ap cons 3 ap ap cons 0 ap ap cons ap ap cons 127 ap ap cons 10 ap ap cons 5 ap ap cons 1 nil nil nil nil")) << std::endl;
}
