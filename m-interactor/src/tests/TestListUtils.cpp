#include <gtest/gtest.h>

#include "lang/Evaluator.h"
#include "lang/points.h"
#include "lang/ListUtils.h"
#include "lang/Wrapper.h"

TEST(evaluator, small_list)
{
    const std::array<std::string, 4> LISTS
    {
        "nil"
        , "ap ap cons 2 nil"
        , "ap ap cons 1 ap ap cons 2 nil"
        , "ap ap cons 1 ap ap cons 2 ap ap cons 3 nil"
    };

    const std::array<std::string, 4> ANSWERS
    {
        ""
        , "2 "
        , "1 2 "
        , "1 2 3 "
    };

    for (size_t i = 0; i < LISTS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock("galaxy = " + LISTS[i]);
        std::vector<SPtr<lang::Term>> vec = lang::list::toVector(term);

        std::string result;
        for (const auto& term : vec)
        {
            result += toString(*term) + ' ';
        }
        EXPECT_EQ(result, ANSWERS[i]);
    }
}

TEST(evaluator, to_points)
{
    const std::array<std::string, 2> LISTS
    {
          "ap ap cons ap ap vec 1 1 nil"
        , "ap ap cons ap ap vec 5 3 ap ap cons ap ap vec 6 3 ap ap cons ap ap vec 4 4 ap ap cons ap ap vec 6 4 ap ap cons ap ap vec 4 5 nil"
    };

    const std::array<Points, 2> ANSWERS
    {
          Points{Point{1, 1}}
        , Points{Point{5, 3}, Point{6, 3}, Point{4, 4}, Point{6, 4}, Point{4, 5}}
    };

    for (size_t i = 0; i < LISTS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock("galaxy = " + LISTS[i]);
        auto result = lang::points(term);

        EXPECT_EQ(result, ANSWERS[i]);
    }
}

TEST(evaluator, plain_to_string)
{
    const std::array<std::string, 3> LISTS
    {
          "ap ap cons 2 nil"
        , "ap ap cons 1 ap ap cons 2 nil"
        , "ap ap cons 1 ap ap cons 2 ap ap cons 3 nil"
    };

    const std::array<std::vector<std::string>, 3> ANSWERS
    {
          std::vector<std::string>{{"2"}}
        , std::vector<std::string>{{"1"}, {"2"}}
        , std::vector<std::string>{{"1"}, {"2"}, {"3"}}
    };

    for (size_t i = 0; i < LISTS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock("galaxy = " + LISTS[i]);
        auto result = lang::list::plainToString(term);

        EXPECT_EQ(result, ANSWERS[i]);
    }
}

