#include <gtest/gtest.h>

#include "lang/Lexer.h"
#include "lang/Parser.h"
#include "lang/tokenToLexemeKind.h"
#include "lang/ListUtils.h"

#include <sstream>
#include <fstream>

TEST(parser, DISABLED_parse_galaxy)
{
    std::ifstream file("galaxy.txt");
    EXPECT_TRUE(file.is_open());
    lang::Lexer lexer(file);

    lang::Parser parser;
    parser.parse(lexer);

    for (const auto& [name, term] : parser.definedVariables)
    {
        std::cout << name << ": " << toString(*term) << '\n';
    }
}

TEST(parser, negate)
{
    std::stringstream buffer(
        ":1029 = ap neg 7\n"
        ":1030 = ap add :1029\n");
    lang::Lexer lexer(buffer);

    lang::Parser parser;
    parser.parse(lexer);

    for (const auto& [name, term] : parser.definedVariables)
    {
        std::cout << name << ": " << toString(*term) << '\n';
    }
}

TEST(parser, numbers)
{
    std::stringstream buffer(
        ":1029 = -7\n"
        ":1030 = 1024\n"
        ":1031 = ap ap mul 10 -10\n"
        ":1032 = ap ap mul 10 0\n"
        ":1033 = ap ap mul 10 -0"
        );
    lang::Lexer lexer(buffer);

    lang::Parser parser;
    parser.parse(lexer);

    for (const auto& [name, term] : parser.definedVariables)
    {
        std::cout << name << ": " << toString(*term) << '\n';
    }
}


TEST(parser, DISABLED_cons)
{
    std::stringstream buffer(":1080 = ap ap cons 5 ap ap cons 14711822 nil");
    lang::Lexer lexer(buffer);

    lang::Parser parser;
    parser.parse(lexer);

    for (const auto& [name, term] : parser.definedVariables)
    {
        std::cout << name << ": " << toString(*term) << '\n';
    }
}

TEST(parser, small_galaxy)
{
    std::stringstream buffer("galaxy = -14711822");
    lang::Lexer lexer(buffer);

    lang::Parser parser;
    parser.parse(lexer);

    for (const auto& [name, term] : parser.definedVariables)
    {
        std::cout << name << ": " << toString(*term) << '\n';
    }
}

TEST(parser, statelessdraw_parse)
{
    std::stringstream buffer("ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil");
    lang::Lexer lexer(buffer);

    lang::Parser parser;
    auto term = parser.parseTerm(lexer);

    auto targetTerm = lang::constants::STATELESSDRAW();
    std::cout << toString(*targetTerm) << "\n";
    std::cout << toString(*term) << '\n';
}

TEST(parser, statefuldraw_parse)
{
    std::stringstream buffer("ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c cons nil ap c cons");
    lang::Lexer lexer(buffer);

    lang::Parser parser;
    auto term = parser.parseTerm(lexer);

    auto targetTerm = lang::constants::STATEFULDRAW();
    std::cout << toString(*targetTerm) << "\n";
    std::cout << toString(*term) << '\n';
}




