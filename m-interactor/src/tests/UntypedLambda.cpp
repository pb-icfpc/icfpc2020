#include <gtest/gtest.h>

#include "lang/UntypedLambda.h"

TEST(UntypedLambda, TestId)
{
    using namespace lang;

    auto id = LAMBDA(x, x);
    EXPECT_EQ("λx.x" , toString(*id));
    EXPECT_EQ("λx.x" , toString(*headEval(id)));
    EXPECT_EQ("y" , toString(*headEval(app(LAMBDA(x, x), var("y")))));
}
