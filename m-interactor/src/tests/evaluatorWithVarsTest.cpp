#include <gtest/gtest.h>

#include "lang/Evaluator.h"

TEST(evaluator, evaluate_full_galaxy)
{
    auto term = lang::file_evaluator::evaluateFile("galaxy.txt");

    std::cout << toString(*term);
}

TEST(evaluator, evaluate_sequence_of_variables)
{
    auto term = lang::file_evaluator::evaluateBlock(":1029 = 7\n"
                                                    ":1030 = :1029\n"
                                                    "galaxy = :1030");

    EXPECT_EQ(toString(*term), "7");
}

TEST(evaluatorWithVars, lazy_eval)
{
    auto term = lang::file_evaluator::evaluateBlock(
        ":1 = :1\n"
        "galaxy = ap car ap ap cons 42 :1"
    );

    EXPECT_EQ(toString(*term), "42");
}

TEST(evaluator, partly_applied_args)
{
    auto term = lang::file_evaluator::evaluateBlock
    (
        "galaxy = ap :mul5 ap :dec2 12\n"
        ":dec2 = ap add ap neg 2\n"
        ":mul5 = ap mul 5\n"
    );

    EXPECT_EQ(toString(*term), "50");
}

/// TODO: IGOR: add more tests.