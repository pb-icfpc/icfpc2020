#include "game/analyseResponse.h"

#include <array>
#include <string>

#include <gtest/gtest.h>

TEST(getPlayerRole, defender)
{
    std::array<std::string, 3> DEFENDER_GAME_RESPONSES
    {
          "ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons 256 ap ap cons 1 ap ap cons ap ap cons 448 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons nil nil ap ap cons nil nil"
        , "ap ap cons 1 ap ap cons 1 ap ap cons ap ap cons 256 ap ap cons 1 ap ap cons ap ap cons 448 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons nil nil ap ap cons ap ap cons 0 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -21 -48 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 4 ap ap cons 8 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 21 48 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 1 ap ap cons 1 ap ap cons 1 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil"
        , "ap ap cons 1 ap ap cons 2 ap ap cons ap ap cons 256 ap ap cons 1 ap ap cons ap ap cons 448 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons nil nil ap ap cons ap ap cons 0 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -21 -48 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 4 ap ap cons 8 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 21 48 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 1 ap ap cons 1 ap ap cons 1 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil"
    };

    for (const std::string& response : DEFENDER_GAME_RESPONSES)
    {
        EXPECT_EQ(game::PlayerRole::DEFENDER, game::getPlayerRole(response));
    }
}

TEST(getPlayerRole, attacker)
{
    std::array<std::string, 4> ATTACKER_GAME_RESPONSES
    {
          "ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons 256 ap ap cons 0 ap ap cons ap ap cons 512 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 3 ap ap cons 4 nil nil ap ap cons nil nil"
        , "ap ap cons 1 ap ap cons 1 ap ap cons ap ap cons 256 ap ap cons 0 ap ap cons ap ap cons 512 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 3 ap ap cons 4 nil nil ap ap cons ap ap cons 0 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -48 -39 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 3 ap ap cons 4 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 48 39 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 4 ap ap cons 8 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil"
        , "ap ap cons 1 ap ap cons 1 ap ap cons ap ap cons 256 ap ap cons 0 ap ap cons ap ap cons 512 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 3 ap ap cons 4 nil nil ap ap cons ap ap cons 5 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -34 -38 ap ap cons ap ap cons 4 1 ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 3 ap ap cons 4 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 34 38 ap ap cons ap ap cons -4 -1 ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 4 ap ap cons 8 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil"
        , "ap ap cons 1 ap ap cons 2 ap ap cons ap ap cons 256 ap ap cons 0 ap ap cons ap ap cons 512 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons 1 ap ap cons 2 ap ap cons 3 ap ap cons 4 nil nil ap ap cons ap ap cons 11 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons -10 -11 ap ap cons ap ap cons 4 7 ap ap cons ap ap cons 0 ap ap cons 0 ap ap cons 0 ap ap cons 0 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons 10 11 ap ap cons ap ap cons -4 -7 ap ap cons ap ap cons 0 ap ap cons 0 ap ap cons 0 ap ap cons 0 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil"
    };

    for (const std::string& response : ATTACKER_GAME_RESPONSES)
    {
        EXPECT_EQ(game::PlayerRole::ATTACKER, game::getPlayerRole(response));
    }
}
