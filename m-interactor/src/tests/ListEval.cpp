#include <gtest/gtest.h>

#include "lang/constants/List.h"
#include "lang/constants/Bool.h"

TEST(ListEval, isNil)
{
    using namespace lang::constants;
    EXPECT_EQ("t" , toString(*headEval(app(ISNIL(), NIL()))));
    EXPECT_EQ("f" , toString(*headEval(
        app(ISNIL(), app(app(CONS(), TRUE()), NIL()))
    )));
}
