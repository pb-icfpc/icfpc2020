#include "sig/mod.h"
#include "sig/dem.h"

#include <gtest/gtest.h>
#include <array>

TEST(modem, modem)
{
    static const std::array<std::string, 14> MESSAGES_MODULATED
    {
          "010", "01100001", "10100001", "01100010", "10100010", "1011011111111", "011110000100000000"
        , "00"
        , "110000"
        , "1101000"
        , "110110000101100010"
        , "1101100001110110001000"
        , "1101100001110110001000"
        , "1101100001111101100010110110001100110110010000"
    };

    static const std::array<std::string, 14> MESSAGES_DEMODULATED
    {
          "0", "1", "-1", "2", "-2", "-255", "256"
        , "nil"
        , "ap ap cons nil nil"
        , "ap ap cons 0 nil"
        , "ap ap cons 1 2"
        , "ap ap cons 1 ap ap cons 2 nil"
        , "ap ap cons 1 ap ap cons 2 nil"
        , "ap ap cons 1 ap ap cons ap ap cons 2 ap ap cons 3 nil ap ap cons 4 nil"
    };

    for (size_t i = 0; i < MESSAGES_MODULATED.size(); ++i)
    {
        EXPECT_EQ(MESSAGES_DEMODULATED.at(i), sig::dem(MESSAGES_MODULATED.at(i)));
        EXPECT_EQ(MESSAGES_MODULATED.at(i), sig::mod(MESSAGES_DEMODULATED.at(i)));
        EXPECT_EQ(MESSAGES_DEMODULATED.at(i), sig::dem(sig::mod(MESSAGES_DEMODULATED.at(i))));
        EXPECT_EQ(MESSAGES_MODULATED.at(i), sig::mod(sig::dem(MESSAGES_MODULATED.at(i))));
    }

    /*std::cout << sig::dem("1101000") << std::endl;
    std::cout << sig::dem("110110000111011111100001000100000010011100") << std::endl;*/
    /*std::cout << sig::mod("ap ap cons 3 ap ap cons 2793554277240046057 ap ap cons ap ap cons 0 ap ap cons 0 ap ap cons 0 ap ap cons 0 nil nil") << std::endl;
    std::cout << sig::dem("1101100011110111111111111111110001001101100010010110010110100000111001110101010100000011110100111110101101011010110100000") << std::endl;*/
}
