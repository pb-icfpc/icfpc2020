#include <gtest/gtest.h>

#include "lang/Evaluator.h"

TEST(evaluatorArithmetic, inc)
{
    const std::array<std::string, 3> ARGS = {"10", "0", "-10"};
    const std::array<std::string, 3> ANSWERS = {"11", "1", "-9"};

    for (size_t i = 0; i < ARGS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock(
            "galaxy = ap inc " + ARGS[i]
        );

        EXPECT_EQ(toString(*term), ANSWERS[i]);
    }
}

TEST(evaluatorArithmetic, dec)
{
    const std::array<std::string, 3> ARGS = {"10", "0", "-10"};
    const std::array<std::string, 3> ANSWERS = {"9", "-1", "-11"};

    for (size_t i = 0; i < ARGS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock(
            "galaxy = ap dec " + ARGS[i]
        );

        EXPECT_EQ(toString(*term), ANSWERS[i]);
    }
}

TEST(evaluatorArithmetic, add)
{
    const std::array<std::string, 3> ARGS = {"10 10", "-10 10", "-10 -10"};
    const std::array<std::string, 3> ANSWERS = {"20", "0", "-20"};

    for (size_t i = 0; i < ARGS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock(
            "galaxy = ap ap add " + ARGS[i]
        );

        EXPECT_EQ(toString(*term), ANSWERS[i]);
    }
}

TEST(evaluatorArithmetic, div)
{
    const std::array<std::string, 4> ARGS = {"10 10", "0 10", "9 4", "-5 3"};
    const std::array<std::string, 4> ANSWERS = {"1", "0", "2", "-1"};

    for (size_t i = 0; i < ARGS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock(
            "galaxy = ap ap div " + ARGS[i]
        );

        EXPECT_EQ(toString(*term), ANSWERS[i]);
    }
}

TEST(evaluatorArithmetic, mul)
{
    const std::array<std::string, 3> ARGS = {"-10 10", "-2 -3", "1 3"};
    const std::array<std::string, 3> ANSWERS = {"-100", "6", "3"};

    for (size_t i = 0; i < ARGS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock(
            "galaxy = ap ap mul " + ARGS[i]
        );

        EXPECT_EQ(toString(*term), ANSWERS[i]);
    }
}