#include <gtest/gtest.h>

#include "lang/Evaluator.h"
#include "lang/ListUtils.h"
#include "lang/toStringGalaxy.h"

TEST(evaluator, evaluate_ap_neg)
{
    auto term = lang::file_evaluator::evaluateBlock(
        "galaxy = ap neg 7"
    );

    EXPECT_EQ(toString(*term), "-7");
}

TEST(evaluator, sum_of_two_positive_numbers)
{
    auto term = lang::file_evaluator::evaluateBlock(
        "galaxy = ap ap add 7 3"
    );

    EXPECT_EQ(toString(*term), "10");
}

TEST(evaluator, sum_of_two_negative_numbers)
{
    auto term = lang::file_evaluator::evaluateBlock(
        "galaxy = ap ap add -7 -3"
    );

    EXPECT_EQ(toString(*term), "-10");
}

TEST(evaluator, sum_of_two_numbers)
{
    auto term = lang::file_evaluator::evaluateBlock(
        "galaxy = ap ap add 7 -3"
    );

    EXPECT_EQ(toString(*term), "4");
}

TEST(evaluator, car_of_the_list)
{
    static const std::string HEAD = "ap car";
    static const std::string TAIL = "ap cdr";

    static const std::string LIST = "ap ap cons 1 ap ap cons 2 ap ap cons nil ap ap cons nil nil";
    static const std::array<std::string, 4> ANSWERS = {"1", "2", "nil", "nil"};

    std::string command = HEAD;
    for (const std::string& answer : ANSWERS)
    {
        auto term = lang::file_evaluator::evaluateBlock
        (
            "galaxy = " + command + " " + LIST
        );
        EXPECT_EQ(answer, toString(*term));

        command += " " + TAIL;
    }
}

TEST(evaluator, powerOf2)
{
    int64_t result = 1;
    for (int i = 0; i < 10; ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock
        (
            "galaxy = ap pwr2 " + std::to_string(i)
        );

        EXPECT_EQ(std::to_string(result), toString(*term));
        result *= 2;
    }
}

TEST(evaluator, powerOf2_bigNumber)
{
    constexpr char ANSWER[] = "68351585149469122636640694597425667667286544715412888638305331450311031224980497600734786781970432";
    auto term = lang::file_evaluator::evaluateBlock
    (
        "galaxy = ap pwr2 325"
    );

    EXPECT_EQ(ANSWER, toString(*term));
}

TEST(evaluator, logical_equal)
{
    static const std::array<std::string, 4> ANSWERS = {"t", "f", "t", "f"};
    static const std::array<std::string, 4> ARG_PACKS = {"5 5", "10 11", "-1 -1", "-34 34"};

    for (size_t i = 0; i < ANSWERS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock
        (
            "galaxy = ap ap eq " + ARG_PACKS[i]
        );
        auto result = toString(*term);
        EXPECT_EQ(ANSWERS[i], result);
    }
}

TEST(evaluator, logical_less_than)
{
    static const std::array<std::string, 4> ANSWERS = {"f", "t", "f", "t"};
    static const std::array<std::string, 4> ARG_PACKS = {"5 5", "10 11", "-1 -1", "-34 34"};

    for (size_t i = 0; i < ANSWERS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock
            (
                "galaxy = ap ap lt " + ARG_PACKS[i]
            );
        auto result = toString(*term);
        EXPECT_EQ(ANSWERS[i], result);
    }
}

TEST(evaluator, logical_if0)
{
    static const std::array<std::string, 2> ANSWERS = {"1", "2"};
    static const std::array<std::string, 2> ARG_PACKS = {"0 1 2", "1 1 2"};

    for (size_t i = 0; i < ANSWERS.size(); ++i)
    {
        auto term = lang::file_evaluator::evaluateBlock
        (
            "galaxy = ap ap ap if0 " + ARG_PACKS[i]
        );
        auto result = toString(*term);
        EXPECT_EQ(ANSWERS[i], result);
    }
}


TEST(evaluator, pretty_print)
{
    static const std::string LIST = "ap ap cons 1 ap ap cons 2 ap ap cons ap ap cons 256 ap ap cons 1 ap ap cons ap ap cons 448 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons nil nil ap ap cons ap ap cons 0 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons 48 -40 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 326 ap ap cons 0 ap ap cons 10 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons -48 40 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 10 ap ap cons 10 ap ap cons 10 ap ap cons 10 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil";

    auto term = lang::file_evaluator::evaluateBlock("galaxy = " + LIST);
    printf("%s\n", lang::list::prettyPrint(term).c_str());
}

TEST(evaluator, pretty_print2)
{
    static const std::string LIST = "ap ap cons 0 ap ap cons 1234567 ap ap cons ap ap cons 100 500 nil";

    auto term = lang::file_evaluator::evaluateBlock("galaxy = " + LIST);
    printf("%s\n", lang::list::prettyPrint(term).c_str());
}

TEST(evaluator, to_string_galaxy)
{
    static const std::string LIST = "ap ap cons 1 ap ap cons 2 ap ap cons ap ap cons 256 ap ap cons 1 ap ap cons ap ap cons 448 ap ap cons 1 ap ap cons 64 nil ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons nil nil ap ap cons ap ap cons 0 ap ap cons ap ap cons 16 ap ap cons 128 nil ap ap cons ap ap cons ap ap cons ap ap cons 1 ap ap cons 0 ap ap cons ap ap cons 48 -40 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 326 ap ap cons 0 ap ap cons 10 ap ap cons 1 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil ap ap cons ap ap cons ap ap cons 0 ap ap cons 1 ap ap cons ap ap cons -48 40 ap ap cons ap ap cons 0 0 ap ap cons ap ap cons 10 ap ap cons 10 ap ap cons 10 ap ap cons 10 nil ap ap cons 0 ap ap cons 64 ap ap cons 1 nil ap ap cons nil nil nil nil nil";

    auto term = lang::file_evaluator::evaluateBlock("galaxy = " + LIST);
    EXPECT_EQ(toStringGalaxy(term), LIST);
}
