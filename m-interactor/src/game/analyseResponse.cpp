#include "analyseResponse.h"

#include "lang/Wrapper.h"
#include "lang/ListUtils.h"
#include "lang/Evaluator.h"

#include "framework/Assert.h"

namespace game
{
    namespace
    {
        const std::string GALAXY = "galaxy = ";
        const std::string HEAD = "ap car ";
        const std::string TAIL = "ap cdr ";

        PlayerRole getPlayerRoleFromString(const std::string& role)
        {
            if ("0" == role)
            {
                return PlayerRole::ATTACKER;
            }
            else if ("1" == role)
            {
                return PlayerRole::DEFENDER;
            }

            ASSERT(false, "[getPlayerRole] Strange game role.");
        }
    }

    bool isSuccess(const std::string& gameResponse)
    {
        const auto term = lang::file_evaluator::evaluateBlock("galaxy = ap car " + gameResponse);

        return "1" == toString(*term);
    }

    GameStage getGameStage(const std::string& gameResponse)
    {
        const auto term = lang::file_evaluator::evaluateBlock("galaxy = ap car ap cdr " + gameResponse);

        const std::string stage = toString(*term);
        if ("0" == stage)
        {
            return GameStage::NOT_STARTED;
        }
        else if ("1" == stage)
        {
            return GameStage::STARTED;
        }
        else if ("2" == stage)
        {
            return GameStage::FINISHED;
        }

        ASSERT(false, "[getGameState] Strange game stage.");
    }

    PlayerRole getPlayerRole(const std::string& gameResponse)
    {
        const auto term = lang::file_evaluator::evaluateBlock("galaxy = ap car ap cdr ap car ap cdr ap cdr " + gameResponse);
        return getPlayerRoleFromString(toString(*term));
    }

    GameState getGameState(const std::string& gameResponse)
    {
        using namespace lang;
        using namespace lang::list;

        GameState state;

        const auto gameStateTerm = lang::file_evaluator::evaluateBlock
        (
            GALAXY + HEAD + TAIL + TAIL + TAIL + gameResponse
        );

        state.tick = static_cast<size_t>(getIntChecked(head(gameStateTerm)));
        state.x1 = "x1";

        const auto shipsAndCommandsTerm = head(tail(tail(gameStateTerm)));

        const auto shipsAndCommandsVector = toVector(shipsAndCommandsTerm);
        for (const auto shipAndCommandsTerm : shipsAndCommandsVector)
        {
            ShipAndCommands shipAndCommands;
            Ship ship;

            const auto shipTerm = head(shipAndCommandsTerm);
            ship.role = getPlayerRoleFromString(tryGetNumber(head(shipTerm)).value());
            ship.id = tryGetNumber(head(tail(shipTerm))).value();

            const auto positionTerm = head(tail(tail(shipTerm)));
            ASSERT(isPairNumbers(positionTerm), "Wrong position term.");
            ship.pos.x = getIntChecked(head(positionTerm));
            ship.pos.y = getIntChecked(tail(positionTerm));

            const auto velocityTerm = head(tail(tail(tail(shipTerm))));
            ASSERT(isPairNumbers(velocityTerm), "Wrong velocity term.");
            ship.vel.x = getIntChecked(head(velocityTerm));
            ship.vel.y = getIntChecked(tail(velocityTerm));

            const auto x4Term = head(tail(tail(tail(tail(shipTerm)))));
            const auto petrolTerm = head(x4Term);
            ship.petrol = getIntChecked(petrolTerm);

            const auto x5Term = head(tail(tail(tail(tail(tail(shipTerm))))));
            ship.temperature = getIntChecked(x5Term);

            ship.x4 = "x4";
            ship.x5 = "x5";
            ship.x6 = "x6";
            ship.x7 = "x7";

            shipAndCommands.ship = ship;
            shipAndCommands.commands = "commands";
            if (PlayerRole::ATTACKER == ship.role)
            {
                state.att.push_back(shipAndCommands);
            }
            else
            {
                state.def.push_back(shipAndCommands);
            }
        }

        return state;
    }

    const ShipsAndCommands& getShipsAndCommandsForRole(const GameState& state, const PlayerRole role)
    {
        return (PlayerRole::ATTACKER == role)
            ? state.att
            : state.def;
    }

    ShipsAndCommands getOurShipsAndCommands(const std::string& gameResponse)
    {
        const PlayerRole ourRole = getPlayerRole(gameResponse);
        const GameState state = getGameState(gameResponse);

        return getShipsAndCommandsForRole(state, ourRole);
    }
}
