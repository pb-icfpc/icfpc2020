#pragma once

#include "PlayerRole.h"
#include "Vec.h"

#include <string>

namespace game
{
    struct Ship final
    {
        PlayerRole role;
        std::string id;
        Vec pos;
        Vec vel;
        int64_t petrol; // first of x4
        int64_t temperature; // = x5
        std::string x4;
        std::string x5;
        std::string x6;
        std::string x7;
    };
}
