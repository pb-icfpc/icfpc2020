#pragma once

#include <stddef.h>

namespace game
{
    enum class GameStage : size_t
    {
          NOT_STARTED = 0
        , STARTED = 1
        , FINISHED = 2
    };
}
