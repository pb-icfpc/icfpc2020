#pragma once

#include <cstdint>
#include <optional>
#include <ostream>

namespace game
{
    struct Vec final
    {
        int x;
        int y;
    };

    Vec operator*(const Vec& x, int l);

    Vec operator+(const Vec& a, const Vec& b);
    Vec& operator+=(Vec& a, const Vec& b);

    Vec operator-(const Vec& a, const Vec& b);
    Vec operator-(const Vec& x);
    Vec& operator-=(Vec& a, const Vec& b);

    int64_t scalarProduct(const Vec& a, const Vec& b);
    int64_t vectorProduct(const Vec& a, const Vec& b, std::optional<Vec> priorTo = std::nullopt);

    int sign(int64_t x);

    int linf(const Vec&);
    int lone(const Vec&);

    std::ostream& operator<<(std::ostream&, const Vec&);
}
