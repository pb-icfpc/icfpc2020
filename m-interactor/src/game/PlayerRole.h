#pragma once

#include <cstddef>
#include <string>

namespace game
{
    enum class PlayerRole : size_t
    {
          ATTACKER = 0
        , DEFENDER = 1
    };

    PlayerRole rev(PlayerRole);

    std::string toString(PlayerRole role);
}
