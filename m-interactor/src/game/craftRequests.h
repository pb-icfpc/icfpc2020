#pragma once

#include "Vec.h"

#include <string>

namespace game
{
    struct GlobalGameState;

    std::string craftJoinRequest(const std::string& playerKey);
    std::string craftStartRequest(const std::string& playerKey, const std::string& gameResponse);
    std::string craftCommandsRequest(const std::string& playerKey, const std::string& gameResponse, size_t tick, GlobalGameState&);
}
