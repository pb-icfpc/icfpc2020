#pragma once

#include "Vec.h"

namespace game
{
    constexpr Vec PLANET_CENTER = {0, 0};
    constexpr int PLANET_RADIUS = 16;
    constexpr int SAFE_ORBIT = 45; // ?
    constexpr int UNSAFE_ORBIT = 128; // ?

    Vec planetAcceleration(const Vec& pos);
}
