#pragma once

#include "Ship.h"
#include "Commands.h"

#include <vector>

namespace game
{
    struct ShipAndCommands final
    {
        Ship ship;
        Commands commands;
    };

    using ShipsAndCommands = std::vector<ShipAndCommands>;
}
