#include "Vec.h"

#include <algorithm>

namespace game
{
    Vec operator+(const Vec& a, const Vec& b)
    {
        return {a.x + b.x, a.y + b.y};
    }

    Vec operator*(const Vec& x, int l)
    {
        return Vec{x.x * l, x.y * l};
    }

    Vec& operator+=(Vec& a, const Vec& b)
    {
        a.x += b.x;
        a.y += b.y;

        return a;
    }

    Vec operator-(const Vec& a, const Vec& b)
    {
        return {a.x - b.x, a.y - b.y};
    }

    Vec operator-(const Vec& x)
    {
        return {-x.x, -x.y};
    }

    Vec& operator-=(Vec& a, const Vec& b)
    {
        a.x -= b.x;
        a.y -= b.y;

        return a;
    }

    int64_t scalarProduct(const Vec& a, const Vec& b)
    {
        return a.x * int64_t{b.x} + a.y * int64_t{b.y};
    }

    int64_t vectorProduct(const Vec& a, const Vec& b, std::optional<Vec> priorTo)
    {
        const Vec x = priorTo ? (a - *priorTo) : a;
        const Vec y = priorTo ? (b - *priorTo) : b;

        return x.x * int64_t{y.y} - y.x * int64_t{x.y};
    }

    int sign(int64_t x)
    {
        return x > int64_t{0} ? 1
            : x == int64_t{0} ? 0
                : -1;
    }

    int linf(const Vec& x)
    {
        return std::max(abs(x.x), abs(x.y));
    }

    int lone(const Vec& x)
    {
        return abs(x.x) + abs(x.y);
    }

    std::ostream& operator<<(std::ostream& out, const Vec& v)
    {
        return out << "Vec(" << v.x << "," << v.y << ")";
    }
}
