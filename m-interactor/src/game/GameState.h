#pragma once

#include "ShipsAndCommands.h"

#include <array>
#include <string>

namespace game
{
    struct GameState final
    {
        size_t tick;
        std::string x1;

        ShipsAndCommands att;
        ShipsAndCommands def;
    };
}
