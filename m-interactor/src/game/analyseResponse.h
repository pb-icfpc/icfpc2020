#pragma once

#include "GameState.h"
#include "GameStage.h"
#include "PlayerRole.h"

#include <string>

namespace game
{
    bool isSuccess(const std::string& gameResponse);
    GameStage getGameStage(const std::string& gameResponse);
    PlayerRole getPlayerRole(const std::string& gameResponse);
    GameState getGameState(const std::string& gameResponse);

    const ShipsAndCommands& getShipsAndCommandsForRole(const GameState&, const PlayerRole);
    ShipsAndCommands getOurShipsAndCommands(const std::string& gameResponse);
}
