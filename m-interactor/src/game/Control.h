#pragma once

#include "Ship.h"
#include "ShipsAndCommands.h"

#include "framework/Aliases.h"

#include <unordered_set>

namespace game
{
    // no guarantees on norm
    Vec orthogonalDirection(const Ship& ship, int tieBreaker);

    // no guarantees on norm
    Vec directionToMove(const Ship& ship);

    // |x| <= 1, |y| <= 1
    Vec acceleration(const Ship& ship, int tick);

    Opt<std::string> explodeCandidateId
    (
        const ShipsAndCommands& ourShips
        , const ShipsAndCommands& enemyShips
        , const std::unordered_set<std::string>& doNotExplodeIds = {}
    );

    std::vector<Vec> get_trajectory(const Ship& ship, int n);
    std::vector<Vec> get_trajectory(Vec init_pos, Vec init_vel, int n);

    double angle(Vec a, Vec b); // degree
    double angleScore(Vec a, Vec b); // lower == better
}
