#include "craftRequests.h"

#include "Control.h"
#include "Planet.h"
#include "analyseResponse.h"
#include "GlobalGameState.h"
#include "GameState.h"

#include "framework/Aliases.h"
#include "framework/Assert.h"

#include "Vec.h"

#include <cmath>

namespace game
{
    namespace
    {
        const std::string ATTACKER_PARAMS         = "ap ap cons 125 ap ap cons 64 ap ap cons 10 ap ap cons 4 nil";
        const std::string ATTACKER_PARAMS_HALF    = "ap ap cons 32 ap ap cons 0 ap ap cons 2 ap ap cons 2 nil";
        const std::string ATTACKER_PARAMS_QUARTER = "ap ap cons 5 ap ap cons 0 ap ap cons 1 ap ap cons 1 nil";

        const std::string DEFENDER_PARAMS         = "ap ap cons 320 ap ap cons 0 ap ap cons 10 ap ap cons 4 nil";
        const std::string DEFENDER_PARAMS_HALF    = "ap ap cons 128 ap ap cons 0 ap ap cons 5 ap ap cons 2 nil";
        const std::string DEFENDER_PARAMS_QUARTER = "ap ap cons 32 ap ap cons 0 ap ap cons 1 ap ap cons 1 nil";

        int64_t manhDist(const Vec& lhs, const Vec& rhs)
        {
            return abs(lhs.x - rhs.x) + abs(lhs.y - rhs.y);
        }
    }

    std::string craftJoinRequest(const std::string& playerKey)
    {
        // (2, playerKey, (...unknown list...))
        return "ap ap cons 2 ap ap cons " + playerKey + " ap ap cons nil nil";
    }

    std::string craftStartRequest(const std::string& playerKey, const std::string& gameResponse)
    {
        const std::string& params = (PlayerRole::ATTACKER == getPlayerRole(gameResponse))
            ? ATTACKER_PARAMS
            : DEFENDER_PARAMS;

        //(3, playerKey, (<number1>, <number2>, <number3>, <number4>))
        return "ap ap cons 3 ap ap cons " + playerKey + " ap ap cons "
             + params
             + " nil";
    }

    std::string craftCommandsList(const std::vector<std::string>& commands)
    {
        std::string result = "";

        for (const auto& command : commands)
        {
            result += "ap ap cons " + command + " ";
        }
        result += "nil";

        return result;
    }

    std::string craftCommandAccelerate(const std::string& ship_id, const Vec& acc)
    {
        static const char * const PARAMS_TEMPLATE = "ap ap cons 0 ap ap cons %s ap ap cons ap ap cons %d %d nil";

        constexpr size_t BUFFER_SIZE = 2048;
        static char buffer[BUFFER_SIZE] = {0, };

        snprintf(buffer, BUFFER_SIZE, PARAMS_TEMPLATE, ship_id.c_str(), acc.x, acc.y);

        return std::string{buffer};
    }

    std::string craftCommandShoot(const std::string& ship_id, const Ship& enemyShip)
    {
        static const char * const PARAMS_TEMPLATE = "ap ap cons 2 ap ap cons %s ap ap cons ap ap cons %d %d ap ap cons 64 nil";

        constexpr size_t BUFFER_SIZE = 2048;
        static char buffer[BUFFER_SIZE] = {0, };

        const Vec target = enemyShip.pos + enemyShip.vel + planetAcceleration(enemyShip.pos);
        snprintf(buffer, BUFFER_SIZE, PARAMS_TEMPLATE, ship_id.c_str(), target.x, target.y);

        return std::string{buffer};
    }

    std::string craftCommandClone(const std::string& ship_id, const std::string& cloneParams)
    {
        static const char * const PARAMS_TEMPLATE = "ap ap cons 3 ap ap cons %s ap ap cons %s nil";

        constexpr size_t BUFFER_SIZE = 2048;
        static char buffer[BUFFER_SIZE] = {0, };

        snprintf(buffer, BUFFER_SIZE, PARAMS_TEMPLATE, ship_id.c_str(), cloneParams.c_str());

        return std::string{buffer};
    }

    std::string craftCommandExplode(const std::string& ship_id)
    {
        static const char * const PARAMS_TEMPLATE = "ap ap cons 1 ap ap cons %s nil";

        constexpr size_t BUFFER_SIZE = 2048;
        static char buffer[BUFFER_SIZE] = {0, };

        snprintf(buffer, BUFFER_SIZE, PARAMS_TEMPLATE, ship_id.c_str());

        return std::string{buffer};
    }

    std::string craftCommandsRequest(const std::string& playerKey, const std::string& gameResponse, const size_t tick, GlobalGameState& globalState)
    {
        const PlayerRole ourRole = getPlayerRole(gameResponse);
        const GameState state = getGameState(gameResponse);

        const auto ourShips = getShipsAndCommandsForRole(state, ourRole);
        const auto enemyShips = getShipsAndCommandsForRole(state, rev(ourRole));
        ASSERT(not ourShips.empty(), "[craftCommandsRequest] No our ships.");
        ASSERT(not enemyShips.empty(), "[craftCommandsRequest] No enemy ships.");

        Opt<std::string> ourShipExplodeId = (PlayerRole::DEFENDER == ourRole)
            ? std::nullopt
            : explodeCandidateId(ourShips, enemyShips, std::unordered_set{globalState.motherShipId});

        std::vector<std::string> commands;
        if (0 == tick)
        {
            const Ship& ship = ourShips.at(0).ship;
            const std::string& splitParams = (PlayerRole::ATTACKER == ourRole)
                ? ATTACKER_PARAMS_HALF
                : DEFENDER_PARAMS_HALF;
            commands.emplace_back(craftCommandClone(ship.id, splitParams));

            globalState.motherShipId = ship.id;
        }
        else if (6 == tick)
        {
            for (const ShipAndCommands& shipAndCommands : ourShips)
            {
                const Ship& ship = shipAndCommands.ship;
                const std::string& splitParams = (PlayerRole::ATTACKER == ourRole)
                    ? ATTACKER_PARAMS_QUARTER
                    : DEFENDER_PARAMS_QUARTER;
                commands.emplace_back(craftCommandClone(ship.id, splitParams));
            }
        }
        else
        {
            for (const ShipAndCommands& shipAndCommands : ourShips)
            {
                const Ship& ship = shipAndCommands.ship;
                if (ourShipExplodeId && ourShipExplodeId.value() == ship.id)
                {
                    commands.emplace_back(craftCommandExplode(ship.id));
                    continue;
                }

                commands.emplace_back(craftCommandAccelerate(ship.id, acceleration(ship, tick)));

                // We need to be cool. Do not shoot while thrusting.
                const bool readyToShoot =
                    (PlayerRole::ATTACKER == ourRole)
                    && ((ship.petrol > 5) || (0 == ship.petrol))
                    && (ship.temperature < 20);

                if (readyToShoot)
                {
                    for (const ShipAndCommands& enemyShipAndCommands : enemyShips)
                    {
                        const Ship& enemyShip = enemyShipAndCommands.ship;

                        /*const int64_t dist = manhDist(ship.pos, enemyShip.pos);
                        const bool toShoot = (dist < 55);*/

                        // Look at the diagram from Galaxy Pad UI.
                        Vec dir = enemyShip.pos - ship.pos; dir.x = abs(dir.x); dir.y = abs(dir.y);
                        Vec minmax; minmax.x = std::min(dir.x, dir.y); minmax.y = std::max(dir.x, dir.y);
                        bool toShoot = (0 == minmax.x);
                        if (not toShoot)
                        {
                            constexpr double EPS = 0.25;
                            const long double ratio = static_cast<long double>(minmax.x) / minmax.y;
                            toShoot = (ratio < EPS) || ((1.0L - ratio) < EPS);
                        }

                        if (toShoot)
                        {
                            commands.emplace_back(craftCommandShoot(ship.id, enemyShip));
                            break;
                        }
                    }
                }
            }
        }

        //(4, playerKey, (... ship commands? ...))
        return "ap ap cons 4 ap ap cons " + playerKey + " ap ap cons "
             + craftCommandsList(commands)
             + " nil";
    }
}
