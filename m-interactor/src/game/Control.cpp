#include "Control.h"

#include "Planet.h"

#include "framework/Assert.h"

#include <tuple>
#include <algorithm>
#include <map>
#include <cassert>
#include <cmath>

namespace game
{
    Vec orthogonalDirection(const Ship& ship, int tieBreaker)
    {
        const Vec delta = ship.pos - PLANET_CENTER;

        const Vec c1 = Vec{delta.y, -delta.x};
        const Vec c2 = Vec{-delta.y, delta.x};

        ASSERT(scalarProduct(c1, delta) == 0, ":(");
        ASSERT(scalarProduct(c2, delta) == 0, ":(");

        // for start position
        if (linf(ship.vel) <= 3)
        {
            if (tieBreaker == sign(vectorProduct(delta, c1)))
            {
                return c1;
            }
            else
            {
                return c2;
            }
        }

        if (sign(vectorProduct(delta, c1)) == sign(vectorProduct(delta, ship.vel)))
        {
            return c1;
        }

        return c2;
    }

    //Vec directionToMove(const Ship& ship)
    //{
    //    constexpr int DELTA = 10;
    //    constexpr int FORECAST = 5;

    //    Vec result = ship.pos;

    //    const auto better = [&ship, FORECAST](const Vec& x, const Vec& y)
    //    {
    //        auto tier = [&ship, &FORECAST](const Vec& t)
    //        {
    //            return std::tuple
    //            (
    //                abs(linf(t) - SAFE_ORBIT),
    //                -std::min(linf(t - ship.pos), FORECAST),
    //                linf(t - ship.pos) / 3,
    //                scalarProduct(t - ship.pos, ship.vel)
    //            );
    //        };

    //        return tier(x) < tier(y);
    //    };

    //    for (int dx = -DELTA; dx <= DELTA; ++dx)
    //    {
    //        for (int dy = -DELTA; dy <= DELTA; ++dy)
    //        {
    //            const Vec cand = ship.vel + Vec{dx, dy};

    //            if (better(cand, result))
    //            {
    //                result = cand;
    //            }
    //        }
    //    }

    //    return result - ship.pos;
    //}

    namespace
    {
        int distance(const Vec& a, const Vec& b)
        {
            return linf(b - a);
        }
    }

    Vec acceleration(const Ship& ship, int tick)
    {
        int forecast = 50;

        if (forecast == 0)
        {
            return Vec {0, 0};
        }

        const size_t ship_id = std::atol(ship.id.c_str());

        using Trajectory = std::vector<Vec>;

        const auto min_distance_to_planet = [](const Trajectory& trajectory) -> int
        {
            assert(trajectory.size() > 0);

            int min_distance = 100000;
            for (Vec v : trajectory)
            {
                min_distance = std::min(min_distance, linf(v));
            }

            return min_distance;
        };

        const Trajectory default_trajectory = get_trajectory(ship, forecast);
        if (PLANET_RADIUS < min_distance_to_planet(default_trajectory))
        {
            return Vec {0, 0};
        }

        const int F =  (tick < 20 ) ? 20 : 1;
        const auto score_trajectory = [F, tick](const Trajectory& trajectory) -> int
        {
            int step = 0;
            Vec prevV = trajectory[0];
            for (const Vec& v : trajectory)
            {
                if (PLANET_RADIUS >= linf(v))
                {
                    int correction = 0;

                    Vec dv = v - prevV;
                    return 10000 * (step / F) + step + sign(dv.x) * v.x + sign(dv.y) * v.y + ((0 == linf(trajectory[0] - trajectory[1])) ? -100 : 0);
                }

                if (UNSAFE_ORBIT < linf(v))
                {
                    return 10000 * (step / F) - lone(v);
                }

                step += 1;
                prevV = v;
            }

            return 10000 * (step / F);
        };

        int best_score = std::numeric_limits<int>::min();
        std::vector<Vec> candidates;

        for (int dx = -1; dx <= +1; ++dx)
        {
            for (int dy = -1; dy <= +1; ++dy)
            {
                if (dx == 0 and dy == 0) { continue; }

                Vec acceleration = Vec{dx, dy};
                int score = score_trajectory(get_trajectory(ship.pos, ship.vel + acceleration, forecast));
                if (score == best_score)
                {
                    candidates.push_back(-acceleration);
                    best_score = score;
                }
                else if(score > best_score)
                {
                    candidates.clear();
                    candidates.push_back(-acceleration);
                    best_score = score;
                }
            }
        }

        ASSERT(candidates.size() > 0, "Logic error in Control.cpp");

        return candidates[ship_id % candidates.size()];

        // const size_t STRATEGIES_COUNT = 4;

        // std::array<int, STRATEGIES_COUNT> MAX_VELOCITIES = {5, 5, 6, 6};
        // std::array<int, STRATEGIES_COUNT> SIGNS = {1, -1, 1, -1};
        // const size_t distToPlanetCenter = distance(ship.pos, PLANET_CENTER);

        // Vec dir = orthogonalDirection(ship, SIGNS[strategy]);

        // if
        // (
        //     distToPlanetCenter <= 40
        //     && linf(ship.vel) > MAX_VELOCITIES[strategy]
        // )
        // {
        //     return Vec{0, 0};
        // }

        // if (distToPlanetCenter > 40 && linf(ship.vel) > MAX_VELOCITIES[strategy])
        // {
        //     dir = PLANET_CENTER - ship.pos;
        // }

        // Vec result = ship.vel;

        // const auto better = [&dir](const Vec& x, const Vec& y)
        // {
        //     const auto spx = scalarProduct(x, dir);
        //     const auto spy = scalarProduct(y, dir);

        //     return spx > spy;
        // };

        // for (int dx = -1; dx <= 1; ++dx)
        // {
        //     for (int dy = -1; dy <= 1; ++dy)
        //     {
        //         const Vec cand = ship.vel + Vec{dx, dy};

        //         if (better(cand, result))
        //         {
        //             result = cand;
        //         }
        //     }
        // }

        // return ship.vel - result;
    }

    struct ExplodeData final
    {
        size_t our_id;
        size_t exploded;
    };

    Opt<ExplodeData> explodeCount
    (
        const std::vector<Vec>& ourShips,
        const std::vector<Vec>& enemyShips,
        const std::unordered_set<size_t>& doNotExplodeIds
    )
    {
        using Dist = uint64_t;
        constexpr uint64_t MAX_DISTANCE_TO_ENEMY = 7;

        if (ourShips.size() == 1)
        {
            return std::nullopt;
        }

        std::map<size_t, ExplodeData> candidatesToBoom;

        for (size_t i = 0; i < ourShips.size(); ++i)
        {
            const Vec& our = ourShips[i];

            ExplodeData data{i, 0};

            for (const Vec& enemy : enemyShips)
            {
                const Dist dist = distance(our, enemy);
                if (dist <= MAX_DISTANCE_TO_ENEMY && doNotExplodeIds.count(i) == 0)
                {
                    ++data.exploded;
                }
            }

            if (data.exploded > 0)
            {
                candidatesToBoom.emplace(data.exploded, data);
            }
        }

        constexpr uint64_t MAX_DISTANCE_TO_OUR = 11;

        Opt<ExplodeData> bestCandidate;

        for (const auto& [exploded, candidate] : candidatesToBoom)
        {
            bool closeToOurs = false;
            for (size_t i = 0; i < ourShips.size(); ++i)
            {
                if (i == candidate.our_id)
                {
                    continue;
                }

                const Dist dist = distance(ourShips[i], ourShips[candidate.our_id]);
                if (dist <= MAX_DISTANCE_TO_OUR)
                {
                    closeToOurs = true;
                    break;
                }
            }

            if (not closeToOurs)
            {
                bestCandidate.emplace(candidate);
            }
        }

        return bestCandidate;
    }

    Opt<std::string> explodeCandidateId
    (
        const ShipsAndCommands& ourShips
        , const ShipsAndCommands& enemyShips
        , const std::unordered_set<std::string>& doNotExplodeIds
    )
    {
        constexpr size_t FORECAST = 5;

        std::vector<ExplodeData> bestCandidates;
        bestCandidates.reserve(FORECAST);

        size_t bestExplodeCount = 0;
        Opt<size_t> bestStep;
        Opt<std::string> bestCandidate;
        for (size_t i = 0; i < FORECAST; ++i)
        {
            std::unordered_set<size_t> notExplode;
            std::vector<Vec> our;
            our.reserve(ourShips.size());
            for (const ShipAndCommands& v : ourShips)
            {
                our.push_back(get_trajectory(v.ship, i + 1).back());

                if (doNotExplodeIds.count(v.ship.id) != 0)
                {
                    notExplode.emplace(our.size() - 1);
                }
            }

            std::vector<Vec> enemy;
            enemy.reserve(enemyShips.size());
            for (const ShipAndCommands& v : enemyShips)
            {
                enemy.push_back(get_trajectory(v.ship, i + 1).back());
            }

            Opt<ExplodeData> explodeData = explodeCount(our, enemy, notExplode);
            if (explodeData.has_value())
            {
                if (explodeData->exploded > bestExplodeCount)
                {
                    bestExplodeCount = explodeData->exploded;
                    bestStep = i;
                    bestCandidate = ourShips[explodeData->our_id].ship.id;
                }
            }
        }

        if (bestStep.has_value() && *bestStep == 0)
        {
            return bestCandidate;
        }

        return std::nullopt;
    }

    std::vector<Vec> get_trajectory(const Ship& ship, int n)
    {
        return get_trajectory(ship.pos, ship.vel, n);
    }

    std::vector<Vec> get_trajectory(Vec pos, Vec vel, int n)
    {
        std::vector<Vec> result;
        result.push_back(pos);

        for (int i = 0; i < n; ++i)
        {
            vel += planetAcceleration(pos);
            pos += vel;

            result.push_back(pos);
        }

        return result;
    }

    double angle(Vec a, Vec b)
    {
        constexpr double pi = 3.14159265358979323846;
        const Vec x = a - b;
        return (std::acos(std::abs(x.x / std::sqrt(std::pow(x.x, 2) + std::pow(x.y, 2)))) * 180) / pi;
    }

    double angleScore(Vec a, Vec b)
    {
        static const std::array GOOD_ANGLES
        {
            0.0,
            45.0,
            90.0
        };

        const double ang = angle(a, b);

        const std::array delta
        {
            std::abs(GOOD_ANGLES[0] - ang),
            std::abs(GOOD_ANGLES[1] - ang),
            std::abs(GOOD_ANGLES[2] - ang)
        };

        return std::min(std::min(delta[0], delta[1]), delta[2]);
    }
}
