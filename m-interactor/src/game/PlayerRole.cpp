#include "PlayerRole.h"

#include "framework/Assert.h"

namespace game
{
    PlayerRole rev(PlayerRole role)
    {
        return (PlayerRole::ATTACKER == role)
            ? PlayerRole::DEFENDER
            : PlayerRole::ATTACKER;
    }

    std::string toString(const PlayerRole role)
    {
        switch (role)
        {
            case PlayerRole::ATTACKER: {return "attacker";}
            case PlayerRole::DEFENDER: {return "defender";}
        }

        ASSERT(false, "[toString(PlayerRole)] Strange player role.");
    }
}
