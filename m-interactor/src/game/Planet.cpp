#include "Planet.h"

#include <algorithm>

namespace game
{
    Vec planetAcceleration(const Vec& pos)
    {
        if (abs(pos.x) == abs(pos.y))
        {
            return Vec{-sign(pos.x), -sign(pos.y)};
        }

        if (abs(pos.x) > abs(pos.y))
        {
            return Vec{-sign(pos.x), 0};
        }

        return Vec{0, -sign(pos.y)};
    }
}
