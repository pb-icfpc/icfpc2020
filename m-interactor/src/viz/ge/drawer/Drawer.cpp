#include "Drawer.h"

#include "ge/render/render.h"
#include "ge/window/window.h"
#include "ge/render_loop/render_loop.h"

#include <vulkan/vulkan.hpp>

#include <random>
#include <iostream>

namespace
{
    constexpr ge::Color BL{{0.f, 0.f, 0.f}};
    constexpr ge::Color WH{{1.f, 1.f, 1.f}};

    ge::Color getRandomColor()
    {
        static std::mt19937 gen(std::random_device{}());
        std::uniform_real_distribution<> dis(0.3, 1.0);
        return ge::Color{{dis(gen), dis(gen), dis(gen)}};
    }

    ge::PointsList convert(const Points& points)
    {
        ge::PointsList result;
        result.points.reserve(points.size());

        result.point_color = WH;
        result.empty_cell_color = BL;

        for (const auto& [x, y] : points)
        {
            result.points.push_back(ge::Point{x, -y});
        }

        return result;
    }

    const ge::Grid& emptyCanvas()
    {
        static const Table table = []
        {
            Table result(1, 1);
            result.data = std::vector<char>{0};
            return result;
        }();
        static const ge::Grid grid{table, BL, WH};
        return grid;
    }
}

namespace viz
{
    struct Drawer::DataImpl final
    {
        std::optional<std::vector<ge::PointsList>> mDrawTask;
        std::vector<ge::PointsList> mAllPoints;
    };

    Drawer::Drawer()
        : mData{new Drawer::DataImpl{}}
        , mRandomColorEnabled{false}
    {
        mThread = std::thread
        (
            [this]
            {
                try
                {
                    constexpr uint16_t width = 500;
                    constexpr uint16_t height = 500;
                    constexpr ge::DynamicSize size
                    {
                        .default_size = ge::Size{width, height}
                        , .min_size = ge::Size{100, 100}
                        , .max_size = std::nullopt
                    };
                    constexpr std::array<uint8_t, 4> background_color{38, 38, 38, 1};

                    auto window = ge::Window::create(size, background_color);

                    ge::Render render
                    (
                        ge::SurfaceParams
                        {
                            .surface_creator = [&window] (const vk::Instance& instance)
                            {
                                return window->create_surface(instance);
                            }
                            , .width = width
                            , .height = height
                            , .background_color = background_color
                        }
                    );

                    window->start_display();
                    render.set_camera_scale(1.f / 27.f);

                    render.set_object_to_draw(emptyCanvas());
                    render.draw_frame();

                    ge::RenderLoop::RightButtonClickedCallback right_button_clicked
                    {
                        [this] (ge::Point point)
                        {
                            std::lock_guard lk{mMutex};
                            mLastClickedPoint.emplace(point.x, point.y);
                            mRightButtonClicked.notify_all();

                            std::cout << "x: " << point.x << " y: " << point.y << std::endl;
                        }
                    };

                    ge::RenderLoop render_loop(*window, render);
                    while (not render_loop.stopped())
                    {
                        {
                            std::lock_guard lk{mMutex};
                            if (mData->mDrawTask.has_value())
                            {
                                if (mData->mDrawTask->empty())
                                {
                                    render.set_object_to_draw(emptyCanvas());
                                }
                                else
                                {
                                    render.set_object_to_draw(*(mData->mDrawTask), BL);
                                }

                                render.draw_frame();
                                mData->mDrawTask.reset();
                            }
                        }

                        render_loop.handle_window_events(right_button_clicked);
                        std::this_thread::sleep_for(std::chrono::milliseconds(10));
                    }
                }
                catch (const std::exception& e)
                {
                    std::cout << "Render failed: " << e.what() << std::endl;
                }
            }
        );
    }

    Drawer::~Drawer()
    {
        if (mThread.joinable())
        {
            mThread.join();
        }
    }

    void Drawer::draw(Points points) const
    {
        std::lock_guard lk{mMutex};

        ge::PointsList list = convert(points);

        if (mRandomColorEnabled)
        {
            list.point_color = getRandomColor();
        }
//        for (ge::PointsList& list : mAllPoints)
//        {
//            ge::Color& color = list.point_color;
//            color.color.r *= 0.9;
//            color.color.g *= 0.9;
//            color.color.b *= 0.9;
//        }

        mData->mAllPoints.emplace_back(std::move(list));

        mData->mDrawTask.emplace(mData->mAllPoints);
    }

    Point Drawer::drawAndWaitClick(Points points) const
    {
        draw(std::move(points));

        std::unique_lock lk{mMutex};
        mRightButtonClicked.wait
        (
            lk
            , [this]
            {
                return mLastClickedPoint.has_value();
            }
        );

        const Point result = mLastClickedPoint.value();
        mLastClickedPoint.reset();
        return result;
    }

    std::optional<Point> Drawer::getBottomLeftCorner() const
    {
        std::optional<int64_t> xMin, yMin;

        for (const auto& list : mData->mAllPoints)
        {
            for (const ge::Point& p : list.points)
            {
                xMin = xMin ? std::min(*xMin, p.x) : p.x;
                yMin = yMin ? std::min(*yMin, p.y) : p.y;
            }
        }

        if (xMin)
            return Point{*xMin, -*yMin};
        return std::nullopt;
    }

    void Drawer::enableRandomColor(bool isEnabled) const
    {
        mRandomColorEnabled = isEnabled;
    }

    void Drawer::clear() const
    {
        std::lock_guard lk{mMutex};
        mData->mAllPoints.clear();

        mData->mDrawTask.emplace(std::vector<ge::PointsList>{});
    }

    const Drawer& getDrawerSingleton()
    {
        static Drawer drawer;
        return drawer;
    }
}
