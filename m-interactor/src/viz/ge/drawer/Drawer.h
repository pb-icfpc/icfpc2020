#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>

#include "framework/Points.h"

namespace viz
{
    class Drawer final
    {
    public:
        explicit Drawer();
        ~Drawer();

        void draw(Points) const;
        Point drawAndWaitClick(Points) const;
        std::optional<Point> getBottomLeftCorner() const;

        void clear() const;

        void enableRandomColor(bool isEnabled) const;

    private:
        std::thread mThread;

        mutable std::mutex mMutex;
        mutable std::optional<::Point> mLastClickedPoint;
        mutable std::condition_variable mRightButtonClicked;

        struct DataImpl;
        std::unique_ptr<DataImpl> mData;

        mutable bool mRandomColorEnabled;
    };

    const Drawer& getDrawerSingleton();
}
