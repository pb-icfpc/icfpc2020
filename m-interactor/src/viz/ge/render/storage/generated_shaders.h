#pragma once

#include <vulkan/vulkan.hpp>

#include <span>

#include <cstdint>

namespace ge
{
    enum class ShaderName
    {
        line_2d_camera_Vertex
        , point_2d_camera_Vertex
        , simple_color_Fragment
    };

    std::span<const uint32_t> get_shader(ShaderName);
    vk::ShaderStageFlagBits get_shader_kind(ShaderName);
}
