#include "ge/render/render.h"
#include "ge/render/render_impl.h"

#include <vulkan/vulkan.h>

namespace ge
{
    Render::Render
    (
        const SurfaceParams& surface_params
    )
        : impl_
        (
            new RenderImpl
            (
                surface_params
            )
        )
        , origin_offset_(Point{0, 0})
    {
    }

    Render::~Render() = default;

    void Render::set_object_to_draw(const std::vector<PointsList>& points_list, const ge::Color& zero_color)
    {
        int64_t min_x = std::numeric_limits<int64_t>::max();
        int64_t min_y = std::numeric_limits<int64_t>::max();
        int64_t max_x = std::numeric_limits<int64_t>::min();
        int64_t max_y = std::numeric_limits<int64_t>::min();
        for (const PointsList& points : points_list)
        {
            for (const Point& point : points.points)
            {
                min_x = std::min(min_x, point.x);
                max_x = std::max(max_x, point.x);
                min_y = std::min(min_y, point.y);
                max_y = std::max(max_y, point.y);
            }
        }

        origin_offset_ = Point{min_x, min_y};

        const size_t rows = max_y - min_y + 1;
        const size_t cols = max_x - min_x + 1;
        Table table(rows, cols);

        for (const PointsList& points : points_list)
        {
            for (const Point& point : points.points)
            {
                const int64_t x = point.x - origin_offset_->x;
                const int64_t y = point.y - origin_offset_->y;
                assert(x >= 0);
                assert(x < table.columns);
                assert(y >= 0);
                assert(y < table.rows);
                table.get(y, x) = 1;
            }
        }

        std::vector<Vertex> points;
        points.reserve(table.data.size());

        std::vector<Graph::Vertice> vertices;
        vertices.reserve(table.data.size());

        for (size_t row = 0; row < table.rows; ++row)
        {
            for (size_t col = 0; col < table.columns; ++col)
            {
                if (table.get(row, col) == 0)
                {
                    const size_t ind = points.size();
                    points.push_back(Vertex{{static_cast<float>(col), static_cast<float>(row)}});
                    vertices.push_back(Graph::Vertice{ind, zero_color});
                }
            }
        }

        for (const PointsList& list : points_list)
        {
            for (const Point& point : list.points)
            {
                const int64_t x = point.x - origin_offset_->x;
                const int64_t y = point.y - origin_offset_->y;

                const size_t ind = points.size();
                points.push_back(Vertex{{static_cast<float>(x), static_cast<float>(y)}});
                vertices.push_back(Graph::Vertice{ind, list.point_color});
            }
        }

        const ge::Graph graph
        {
            points
          , vertices
          , std::array<ge::Graph::Arc, 0>{}
        };

        set_object_to_draw(graph);
    }

    void Render::set_object_to_draw(const PointsList& list)
    {
        int64_t min_x = std::numeric_limits<int64_t>::max();
        int64_t min_y = std::numeric_limits<int64_t>::max();
        int64_t max_x = std::numeric_limits<int64_t>::min();
        int64_t max_y = std::numeric_limits<int64_t>::min();
        for (const Point& point : list.points)
        {
            min_x = std::min(min_x, point.x);
            max_x = std::max(max_x, point.x);
            min_y = std::min(min_y, point.y);
            max_y = std::max(max_y, point.y);
        }

        origin_offset_ = Point{min_x, min_y};

        const size_t rows = max_y - min_y + 1;
        const size_t cols = max_x - min_x + 1;
        Table table(rows, cols);

        for (const Point& point : list.points)
        {
            const int64_t x = point.x - origin_offset_->x;
            const int64_t y = point.y - origin_offset_->y;
            table.get(y, x) = 1;
        }

        Grid grid{table, list.empty_cell_color, list.point_color};
        set_object_to_draw(grid);
    }

    Point Render::origin_offset() const
    {
        assert(origin_offset_.has_value());
        return origin_offset_.value();
    }

    void Render::set_object_to_draw(const Grid& grid)
    {
        std::vector<Vertex> points;
        points.reserve(grid.table.data.size());

        std::vector<Graph::Vertice> vertices;
        vertices.reserve(grid.table.data.size());

        for (size_t row = 0; row < grid.table.rows; ++row)
        {
            for (size_t col = 0; col < grid.table.columns; ++col)
            {
                const size_t ind = points.size();
                points.push_back(Vertex{{static_cast<float>(col), static_cast<float>(row)}});

                const ge::Color& color = grid.table.get(row, col) == 0
                    ? grid.zero_color
                    : grid.one_color;
                vertices.push_back(Graph::Vertice{ind, color});
            }
        }

        const ge::Graph graph
        {
            points
          , vertices
          , std::array<ge::Graph::Arc, 0>{}
        };

        set_object_to_draw(graph);
    }

    void Render::set_object_to_draw(const Graph& graph)
    {
        impl_->set_object_to_draw(graph);
    }

    void Render::draw_frame()
    {
        impl_->draw_frame();
    }

    void Render::resize(const uint16_t new_surface_width, const uint16_t new_surface_height)
    {
        impl_->resize(new_surface_width, new_surface_height);
    }

    glm::vec2 Render::camera_pos() const
    {
        return impl_->camera_pos();
    }

    void Render::set_camera_pos(const glm::vec2& pos)
    {
        impl_->set_camera_pos(pos);
    }

    float Render::camera_scale() const
    {
        return impl_->camera_scale();
    }

    void Render::set_camera_scale(float scale)
    {
        impl_->set_camera_scale(scale);
    }

    glm::vec2 Render::normalize_in_proj_space(const glm::vec2& coord) const
    {
        return impl_->normalize_in_proj_space(coord);
    }

    glm::vec2 Render::proj_to_model_space(const glm::vec2& coord) const
    {
        return impl_->proj_to_model_space(coord);
    }
}
