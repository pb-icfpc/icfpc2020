#pragma once

#include "framework/Table.h"

#include "ge/render/vertex.h"

namespace ge
{
    struct Grid final
    {
        Table table;
        ge::Color zero_color;
        ge::Color one_color;
    };
}
