#pragma once

#include "ge/render/vertex.h"

#include <vector>

namespace ge
{
    struct Point final
    {
        int64_t x;
        int64_t y;
    };

    struct PointsList final
    {
        std::vector<Point> points;
        ge::Color point_color;
        ge::Color empty_cell_color;
    };
}

