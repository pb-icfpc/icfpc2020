#pragma once

#include "ge/render/points_list.h"

#include <glm/vec2.hpp>

#include <optional>
#include <functional>

namespace ge
{
    class Window;
    class Render;

    class RenderLoop final
    {
    public:
        using RightButtonClickedCallback = std::function<void(Point)>;

        RenderLoop(ge::Window& window, ge::Render& render);

        bool stopped() const;
        void handle_window_events();
        void handle_window_events(const RightButtonClickedCallback&);

    private:
        using ProjVec2 = glm::vec2;
        using ModelVec2 = glm::vec2;

        template <typename T>
        void handle_window_event(const T&);

        ge::Window& window_;
        ge::Render& render_;
        bool stopped_;

        std::optional<ModelVec2> prev_move_mouse_pos_;
        bool need_draw_;

        std::optional<std::reference_wrapper<const RightButtonClickedCallback>> rb_clicked_callback_;
    };
}
