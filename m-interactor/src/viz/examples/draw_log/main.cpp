#include "ge/render/render.h"
#include "ge/window/window.h"
#include "ge/render_loop/render_loop.h"

#include <vulkan/vulkan.hpp>

#include <thread>
#include <fstream>
#include <regex>
#include <span>
#include <iostream>
#include <fstream>
#include <regex>

namespace points
{
    constexpr ge::Color BL{{0.f, 0.f, 0.f}};
    constexpr ge::Color WH{{1.f, 1.f, 1.f}};

    ge::PointsList read_points(const std::string& filepath)
    {
        const std::string COORD{"-?[[:digit:]]{1,2}[[:space:]]-?[[:digit:]]{1,2}"};
        static const std::regex EXPR_FIND{COORD};

        std::ifstream input(filepath);

        std::vector<ge::Point> points;

        std::string log_line;
        while (std::getline(input, log_line))
        {
            const auto startIterator = std::sregex_iterator(log_line.begin(), log_line.end(), EXPR_FIND);
            static const auto endIterator = std::sregex_iterator();

            for (auto iter = startIterator; iter != endIterator; ++iter)
            {
                const std::string coord = iter->str();
                const size_t space = coord.find_first_of(' ');

                assert(space != std::string::npos);
                assert(space != coord.size());

                points.emplace_back
                (
                    std::stod(coord.substr(0, space))
                    , std::stod(coord.substr(space + 1))
                );
            }
        }

        return ge::PointsList{points, WH, BL};
    }
}

int main(int argc, char* argv[])
{
    assert(argc > 1);

    const std::string filepath{argv[1]};

    try
    {
        constexpr uint16_t width = 500;
        constexpr uint16_t height = 500;
        constexpr ge::DynamicSize size
        {
            .default_size = ge::Size{width, height}
            , .min_size = ge::Size{100, 100}
            , .max_size = std::nullopt
        };
        constexpr std::array<uint8_t, 4> background_color{100, 100, 100, 1};

        auto window = ge::Window::create(size, background_color);

        ge::Render render
        (
            ge::SurfaceParams
            {
                .surface_creator = [&window] (const vk::Instance& instance)
                {
                    return window->create_surface(instance);
                }
                , .width = width
                , .height = height
                , .background_color = background_color
            }
        );

        window->start_display();
        render.set_camera_scale(1.f / 27.f);

        ge::RenderLoop::RightButtonClickedCallback right_button_clicked
        {
            [] (ge::Point point)
            {
                std::cout << "x: " << point.x << " y: " << point.y << std::endl;
            }
        };

        render.set_object_to_draw(points::read_points(filepath));
        render.draw_frame();

        ge::RenderLoop render_loop(*window, render);
        while (not render_loop.stopped())
        {
            render_loop.handle_window_events(right_button_clicked);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    catch (const std::exception& e)
    {
        std::cout << "Render failed: " << e.what() << std::endl;
    }

    return 0;
}
