#include "ge/drawer/Drawer.h"

#include <iostream>

int main()
{
    viz::getDrawerSingleton().draw({{10, 10}, {-10, 10}});

    const Point clicked_point = viz::getDrawerSingleton().drawAndWaitClick
    (
        {
            {-3, -3},
            {-3, -2},
            {-3, -1},
            {-3, 0},
            {-3, 1},
            {-3, 2},
            {-3, 3},
            {-2, -3},
            {-2, 0},
            {-2, 3},
            {-1, -3},
            {-1, 0},
            {-1, 3},
            {0, -3},
            {0, -2},
            {0, -1},
            {0, 0},
            {0, 1},
            {0, 2},
            {0, 3},
            {1, -3},
            {1, 0},
            {1, 3},
            {2, -3},
            {2, 0},
            {2, 3},
            {3, -3},
            {3, -2},
            {3, -1},
            {3, 0},
            {3, 1},
            {3, 2},
            {3, 3},
        }
    );

    std::cout << "x: " << clicked_point.first << " y: " << clicked_point.second << std::endl;

//    viz::getDrawerSingleton().clear();

    return 0;
}
