#include "ge/render/render.h"
#include "ge/window/window.h"
#include "ge/render_loop/render_loop.h"

#include <vulkan/vulkan.hpp>

#include <thread>
#include <fstream>
#include <regex>
#include <span>
#include <iostream>

namespace grid
{
    using Cl = ge::Color;
    constexpr Cl BL{{0.f, 0.f, 0.f}};
    constexpr Cl WH{{1.f, 1.f, 1.f}};

    const ge::Grid& grid()
    {
        static const Table table = []
        {
            Table result(7, 7);
            result.data = std::vector<char>
            {
                1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 1, 0, 0, 1,
                1, 0, 0, 1, 0, 0, 1,
                1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 1, 0, 0, 1,
                1, 0, 0, 1, 0, 0, 1,
                1, 1, 1, 1, 1, 1, 1
            };
            return result;
        }();
        static const ge::Grid grid{table, BL, WH};
        return grid;
    }
}

int main(int /*argc*/, char* /*argv*/[])
{
    try
    {
        constexpr uint16_t width = 500;
        constexpr uint16_t height = 500;
        constexpr ge::DynamicSize size
        {
            .default_size = ge::Size{width, height}
            , .min_size = ge::Size{100, 100}
            , .max_size = std::nullopt
        };
        constexpr std::array<uint8_t, 4> background_color{100, 100, 100, 1};

        auto window = ge::Window::create(size, background_color);

        ge::Render render
        (
            ge::SurfaceParams
            {
                .surface_creator = [&window] (const vk::Instance& instance)
                {
                    return window->create_surface(instance);
                }
                , .width = width
                , .height = height
                , .background_color = background_color
            }
        );

        window->start_display();

        render.set_object_to_draw(grid::grid());

        render.set_camera_scale(1.f / 27.f);

        render.draw_frame();

        ge::RenderLoop::RightButtonClickedCallback right_button_clicked
        {
            [] (ge::Point point)
            {
                std::cout << "x: " << point.x << " y: " << point.y << std::endl;
            }
        };

        ge::RenderLoop render_loop(*window, render);
        while (not render_loop.stopped())
        {
            render_loop.handle_window_events(right_button_clicked);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    catch (const std::exception& e)
    {
        std::cout << "Render failed: " << e.what() << std::endl;
    }

    return 0;
}
