#include "utils.h"

void drawGrid(Table& table)
{
    for (size_t i = 1; i < table.columns; ++i)
    {
        table.get(0, i) = 1;
    }

    for (size_t j = 1; j < table.rows; ++j)
    {
        table.get(j, 0) = 1;
    }
}

Table createNumber(size_t gridSize, bool negative)
{
    Table table = (negative)
        ? Table(gridSize + 2, gridSize + 1)
        : Table(gridSize + 1, gridSize + 1);

    drawGrid(table);
    return table;
}

void setNumberBit(Table& table, size_t powerOfTwo, char value)
{
    size_t gridSize = table.columns - 1;
    table.get(1 + powerOfTwo / gridSize, 1 + powerOfTwo % gridSize) = value;
}

Table numberToImage(int value)
{
    bool negative = value < 0;
    value = std::abs(value);

    int maxPowerOfTwo = 1;
    size_t counter = 0;
    while (maxPowerOfTwo < value)
    {
        maxPowerOfTwo *= 2;
        ++counter;
    }

    int gridSize = 1;
    while (gridSize * gridSize <= counter)
    {
        ++gridSize;
    }

    Table table = createNumber(gridSize, negative);
    while (value > 0)
    {
        if (value / maxPowerOfTwo == 1)
        {
            setNumberBit(table, counter, 1);
            value -= maxPowerOfTwo;
        }

        maxPowerOfTwo /= 2;
        --counter;
    }

    return table;
}
