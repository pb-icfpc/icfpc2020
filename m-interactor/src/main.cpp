#include "sig/send.h"
#include "game/craftRequests.h"
#include "game/analyseResponse.h"
#include "game/GlobalGameState.h"

#include "framework/Logger.h"
#include "framework/Assert.h"

#include "httplib.h"

#include <iostream>
#include <regex>
#include <string>

#include <curl/curl.h>

int mainStarterKit(int argc, char* argv[])
{
	const std::string serverUrl(argv[1]);
	const std::string playerKey(argv[2]);

	std::cout << "ServerUrl: " << serverUrl << "; PlayerKey: " << playerKey << std::endl;

	const std::regex urlRegexp("http://(.+):(\\d+)");
	std::smatch urlMatches;
	if (!std::regex_search(serverUrl, urlMatches, urlRegexp) || urlMatches.size() != 3) {
		std::cout << "Unexpected server response:\nBad server URL" << std::endl;
		return 1;
	}
	const std::string serverName = urlMatches[1];
	const int serverPort = std::stoi(urlMatches[2]);
	httplib::Client client(serverName, serverPort);
	const std::shared_ptr<httplib::Response> serverResponse =
		client.Post(serverUrl.c_str(), playerKey.c_str(), "text/plain");

	if (!serverResponse) {
		std::cout << "Unexpected server response:\nNo response from server" << std::endl;
		return 1;
	}

	if (serverResponse->status != 200) {
		std::cout << "Unexpected server response:\nHTTP code: " << serverResponse->status
		          << "\nResponse body: " << serverResponse->body << std::endl;
		return 2;
	}

	std::cout << "Server response: " << serverResponse->body << std::endl;
	return 0;
}

int mainInteractor()
{
    curl_global_init(CURL_GLOBAL_ALL);

    static const std::string REQUEST = "ap ap cons 0 nil";
    std::cout << REQUEST << " -> " << sig::sendrecv(REQUEST) << std::endl;

    curl_global_cleanup();
    return 0;
}

int mainGame(int argc, char* argv[])
{
    constexpr int ARGS_NUMBER_MIN = 3;
    ASSERT(ARGS_NUMBER_MIN <= argc, "Illegal number of arguments.");

    const std::string serverUrl(argv[1]);
    const std::string playerKey(argv[2]);
    LOG(serverUrl);
    LOG(playerKey);

    game::GlobalGameState globalGameState;
    game::GameStage gameStage = game::GameStage::NOT_STARTED;
    std::string gameResponse;

    // JOIN
    const std::string joinRequest = game::craftJoinRequest(playerKey);
    LOG(joinRequest);
    gameResponse = sig::sendrecv(serverUrl, joinRequest);
    LOG(gameResponse);

    ASSERT(game::isSuccess(gameResponse), "Unsuccessful response.");
    gameStage = game::getGameStage(gameResponse);
    if (game::GameStage::FINISHED == gameStage)
    {
        std::cout << "Game has finished." << std::endl;
        return 0;
    }
    const std::string role = game::toString(game::getPlayerRole(gameResponse));
    LOG(role);

    // START
    const std::string startRequest = game::craftStartRequest(playerKey, gameResponse);
    LOG(startRequest);
    gameResponse = sig::sendrecv(serverUrl, startRequest);
    LOG(gameResponse);

    ASSERT(game::isSuccess(gameResponse), "Unsuccessful response.");
    gameStage = game::getGameStage(gameResponse);
    if (game::GameStage::FINISHED == gameStage)
    {
        std::cout << "Game has finished." << std::endl;
        return 0;
    }

    // BATTLE
    size_t tick = 0;
    while (game::GameStage::FINISHED != gameStage)
    {
        const std::string commandsRequest = game::craftCommandsRequest(playerKey, gameResponse, tick, globalGameState);
        LOG(commandsRequest);
        gameResponse = sig::sendrecv(serverUrl, commandsRequest);
        LOG(gameResponse);

        ASSERT(game::isSuccess(gameResponse), "Unsuccessful response.");
        gameStage = game::getGameStage(gameResponse);

        ++tick;
    }

    std::cout << "Game has finished." << std::endl;

    return 0;
}

int main(int argc, char* argv[])
{
//    return mainStarterKit(argc, argv);
//    return mainInteractor();
    return mainGame(argc, argv);
}
