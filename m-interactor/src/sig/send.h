#pragma once

#include <string>

namespace sig
{
    std::string sendrecv(const std::string& message);
    std::string sendrecv(const std::string& url, const std::string& message);
}
