#pragma once

#include <string>

namespace sig
{
    std::string mod(const std::string& inp);
}
