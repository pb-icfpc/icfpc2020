#include "mod.h"

#include "framework/BigInt.h"

#include <utility>
#include <sstream>
#include <cassert>
#include <algorithm>

namespace sig
{
    namespace
    {
        bool isEnd(const std::string& inp, const size_t startIndex)
        {
            return inp.size() == startIndex;
        }

        bool isWhitespace(const std::string& inp, const size_t startIndex)
        {
            assert(startIndex < inp.size());
            return ' ' == inp.at(startIndex);
        }

        bool isNil(const std::string& inp, const size_t startIndex)
        {
            assert(startIndex < inp.size());
            return 'n' == inp.at(startIndex);
        }

        bool isApApCons(const std::string& inp, const size_t startIndex)
        {
            assert(startIndex < inp.size());
            return 'a' == inp.at(startIndex);
        }

        bool isNumber(const std::string& inp, const size_t startIndex)
        {
            assert(startIndex < inp.size());

            const char ch = inp.at(startIndex);
            return ('-' == ch)
                || ((ch >= '0') && (ch <= '9'));
        }

        // TODO Performance could be improved significantly.
        size_t modNumber(const std::string& inp, size_t startIndex, std::stringstream& out)
        {
            assert(isNumber(inp, startIndex));

            size_t lengthDec = 0;
            while ((startIndex + lengthDec) < inp.size() && isNumber(inp, startIndex + lengthDec))
            {
                ++lengthDec;
            }

            BigInt result{inp.substr(startIndex, lengthDec)};
            startIndex += lengthDec;

            if (result < 0)
            {
                out << "10";
                result = -result;
            }
            else
            {
                out << "01";
            }

            const std::string numberBin = (0 == result)
                ? ""
                : result.get_str(2);

            size_t extraZeroesNumber = 0;
            while (0 != ((numberBin.size() + extraZeroesNumber) % 4))
            {
                ++extraZeroesNumber;
            }
            const size_t bits = (numberBin.size() + extraZeroesNumber) / 4;
            for (size_t i = 0; i < bits; ++i)
            {
                out << '1';
            }
            out << '0';

            for (size_t i = 0; i < extraZeroesNumber; ++i)
            {
                out << '0';
            }
            out << numberBin;

            return startIndex;
        }
    }

    std::string mod(const std::string& inp)
    {
        size_t startIndex = 0;
        std::stringstream out;

        while (not isEnd(inp, startIndex))
        {
            if (isWhitespace(inp, startIndex))
            {
                ++startIndex;
            }
            else if (isNil(inp, startIndex))
            {
                out << "00";
                startIndex += 3;
            }
            else if (isApApCons(inp, startIndex))
            {
                out << "11";
                startIndex += 10;
            }
            else
            {
                assert(isNumber(inp, startIndex));
                startIndex = modNumber(inp, startIndex, out);
            }
        }

        return out.str();
    }
}
