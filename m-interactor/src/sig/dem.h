#pragma once

#include <string>

namespace sig
{
    std::string dem(const std::string& inp);
}
