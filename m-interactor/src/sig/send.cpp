#include "send.h"

#include "mod.h"
#include "dem.h"

#include "framework/Assert.h"

#include <curl/curl.h>

namespace sig
{
    namespace
    {
        // Do not share, contains secret key.
        const char * const ALIENS_POST_BASE_URL = "https://icfpc2020-api.testkontur.ru";
        const char * const ALIENS_POST_APPEND_URL = "/aliens/send?apiKey=afa320d4fc534d97adfc32c2734ecd3c";
        const char * const ALIENS_GET_URL_TEMPLATE = "https://icfpc2020-api.testkontur.ru/aliens/%s?apiKey=afa320d4fc534d97adfc32c2734ecd3c";

        size_t writeCallback(const char * const contents, const size_t size, const size_t nmemb, void* userp)
        {
            ((std::string*)userp)->append(contents, size * nmemb);
            return size * nmemb;
        }

        std::string sendrecvmodulated(const std::string& url, const std::string& message)
        {
            CURL* curl = curl_easy_init();

            std::string buffer;

            curl_easy_setopt(curl, CURLOPT_URL, (url + ALIENS_POST_APPEND_URL).c_str());
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, message.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);

            const CURLcode res = curl_easy_perform(curl);
            ASSERT(CURLE_OK == res, curl_easy_strerror(res));

            curl_easy_cleanup(curl);

            return buffer;
        }
    }

    std::string sendrecv(const std::string& url, const std::string& message)
    {
        return dem(sendrecvmodulated(url, mod(message)));
    }

    std::string sendrecv(const std::string& message)
    {
        return sendrecv(ALIENS_POST_BASE_URL, message);
    }
}
