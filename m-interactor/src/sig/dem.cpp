#include "dem.h"

#include "framework/BigInt.h"

#include <sstream>
#include <utility>
#include <cassert>

namespace sig
{
    namespace
    {
        constexpr size_t HEADER_SIZE = 2;
        constexpr size_t NUMBER_WIDTH_MULTIPLIER = 4;

        bool isEnd(const std::string& inp, const size_t startIndex)
        {
            return inp.size() == startIndex;
        }

        bool isNumber(const std::string& inp, const size_t startIndex)
        {
            assert(not isEnd(inp, startIndex));
            assert(startIndex <= inp.size());

            const size_t size = inp.size() - startIndex;
            assert(size >= HEADER_SIZE);

            return ('0' == inp[startIndex] && '1' == inp[1 + startIndex])
                or ('1' == inp[startIndex] && '0' == inp[1 + startIndex]);
        }

        bool isPositiveNumber(const std::string& inp, const size_t startIndex)
        {
            assert(isNumber(inp, startIndex));
            return '0' == inp[startIndex] && '1' == inp[1 + startIndex];
        }

        bool isNegativeNumber(const std::string& inp, const size_t startIndex)
        {
            assert(isNumber(inp, startIndex));
            return '1' == inp[startIndex] && '0' == inp[1 + startIndex];
        }

        bool isList(const std::string& inp, const size_t startIndex)
        {
            assert(not isEnd(inp, startIndex));
            assert(startIndex <= inp.size());

            const size_t size = inp.size() - startIndex;
            assert(size >= HEADER_SIZE);

            return ('1' == inp[startIndex] && '1' == inp[1 + startIndex]);
        }

        bool isNil(const std::string& inp, const size_t startIndex)
        {
            assert(not isEnd(inp, startIndex));
            assert(startIndex <= inp.size());

            const size_t size = inp.size() - startIndex;
            assert(size >= HEADER_SIZE);

            return ('0' == inp[startIndex] && '0' == inp[1 + startIndex]);
        }

        std::pair<BigInt, size_t> demNumber(const std::string& inp, size_t startIndex)
        {
            assert(isNumber(inp, startIndex));

            assert(startIndex <= inp.size());
            const size_t size = inp.size() - startIndex;
            assert(size >= HEADER_SIZE);

            const bool negative = isNegativeNumber(inp, startIndex);

            startIndex += HEADER_SIZE;
            BigInt result = 0;
            size_t bitsNumber = 0;
            while ('1' == inp.at(startIndex + bitsNumber))
            {
                ++bitsNumber;
            }

            startIndex += bitsNumber + 1;
            bitsNumber *= NUMBER_WIDTH_MULTIPLIER;
            for (size_t i = 0; i < bitsNumber; ++i)
            {
                result *= 2;
                result += inp.at(startIndex + i) - '0';
            }
            startIndex += bitsNumber;

            if (negative)
            {
                result = -result;
            }

            return std::pair{result, startIndex};
        }

        size_t demNumber(const std::string& inp, const size_t startIndex, std::stringstream& out)
        {
            const auto& res = demNumber(inp, startIndex);
            out << res.first;
            return res.second;
        }
    }

    std::string dem(const std::string& inp)
    {
        size_t startIndex = 0;
        std::stringstream out;

        while (not isEnd(inp, startIndex))
        {
            if (0 != startIndex)
            {
                out << ' ';
            }

            if (isNil(inp, startIndex))
            {
                out << "nil";
                startIndex += HEADER_SIZE;
            }
            else if (isList(inp, startIndex))
            {
                out << "ap ap cons";
                startIndex += HEADER_SIZE;
            }
            else
            {
                assert(isNumber(inp, startIndex));
                startIndex = demNumber(inp, startIndex, out);
            }
        }

        return out.str();
    }
}
