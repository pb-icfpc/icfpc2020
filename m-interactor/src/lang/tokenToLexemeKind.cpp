#include "tokenToLexemeKind.h"

#include <unordered_map>
#include <stdexcept>


namespace lang
{
    namespace
    {
        using TokenToLexemeKind = std::unordered_map<std::string, LexemeKind>;

        const TokenToLexemeKind& TOKEN_TO_LEXEME_KIND()
        {
            static const TokenToLexemeKind& TOKEN_TO_LEXEME_KIND
            {
                {"=", LexemeKind::EQ}
                , {"ap", LexemeKind::APPLICATION}
            };

            return TOKEN_TO_LEXEME_KIND;
        }

        bool isNumber(const std::string& token)
        {
            auto begin = token.begin();
            if (token[0] == '-')
            {
                std::advance(begin, 1);
            }

            return std::all_of
            (
                begin
                , token.end()
                , [](char ch)
                {
                    return std::isdigit(ch) != 0;
                }
            );
        }
    }

    LexemeKind tokenToLexemeKind(const std::string& token)
    {
        if (token[0] == ':')
            return LexemeKind::VAR;

        const auto& iter = TOKEN_TO_LEXEME_KIND().find(token);
        if (end(TOKEN_TO_LEXEME_KIND()) != iter)
        {
            return iter->second;
        }

        if (isNumber(token))
            return LexemeKind::NUM;

        return LexemeKind::FUNCTION;
    }

    std::ostream& operator<<(std::ostream& stream, LexemeKind targetKind)
    {
        switch (targetKind) {
            case LexemeKind::UNKNOWN:
                stream << "UNKNOWN";
                return stream;
            case LexemeKind::END:
                stream << "END";
                return stream;
            case LexemeKind::NUM:
                stream << "NUM";
                return stream;
            case LexemeKind::EQ:
                stream << "EQ";
                return stream;
            case LexemeKind::VAR:
                stream << "VAR";
                return stream;
            case LexemeKind::FUNCTION:
                stream << "FUNCTION";
                return stream;
            case LexemeKind::APPLICATION:
                stream << "APPLICATION";
                return stream;
        }

        throw std::logic_error(std::string("Unknown LexemeKind=") + std::to_string(static_cast<int>(targetKind)));
    }
}
