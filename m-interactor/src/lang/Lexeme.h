#pragma once

#include "LexemeKind.h"

#include <string>

namespace lang
{
    struct Lexeme final
    {
        LexemeKind kind;
        std::string token;
    };

    std::string to_string(const Lexeme& lexeme);
}
