#pragma once

#include "UntypedLambda.h"

namespace lang::list
{
    bool isNil(SPtr<Term> list);
    bool isPairNumbers(SPtr<Term> list);

    SPtr<Term> head(SPtr<Term> list);
    SPtr<Term> tail(SPtr<Term> list);

    std::optional<std::string> tryGetNumber(SPtr<Term> listOrNumber);

    std::vector<SPtr<Term>> toVector(SPtr<Term> listOrPairNumbers);

    std::string prettyPrint(SPtr<Term> listOrPairNumbers);

    std::vector<std::string> plainToString(SPtr<Term> listOrPairNumbers);
}
