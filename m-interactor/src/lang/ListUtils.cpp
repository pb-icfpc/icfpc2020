#include "ListUtils.h"

#include "constants/Constants.h"

namespace lang::list
{
    bool isNil(SPtr<Term> list)
    {
        using namespace constants;
        return isTrue(app(ISNIL(), list));
    }

    bool isPairNumbers(SPtr<Term> list)
    {
        return lang::tryGetNumber(head(list)) && lang::tryGetNumber(tail(list));
    }

    SPtr<Term> head(SPtr<Term> list)
    {
        using namespace constants;
        return headEval(app(HEAD(), list));
    }

    SPtr<Term> tail(SPtr<Term> list)
    {
        using namespace constants;
        return headEval(app(TAIL(), list));
    }

    Opt<std::string> tryGetNumber(SPtr<Term> listOrNumber)
    {
        using namespace constants;
        Opt<SPtr<Number>> number = ::lang::tryGetNumber(listOrNumber);

        if (not number)
        {
            return std::nullopt;
        }

        return number.value()->toString();
    }

    std::vector<SPtr<Term>> toVector(SPtr<Term> listOrPairOfNumbers)
    {
        using namespace constants;

        std::vector<SPtr<Term>> result;

        if (isPairNumbers(listOrPairOfNumbers))
        {
            result.push_back(head(listOrPairOfNumbers));
            result.push_back(tail(listOrPairOfNumbers));
            return result;
        }

        auto iterate = [](SPtr<Term> listOrPairOfNumbers, auto onHead)
        {
            while (true)
            {
                if (list::isNil(listOrPairOfNumbers))
                {
                    break;
                }

                onHead(head(listOrPairOfNumbers));
                listOrPairOfNumbers = tail(listOrPairOfNumbers);
            }
        };

        auto addTerm = [&result, &iterate](SPtr<Term> listOrPairOfNumbers)
        {
            result.push_back(listOrPairOfNumbers);
        };

        iterate(listOrPairOfNumbers, addTerm);

        return result;
    }

    std::string prettyPrint(SPtr<Term> term)
    {
        std::string result;

        std::function<void(SPtr<Term>)> printList;
        printList = [&](SPtr<Term> term) -> void
        {
            bool first = true;
            result += "(";
            while (not list::isNil(term))
            {
                if (not first)
                {
                    result += ", ";
                }

                SPtr<Term> head = list::head(term);

                if (Opt<std::string> number = list::tryGetNumber(head))
                {
                    result += *number;
                }
                else
                {
                    // Must be nested list or pair
                    printList(head);
                }

                SPtr<Term> tail = list::tail(term);
                if (Opt<std::string> number = list::tryGetNumber(tail))
                {
                    result += ", " + *number;
                    break;
                }
                else
                {
                    term = tail;
                }

                first = false;
            }

            result += ")";
        };

        printList(term);
        return result;
    }

    std::vector<std::string> plainToString(SPtr<Term> listOrPairNumbers)
    {
        auto terms = toVector(listOrPairNumbers);

        std::vector<std::string> result;
        for (const auto& term : terms)
        {
            result.push_back(toString(*term));
        }

        return result;
    }
}
