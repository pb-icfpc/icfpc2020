#include "toStringGalaxy.h"
#include "ListUtils.h"

#include "framework/Assert.h"

namespace lang
{
    std::string toStringGalaxy(SPtr<Term> term)
    {
        std::string result;

        std::function<void(SPtr<Term>)> printList;
        printList = [&](SPtr<Term> term) -> void
        {
            bool first = true;
            bool pair = false;
            while (not list::isNil(term))
            {
                result += "ap ap cons ";
                SPtr<Term> head = list::head(term);

                if (Opt<std::string> number = list::tryGetNumber(head))
                {
                    result += *number + " ";
                }
                else
                {
                    // Must be nested list or pair
                    printList(head);
                }

                SPtr<Term> tail = list::tail(term);
                if (Opt<std::string> number = list::tryGetNumber(tail))
                {
                    pair = true;
                    result += *number + " ";
                    break;
                }
                else
                {
                    term = tail;
                }

                first = false;
            }

            if (not pair) result += "nil ";
        };

        printList(term);

        if (not result.empty()) result.pop_back();

        return result;
    }
}
