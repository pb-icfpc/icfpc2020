#include "framework/Assert.h"
#include "framework/Aliases.h"
#include "Lexeme.h"
#include "Lexer.h"
#include "Parser.h"
#include "UntypedLambda.h"

#include <sstream>
#include <fstream>


namespace lang::file_evaluator
{
    SPtr<Term> evaluateStream(std::istream& stream)
    {
        Lexer lexer(stream);
        Parser parser;
        parser.parse(lexer);
        auto term = assign(var("galaxy"), parser.definedVariables);
        return headEval(term);
    }

    SPtr<Term> evaluateFile(const std::string& path)
    {
        std::ifstream stream(path);
        ASSERT(stream.is_open(), "Failed to open file: " + path);
        return evaluateStream(stream);
    }

    SPtr<Term> evaluateBlock(const std::string& rawCode)
    {
        std::stringstream stream(rawCode);
        return evaluateStream(stream);
    }
}
