#pragma once

#include "framework/BigInt.h"
#include "ListUtils.h"

namespace lang
{
    int64_t getIntChecked(SPtr<Term> number);
    BigInt getBigIntChecked(SPtr<Term> number);
}
