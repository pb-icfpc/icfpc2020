#pragma once

#include "framework/Assert.h"
#include "framework/Aliases.h"
#include "Lexeme.h"
#include "Lexer.h"
#include "UntypedLambda.h"
#include "lang/constants/Number.h"
#include "lang/constants/Constants.h"
#include "lang/constants/Interact.h"

#include <vector>
#include <iostream>
#include <unordered_map>
#include <lang/constants/Numeric.h>


namespace lang
{
    struct Parser
    {
        NameToVariable definedVariables;
        NameToVariable undefinedVariables;
        std::vector<std::string> varsWhitelist;
        bool removeUnusedVars;

        Parser(bool removeUnusedVars = false, const std::vector<std::string>& varsWhitelist = {"galaxy"});

        void parse(lang::Lexer& lexer);
        SPtr<Term> parseTerm(lang::Lexer& lexer);

    private:
        void removeUnusedVariables();

        void parseNextExpression(lang::Lexer& lexer);

    };

}
