#pragma once

#include "LexemeKind.h"

#include <string>
#include <ostream>

namespace lang
{
    LexemeKind tokenToLexemeKind(const std::string& token);

    std::ostream& operator<<(std::ostream& stream, LexemeKind kind);
}
