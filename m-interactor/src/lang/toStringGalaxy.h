#pragma once

#include "UntypedLambda.h"

namespace lang
{
    std::string toStringGalaxy(SPtr<Term> term);
}
