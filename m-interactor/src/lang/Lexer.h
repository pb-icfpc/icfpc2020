#pragma once

#include "framework/Aliases.h"
#include "Lexeme.h"

#include <istream>

namespace lang
{
    class Lexer final
    {
    public:
        using Stream = std::istream;

        explicit Lexer(Stream& in);

        bool hasNext();
        Lexeme nextLexeme();

    private:
        Ref<Stream> mIn;
    };
}