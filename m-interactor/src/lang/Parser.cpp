#include "framework/Assert.h"
#include "framework/Aliases.h"
#include "Lexeme.h"
#include "Lexer.h"
#include "UntypedLambda.h"
#include "lang/constants/Number.h"
#include "lang/constants/Constants.h"
#include "lang/constants/Interact.h"
#include "Parser.h"

#include <vector>
#include <iostream>
#include <unordered_map>
#include <lang/constants/Numeric.h>


namespace lang
{
    Parser::Parser(bool removeUnusedVars, const std::vector<std::string>& varsWhitelist)
        : removeUnusedVars(removeUnusedVars)
        , varsWhitelist(varsWhitelist)
    {
    }

    void Parser::parse(lang::Lexer& lexer)
    {
        while (lexer.hasNext())
        {
            parseNextExpression(lexer);
        }

        removeUnusedVariables();
    }

    void Parser::removeUnusedVariables()
    {
        if (not removeUnusedVars)
            return;

        auto iter = definedVariables.begin();
        while (iter != definedVariables.end())
        {
            auto undefVarIter = undefinedVariables.find(iter->first);
            if
                (
                undefVarIter == undefinedVariables.end() &&
                (std::find(varsWhitelist.begin(), varsWhitelist.end(), iter->first) == varsWhitelist.end())
                )
            {
                std::cout << "Removed variable: " << iter->first << '\n';
                iter = definedVariables.erase(iter);
            } else
            {
                ++iter;
            }
        }
    }
        
    void Parser::parseNextExpression(lang::Lexer& lexer)
    {
        Lexeme lexemeVariable = lexer.nextLexeme();
        Lexeme lexemeEqual = lexer.nextLexeme();
        ASSERT(lexemeVariable.kind == LexemeKind::VAR || lexemeVariable.kind == LexemeKind::FUNCTION, "First lexeme of the expression must be VARIABLE declaration.");
        ASSERT(lexemeEqual.kind == LexemeKind::EQ, "VARIABLE declaration must be followed by EQ.");
        ASSERT(definedVariables.count(lexemeVariable.token) == 0, "VARIABLES redifinition is not allowed.");

        definedVariables.insert(std::make_pair(lexemeVariable.token, parseTerm(lexer)));
    }

    SPtr<Term> Parser::parseTerm(lang::Lexer& lexer)
    {
        static const std::unordered_map<std::string, SPtr<Term>> termsFactory
        {
            {"draw", lang::constants::DRAW() },
            // {"checkerboard", lang::constants::DRAW() },
            // {"multipledraw", lang::constants::DRAW() },

            {"eq", lang::constants::EQ() },
            {"lt", lang::constants::LT() },
            {"if0", lang::constants::IF0() },

            // {"mod", lang::constants::MOD() },
            // {"dem", lang::constants::DEM() },
            {"send", lang::constants::SEND() },
            // {"humans", lang::constants::HUMANS() },
            // {"aliens", lang::constants::ALIENS() },

            // {"modem", lang::constants::MODEM() },
            {"interact", lang::constants::INTERACT() },
            // {"statelessdraw", lang::constants::STATELESSDRAW() },

            {"cons", lang::constants::CONS() },
            {"vec", lang::constants::VEC() },
            {"car", lang::constants::CAR() },
            {"cdr", lang::constants::CDR() },

            {"nil",  lang::constants::NIL() },
            {"isnil",  lang::constants::ISNIL() },
            {"i",  lang::constants::I() },
            {"s", lang::constants::S() },
            {"c", lang::constants::C() },
            {"b", lang::constants::B() },
            {"t", lang::constants::K() },
            {"f", lang::constants::FALSE() },

            {"inc", lang::constants::INC() },
            {"dec", lang::constants::DEC() },
            {"neg", lang::constants::NEGATE() },
            {"add", lang::constants::ADD() },
            {"mul", lang::constants::MUL() },
            {"div", lang::constants::DIV() },
            {"pwr2", lang::constants::PWR2() }
        };

        Lexeme lexeme = lexer.nextLexeme();
        switch (lexeme.kind)
        {
            case LexemeKind::APPLICATION:
            {
                auto function = parseTerm(lexer);
                auto argument = parseTerm(lexer);
                return app(function, argument);
            }
            case LexemeKind::FUNCTION:
            {
                auto factoryIter = termsFactory.find(lexeme.token);
                if (factoryIter != termsFactory.end())
                {
                    return factoryIter->second;
                }

                ASSERT(false, std::string("Unknown Function lexeme, token=") + lexeme.token);
            }
            case LexemeKind::UNKNOWN:
                ASSERT(false, "Unknown LexemeKind expression.");
            case LexemeKind::EQ:
                ASSERT(false, "EQ can be only at the start of an expression.");
            case LexemeKind::END:
                ASSERT(false, "Stream has ended too early.");
            case LexemeKind::VAR:
            {
                auto definedVarIter = definedVariables.find(lexeme.token);
                if (definedVarIter != definedVariables.end())
                {
                    return definedVarIter->second;
                }

                auto undefinedVarIter = undefinedVariables.find(lexeme.token);
                if (undefinedVarIter == undefinedVariables.end())
                {
                    auto res = undefinedVariables.insert(std::make_pair(lexeme.token, var(lexeme.token)));
                    return res.first->second;
                }

                return undefinedVarIter->second;
            }
            case LexemeKind::NUM:
            {
                auto value = BigInt(lexeme.token);
                return lang::makeNumber(value);
            }
        }

        ASSERT(false, "Unsupported lexeme: " + to_string(lexeme));
    }
}
