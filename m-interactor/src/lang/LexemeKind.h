#pragma once

namespace lang
{
    enum class LexemeKind
    {
          UNKNOWN = 0
        , END
        , NUM
        , EQ
        , VAR
        , FUNCTION
        , APPLICATION
    };
}