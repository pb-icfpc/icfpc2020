#include "Lexeme.h"

#include <string>

namespace lang
{
    std::string to_string(const Lexeme& lexeme)
    {
        return std::string("kind=") + std::to_string(static_cast<int>(lexeme.kind)) + ", token=" + lexeme.token;
    }
}
