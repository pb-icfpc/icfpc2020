#include "points.h"

#include "ListUtils.h"

#include "framework/Assert.h"

namespace lang
{
    Points points(SPtr<Term> term)
    {
        Points result;

        auto x = list::toVector(term);

        for (const auto& pair : x)
        {
            auto p = list::toVector(pair);

            result.push_back
            (
                std::pair<int64_t, int64_t>
                {
                    std::atoll(list::tryGetNumber(p[0]).value().c_str()),
                    std::atoll(list::tryGetNumber(p[1]).value().c_str())
                }
            );
        }

        return result;
    }
}
