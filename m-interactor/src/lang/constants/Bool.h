#pragma once

#include "lang/UntypedLambda.h"

namespace lang::constants
{
    SPtr<Term> TRUE();
    SPtr<Term> FALSE();
    bool isTrue(SPtr<Term> term);

    SPtr<Term> EQ();
    SPtr<Term> IF0();
    SPtr<Term> LT();
}
