#include "Combinators.h"
#include "Number.h"

#include "framework/Assert.h"

namespace lang
{
    NamedCombinator::NamedCombinator
    (
          std::string name
        , SPtr<Term> term
    )
        : mName{std::move(name)}
        , mTerm{std::move(term)}
    {
        ASSERT(mTerm, "Should not be empty.");

        // Ask Dima why so.
        // ASSERT(std::holds_alternative<Abstraction>(*mTerm), "Should hold Abstraction.");
    }

    SPtr<Term> NamedCombinator::apply(SPtr<Term> arg) const
    {
        return app(mTerm, arg);
    }

    bool NamedCombinator::isAbstraction() const
    {
        return true;
    }

    std::string NamedCombinator::toString() const
    {
        return mName;
    }

    SPtr<Term> namedCombinator(std::string name, SPtr<Term> term)
    {
        return std::make_shared<Term>(std::make_shared<NamedCombinator>(std::move(name), std::move(term)));
    }

    UnaryOperator::UnaryOperator(std::string name, std::function<SPtr<Term>(SPtr<Term>)> op)
        : mName(std::move(name))
        , mOperator(std::move(op))
    {}

    SPtr<Term> UnaryOperator::apply(SPtr<Term> arg) const
    {
        return mOperator(std::move(arg));
    }

    bool UnaryOperator::isAbstraction() const
    {
        return true;
    }
    std::string UnaryOperator::toString() const
    {
        return mName;
    }

    SPtr<Term> unaryOp(std::string name, std::function<SPtr<Term>(SPtr<Term>)> op)
    {
        return std::make_shared<Term>(std::make_shared<UnaryOperator>(std::move(name), std::move(op)));
    }

    SPtr<Term> binaryOp(std::string name, std::function<SPtr<Term>(SPtr<Term>, SPtr<Term>)> op2)
    {
        return unaryOp
        (
            name,
            [name, op2=std::move(op2)](SPtr<Term> a) -> SPtr<Term>
            {
                return unaryOp
                (
                    name + "'", [a=std::move(a), op2=std::move(op2)](SPtr<Term> b) -> SPtr<Term>
                    {
                        return op2(std::move(a), std::move(b));
                    }
                );
            }
        );
    }

    SPtr<Term> ternaryOp(std::string name, std::function<SPtr<Term>(SPtr<Term>, SPtr<Term>, SPtr<Term>)> op3)
    {
        return binaryOp
        (
            name,
            [name, op3=std::move(op3)](SPtr<Term> a, SPtr<Term> b) -> SPtr<Term>
            {
                return unaryOp
                (
                    name + "''", [a=std::move(a), b=std::move(b), op3=std::move(op3)](SPtr<Term> c) -> SPtr<Term>
                    {
                        return op3(std::move(a), std::move(b), std::move(c));
                    }
                );
            }
        );
    }
}

namespace lang::constants
{
    SPtr<Term> S()
    {
        static SPtr<Term> S = ternaryOp
        (
            "s",
            [](SPtr<Term> x, SPtr<Term> y, SPtr<Term> z)
            {
                return app(app(x, z), app(y, z));
            }
        );

        return S;
    }

    SPtr<Term> B()
    {
        static SPtr<Term> B = ternaryOp
        (
            "b",
            [](SPtr<Term> x, SPtr<Term> y, SPtr<Term> z)
            {
                return app(x, app(y, z));
            }
        );

        return B;
    }

    SPtr<Term> C()
    {
        static SPtr<Term> C = ternaryOp
        (
            "c",
            [](SPtr<Term> x, SPtr<Term> y, SPtr<Term> z)
            {
                return  app(app(x, z), y);
            }
        );

        return C;
    }

    SPtr<Term> K()
    {
        static SPtr<Term> K = binaryOp
        (
            "t",
            [](SPtr<Term> x, SPtr<Term> y)
            {
                return x;
            }
        );

        return K;
    }

    SPtr<Term> W()
    {
        static SPtr<Term> W = binaryOp
        (
            "w",
            [](SPtr<Term> x, SPtr<Term> y)
            {
                return app(app(x, y), y);
            }
        );

        return W;
    }

    SPtr<Term> I()
    {
        static SPtr<Term> I = namedCombinator("i", LAMBDA(x, x));
        return I;
    }

    SPtr<Term> FIRST()
    {
        return K();
    }

    SPtr<Term> SECOND()
    {
        static SPtr<Term> SECOND = binaryOp
        (
            "second",
            [](SPtr<Term> x, SPtr<Term> y)
            {
                return y;
            }
        );

        return SECOND;
    }
}
