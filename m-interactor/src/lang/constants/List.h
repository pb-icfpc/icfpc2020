#pragma once

#include "lang/UntypedLambda.h"

namespace lang::constants
{
    SPtr<Term> CONS();
    SPtr<Term> VEC();

    SPtr<Term> CAR();
    SPtr<Term> CDR();

    SPtr<Term> HEAD();
    SPtr<Term> TAIL();

    SPtr<Term> NIL();
    SPtr<Term> ISNIL();

    SPtr<Term> MAP();
}
