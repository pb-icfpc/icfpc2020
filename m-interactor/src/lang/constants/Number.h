#pragma once

#include "framework/BigInt.h"
#include "lang/UntypedLambda.h"

#include <string>

namespace lang
{
    // introduce long arithmetics
    using NumberValue = BigInt;

    class Number final : public ConstI
    {
    public:
        explicit Number(const std::string& value);
        explicit Number(NumberValue value);

        SPtr<Term> apply(SPtr<Term>) const override;
        bool isAbstraction() const override;
        std::string toString() const override;

        NumberValue get() const;
        NumberValue extract() &&;

    private:
        NumberValue mValue;
    };

    SPtr<Term> makeNumber(NumberValue);
    SPtr<Number> getNumber(SPtr<Term> term);
    std::optional<SPtr<Number>> tryGetNumber(SPtr<Term> term);
    bool isNumber(SPtr<Term> term);
}
