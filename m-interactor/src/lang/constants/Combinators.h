#pragma once

#include "lang/UntypedLambda.h"

#include <functional>

namespace lang
{
    class NamedCombinator final : public ConstI
    {
    public:
        explicit NamedCombinator(std::string name, SPtr<Term>);

        SPtr<Term> apply(SPtr<Term> arg) const override;
        bool isAbstraction() const override;
        std::string toString() const override;

    private:
        std::string mName;
        SPtr<Term> mTerm;
    };

    SPtr<Term> namedCombinator(std::string name, SPtr<Term>);

    class UnaryOperator final : public ConstI
    {
    public:
        explicit UnaryOperator(std::string name, std::function<SPtr<Term>(SPtr<Term>)> op);

        SPtr<Term> apply(SPtr<Term> arg) const override;
        bool isAbstraction() const override;
        std::string toString() const override;

    private:
        std::string mName;
        std::function<SPtr<Term>(SPtr<Term>)> mOperator;
    };

    SPtr<Term> unaryOp(std::string name, std::function<SPtr<Term>(SPtr<Term>)> op1);
    SPtr<Term> binaryOp(std::string name, std::function<SPtr<Term>(SPtr<Term>, SPtr<Term>)> op2);
    SPtr<Term> ternaryOp(std::string name, std::function<SPtr<Term>(SPtr<Term>, SPtr<Term>, SPtr<Term>)> op3);
}

namespace lang::constants
{
    // S = λxyz.xz(yz)
    SPtr<Term> S();

    // https://en.wikipedia.org/wiki/B,_C,_K,_W_system
    SPtr<Term> B();
    SPtr<Term> C();
    SPtr<Term> K();
    SPtr<Term> W();

    SPtr<Term> I();

    SPtr<Term> FIRST();
    SPtr<Term> SECOND();
}
