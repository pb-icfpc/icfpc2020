#include "Interact.h"
#include "Combinators.h"
#include "Constants.h"
#include "List.h"

#include "framework/Assert.h"

#include "sig/send.h"

#include "viz/ge/drawer/Drawer.h"

#include "lang/ListUtils.h"
#include "lang/Evaluator.h"
#include "lang/points.h"
#include "lang/toStringGalaxy.h"

namespace lang::constants
{
    SPtr<Term> SEND()
    {
        static SPtr<Term> SEND = unaryOp
        (
            "send",
            [](SPtr<Term> term) -> SPtr<Term>
            {
                std::string termString = toStringGalaxy(term);
                std::string received = sig::sendrecv(termString);
                return file_evaluator::evaluateBlock("galaxy = " + received);
            }
        );

        return SEND;
    }

    SPtr<Term> MOD()
    {
        ASSERT(false, "Not implemented.");
    }

    SPtr<Term> DEMOD()
    {
        ASSERT(false, "Not implemented.");
    }

    SPtr<Term> DRAW()
    {
        static SPtr<Term> DRAW = unaryOp
        (
            "draw",
            [](SPtr<Term> term) -> SPtr<Term>
            {
                #if WITH_VIZ
                    viz::getDrawerSingleton().draw(points(term));
                #endif
                return NIL();
            }
        );

        return DRAW;
    }

    SPtr<Term> MULTIPLEDRAW()
    {
        static SPtr<Term> MULTIPLEDRAW = app(MAP(), DRAW());
        return MULTIPLEDRAW;
    }

    SPtr<Term> INTERACT()
    {
        /*
            https://message-from-space.readthedocs.io/en/latest/message38.html

            interact protocol state vector = f38 protocol (protocol state vector)
        */
        static SPtr<Term> INTERACT = ternaryOp
        (
            "interact",
            [](SPtr<Term> protocol, SPtr<Term> state, SPtr<Term> vec) -> SPtr<Term>
            {
                return app(app(F38(), protocol),  app(app(protocol, state), vec));
            }
        );

        return INTERACT;
    }

    SPtr<Term> STATELESSDRAW()
    {
        /*
            https://message-from-space.readthedocs.io/en/latest/message40.html

            statelessdraw = ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil
        */

        static SPtr<Term> STATELESSDRAW = namedCombinator(
                "statelessdraw",
                app( app( C(), app( app( B(), B()), app( app( B(), app( B(), app( CONS(), makeNumber(0)))), app( app( C(), app( app( B(), B()), CONS())), app( app( C(), CONS()), NIL()))))), app( app( C(), app( app( B(), CONS()), app( app( C(), CONS()), NIL()))), NIL()))
            );

        return STATELESSDRAW;
    }

    SPtr<Term> STATEFULDRAW()
    {
        /*
            https://message-from-space.readthedocs.io/en/latest/message41.html

            statefuldraw = ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c cons nil ap c cons
        */

        static SPtr<Term> STATELESSDRAW = namedCombinator(
            "statefuldraw",
            app( app( B(), app( B(), app( app( S(), app( app( B(), app( B(), app( CONS(), makeNumber(0)))), app( app( C(), app( app( B(), B()), CONS())), app( app( C(), CONS()), NIL())))), app( app( C(), CONS()), NIL())))), app( C(), CONS()))
        );

        return STATELESSDRAW;
    }


    SPtr<Term> F38()
    {
        /*
            https://message-from-space.readthedocs.io/en/latest/message38.html

            f38 protocol (flag, newState, data) = if flag == 0
                then (modem newState, multipledraw data)
                else interact protocol (modem newState) (send data)
        */
        static SPtr<Term> F38 = binaryOp
        (
            "f38",
            [](SPtr<Term> protocol, SPtr<Term> args) -> SPtr<Term>
            {
                std::vector<SPtr<Term>> vecArgs = list::toVector(args);
                ASSERT(vecArgs.size() == 3, "F38 expects exactly 3 arguments");

                NumberValue flag = getNumber(vecArgs[0])->get();
                SPtr<Term> newState = vecArgs[1];
                SPtr<Term> data = vecArgs[2];

                if (flag == 0)
                {
                    return app(app(CONS(), newState), app(app(CONS(), app(MULTIPLEDRAW(), data)), NIL()));
                }
                else
                {
                    return app(app(app(INTERACT(), protocol), newState), app(SEND(), data));
                }
            }
        );

        return F38;
    }
}
