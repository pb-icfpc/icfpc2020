#include "Numeric.h"

#include "Combinators.h"
#include "Number.h"

#include "framework/Assert.h"

namespace lang::constants
{
    SPtr<Term> ADD()
    {
        static const SPtr<Term> ADD = binaryOp("add", [](SPtr<Term> a, SPtr<Term> b)
        {
            return makeNumber(getNumber(a)->get() + getNumber(b)->get());
        });

        return ADD;
    }

    SPtr<Term> DIV()
    {
        static const SPtr<Term> DIV = binaryOp("div", [](SPtr<Term> a, SPtr<Term> b)
        {
            return makeNumber(getNumber(a)->get() / getNumber(b)->get());
        });

        return DIV;
    }

    SPtr<Term> MUL()
    {
        static const SPtr<Term> MUL = binaryOp("mul", [](SPtr<Term> a, SPtr<Term> b)
        {
            return makeNumber(getNumber(a)->get() * getNumber(b)->get());
        });

        return MUL;
    }

    SPtr<Term> NEGATE()
    {
        static const SPtr<Term> NEG = unaryOp
        (
            "neg",
            [](SPtr<Term> term) -> SPtr<Term>
            {
                return makeNumber(-getNumber(std::move(term))->get());
            }
        );

        return NEG;
    }

    SPtr<Term> PWR2()
    {
        static const SPtr<Term> PWR2 = unaryOp
        (
            "pwr2",
            [](SPtr<Term> term) -> SPtr<Term>
            {
                return makeNumber(pow2(getNumber(std::move(term))->get().get_ui()));
            }
        );

        return PWR2;
    }

    SPtr<Term> INC()
    {
        static const SPtr<Term> INC = unaryOp
        (
            "inc",
            [](SPtr<Term> term) -> SPtr<Term>
            {
                return makeNumber(1 + getNumber(std::move(term))->get());
            }
        );

        return INC;
    }

    SPtr<Term> DEC()
    {
        static const SPtr<Term> DEC = unaryOp
        (
            "dec",
            [](SPtr<Term> term) -> SPtr<Term>
            {
                return makeNumber(-1 + getNumber(std::move(term))->get());
            }
        );

        return DEC;
    }
}
