#include "Number.h"

#include "framework/Assert.h"

namespace lang
{
    Number::Number(const std::string& value)
        : mValue{value}
    {
    }

    Number::Number(NumberValue value)
        : mValue{value}
    {
    }

    SPtr<Term> Number::apply(SPtr<Term>) const
    {
        ASSERT(false, "Should not apply number to anything.");
    }

    bool Number::isAbstraction() const
    {
        return false;
    }

    std::string Number::toString() const
    {
        return mValue.get_str();
    }

    NumberValue Number::get() const
    {
        return mValue;
    }

    SPtr<Term> makeNumber(NumberValue value)
    {
        return std::make_shared<Term>(std::make_shared<Number>(value));
    }

    SPtr<Number> getNumber(SPtr<Term> term)
    {
        std::optional<SPtr<Number>> number = tryGetNumber(term);

        if (!number.has_value())
        {
            throw std::runtime_error(std::string("Expected number, got ") + toString(*term) + " instead");
        }

        return number.value();
    }

    std::optional<SPtr<Number>> tryGetNumber(SPtr<Term> term)
    {
        headEval(term);

        if (Const* c = std::get_if<Const>(term.get()))
        {
            if (SPtr<Number> number = std::dynamic_pointer_cast<Number>(*c))
            {
                return number;
            }
        }

        return std::nullopt;
    }

    bool isNumber(SPtr<Term> term)
    {
        return tryGetNumber(std::move(term)).has_value();
    }

    NumberValue Number::extract() &&
    {
        return std::move(mValue);
    }
}
