#include "List.h"
#include "Bool.h"
#include "Combinators.h"
#include "lang/ListUtils.h"

namespace lang::constants
{
    SPtr<Term> CONS()
    {
        static SPtr<Term> CONS = ternaryOp
        (
            "cons",
            [](SPtr<Term> head, SPtr<Term> tail, SPtr<Term> extractor)
            {
                return app(app(extractor, head), tail);
            }
        );

        return CONS;
    }

    SPtr<Term> VEC()
    {
        return CONS();
    }

    SPtr<Term> CAR()
    {
        static SPtr<Term> CAR = namedCombinator("car", LAMBDA(list, app(list, TRUE())));
        return CAR;
    }

    SPtr<Term> CDR()
    {
        static SPtr<Term> CDR = namedCombinator("cdr", LAMBDA(list, app(list, FALSE())));
        return CDR;
    }

    SPtr<Term> HEAD()
    {
        return CAR();
    }

    SPtr<Term> TAIL()
    {
        return CDR();
    }

    SPtr<Term> NIL()
    {
        static SPtr<Term> NIL = namedCombinator("nil", LAMBDA(x, TRUE()));
        return NIL;
    }

    SPtr<Term> ISNIL()
    {
        static SPtr<Term> IS_NIL = namedCombinator("isnil", LAMBDA(list, app(list, LAMBDA(a, LAMBDA(b, FALSE())))));
        return IS_NIL;
    }

    SPtr<Term> MAP()
    {
        static SPtr<Term> MAP_OP = binaryOp
        (
            "map",
            [](SPtr<Term> f, SPtr<Term> list) -> SPtr<Term>
            {
                if (list::isNil(list))
                {
                    return NIL();
                }
                else
                {
                    return app(app(CONS(), app(f, app(HEAD(), list))), app(app(MAP(), f), app(TAIL(), list)));
                }
            }
        );

        return MAP_OP;
    }
}
