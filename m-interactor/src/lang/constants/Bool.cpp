#include "Bool.h"
#include "Number.h"
#include "Combinators.h"

namespace lang::constants
{
    SPtr<Term> TRUE()
    {
        return FIRST();
    }

    SPtr<Term> FALSE()
    {
        static SPtr<Term> FALSE = binaryOp
        (
            "f",
            [](SPtr<Term> x, SPtr<Term> y)
            {
                return y;
            }
        );
        return FALSE;
    }

    bool isTrue(SPtr<Term> term)
    {
        headEval(term);

        if (Const* c = std::get_if<Const>(term.get()))
        {
            return *c == std::get<Const>(*TRUE());
        }

        return false;
    }

    SPtr<Term> EQ()
    {
        static const SPtr<Term> EQ = binaryOp("eq", [](SPtr<Term> a, SPtr<Term> b)
        {
            return (getNumber(a)->get() == getNumber(b)->get()) ? TRUE() : FALSE();
        });

        return EQ;
    }

    SPtr<Term> IF0()
    {
        static const SPtr<Term> IF0 = unaryOp("if0", [](SPtr<Term> x)
        {
            return (getNumber(x)->get() == 0) ? TRUE() : FALSE();
        });

        return IF0;
    }

    SPtr<Term> LT()
    {
        static const SPtr<Term> LT = binaryOp("lt", [](SPtr<Term> a, SPtr<Term> b)
        {
            return (getNumber(a)->get() < getNumber(b)->get()) ? TRUE() : FALSE();
        });

        return LT;
    }
}
