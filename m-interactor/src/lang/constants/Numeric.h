#pragma once

#include "lang/UntypedLambda.h"

namespace lang::constants
{
    SPtr<Term> INC();
    SPtr<Term> DEC();
    SPtr<Term> ADD();
    SPtr<Term> DIV();
    SPtr<Term> MUL();
    SPtr<Term> NEGATE();
    SPtr<Term> PWR2();
}
