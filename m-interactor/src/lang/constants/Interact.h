#pragma once

#include "lang/UntypedLambda.h"

namespace lang::constants
{
    SPtr<Term> SEND();
    SPtr<Term> MOD();
    SPtr<Term> DEMOD();

    SPtr<Term> DRAW();
    SPtr<Term> MULTIPLEDRAW();
    SPtr<Term> STATELESSDRAW();
    SPtr<Term> STATEFULDRAW();

    SPtr<Term> INTERACT();
    SPtr<Term> F38();
}
