#include "Wrapper.h"

#include "constants/Constants.h"

#include "framework/Assert.h"

namespace lang
{
    int64_t getIntChecked(SPtr<Term> term)
    {
        Opt<SPtr<Number>> number = ::lang::tryGetNumber(term);

        ASSERT(number, "Not a number.");

        return number.value()->get().get_si();
    }

    BigInt getBigIntChecked(SPtr<Term> term)
    {
        Opt<SPtr<Number>> number = ::lang::tryGetNumber(term);

        ASSERT(number, "Not a number.");

        return number.value()->get();
    }
}
