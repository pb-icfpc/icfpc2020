#include "Lexer.h"

#include "tokenToLexemeKind.h"

namespace lang
{
    Lexer::Lexer(Stream& in)
        : mIn{in}
    {
    }

    bool Lexer::hasNext()
    {
        return mIn.get() and not mIn.get().eof();
    }

    Lexeme Lexer::nextLexeme()
    {
        if (not hasNext())
        {
            return {.kind = LexemeKind::END, .token = {}};
        }

        std::string token;
        mIn.get() >> token;

        while (mIn.get().peek() == '\n')
        {
            mIn.get().get();
        }

        return {.kind = tokenToLexemeKind(token), .token = std::move(token)};
    }
}
