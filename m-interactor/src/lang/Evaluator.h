#pragma once

#include "framework/Assert.h"
#include "framework/Aliases.h"
#include "UntypedLambda.h"

#include <vector>
#include <istream>


namespace lang::file_evaluator
{
    SPtr<Term> evaluateFile(const std::string& path);
    SPtr<Term> evaluateBlock(const std::string& rawCode);
    SPtr<Term> evaluateStream(std::istream& stream);
}
