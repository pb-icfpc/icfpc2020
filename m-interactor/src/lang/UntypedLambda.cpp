#include "UntypedLambda.h"

#include <sstream>
#include <unordered_set>

namespace lang
{
    std::ostream& operator<<(std::ostream& out, const Term& term)
    {
        if (const Abstraction* abstraction = std::get_if<Abstraction>(&term))
        {
            return out << "λ" << *abstraction->variable << "." << *abstraction->body;
        }
        else if (const Application* application = std::get_if<Application>(&term))
        {
            if (std::holds_alternative<Abstraction>(*application->func))
            {
                out << '(' << *application->func << ")";
            }
            else
            {
                out << *application->func;
            }

            out << ' ';

            if (std::holds_alternative<Variable>(*application->arg) or std::holds_alternative<Const>(*application->arg))
            {
                out << *application->arg;
            }
            else
            {
                out << "(" << *application->arg << ')';
            }

            return out;
        }
        else if (const Variable* variable = std::get_if<Variable>(&term))
        {
            return out << variable->name;
        }
        else if (const Const* c = std::get_if<Const>(&term))
        {
            return out << (*c)->toString();
        }

        throw std::logic_error("");
    }

    std::string toString(const Term& term)
    {
        std::ostringstream out;
        out << term;
        return out.str();
    }

    Abstraction::Abstraction(SPtr<Term> variable, SPtr<Term> body)
        : variable(std::move(variable))
        , body(std::move(body))
    {
        assert(this->variable != nullptr and this->body != nullptr);
        assert(std::holds_alternative<Variable>(*this->variable));
    }

    SPtr<Term> app(SPtr<Term> func, SPtr<Term> arg)
    {
        return std::make_shared<Term>(Application{ .func=std::move(func), .arg=std::move(arg) });
    }

    SPtr<Term> var(std::string name)
    {
        return std::make_shared<Term>(Variable { .name=std::move(name) });
    }

    SPtr<Term> substitute(SPtr<Term> term, SPtr<Term> variable, SPtr<Term> value)
    {
        if (const Abstraction* abstraction = std::get_if<Abstraction>(term.get()))
        {
            if (abstraction->variable != variable)
            {
                SPtr<Term> new_body = substitute(abstraction->body, std::move(variable), std::move(value));

                if (new_body == abstraction->body) // optimization
                {
                    return term;
                }

                return std::make_shared<Term>(Abstraction(abstraction->variable, std::move(new_body)));
            }
        }
        else if (const Application* application = std::get_if<Application>(term.get()))
        {
            SPtr<Term> new_func = substitute(application->func, variable, value);
            SPtr<Term> new_arg = substitute(application->arg, std::move(variable), std::move(value));

            if (new_func == application->func and new_arg == application->arg) // optimization
            {
                return term;
            }

            return app(std::move(new_func), std::move(new_arg));
        }
        else if (const Variable* term_variable = std::get_if<Variable>(term.get()))
        {
            if (term_variable == &std::get<Variable>(*variable))
            {
                return value;
            }

            return term;
        }

        assert(nullptr != std::get_if<Const>(term.get()));
        return term;
    }

    void substituteMulti(SPtr<Term> term, std::unordered_map<SPtr<Term>, SPtr<Term>>& variableMap)
    {
        if (const Abstraction* abstraction = std::get_if<Abstraction>(term.get()))
        {
            auto nodeHandle = variableMap.extract(abstraction->variable);

            if (variableMap.size() > 0)
            {
                substituteMulti(abstraction->body, variableMap);
            }

            variableMap.insert(std::move(nodeHandle));
        }
        else if (const Application* application = std::get_if<Application>(term.get()))
        {
            substituteMulti(application->func, variableMap);
            substituteMulti(application->arg, variableMap);
        }
        else if (const Variable* termVariable = std::get_if<Variable>(term.get()))
        {
            if (variableMap.count(term) > 0)
            {
                *term = *variableMap.at(term);
            }
        }
    }

    bool isHeadReducible(const SPtr<Term>& term)
    {
        if (const Application* application = std::get_if<Application>(term.get()))
        {
            if (const Abstraction* abstraction = std::get_if<Abstraction>(application->func.get()))
            {
                return true;
            }
            else if (const Const* c = std::get_if<Const>(application->func.get()))
            {
                return (*c)->isAbstraction();
            }
        }

        return false;
    }

    bool betaReduceHead(SPtr<Term> term)
    {
        if (const Application* application = std::get_if<Application>(term.get()))
        {
            headEval(application->func);

            if (const Abstraction* abstraction = std::get_if<Abstraction>(application->func.get()))
            {
                *term = *substitute(abstraction->body, abstraction->variable, application->arg);
                return true;
            }
            else if (const Const* c = std::get_if<Const>(application->func.get()))
            {
                if ((*c)->isAbstraction())
                {
                    *term = *(*c)->apply(application->arg);
                    return true;
                }
            }
        }

        return false;
    }

    void fillFreeVariablesTable
    (
        SPtr<Term> term,
        std::unordered_map<std::string, SPtr<Term>>& freeVariables,
        std::unordered_set<SPtr<Term>>& boundVariables
    )
    {
        if (const Abstraction* abstraction = std::get_if<Abstraction>(term.get()))
        {
            if (boundVariables.count(abstraction->variable) != 0)
            {
                fillFreeVariablesTable(abstraction->body, freeVariables, boundVariables);
            }
            else
            {
                boundVariables.insert(abstraction->variable);
                fillFreeVariablesTable(abstraction->body, freeVariables, boundVariables);
                boundVariables.erase(abstraction->variable);
            }
        }
        else if (const Application* application = std::get_if<Application>(term.get()))
        {
            fillFreeVariablesTable(application->func, freeVariables, boundVariables);
            fillFreeVariablesTable(application->arg, freeVariables, boundVariables);
        }
        else if (const Variable* variable = std::get_if<Variable>(term.get()))
        {
            if (boundVariables.count(term) == 0)
            {
                if (freeVariables.count(variable->name) != 0)
                {
                    assert(freeVariables[variable->name] == term);
                }
                else
                {
                    freeVariables[variable->name] = term;
                }
            }
        }
        else if (const Const* c = std::get_if<Const>(term.get()))
        {
            return;
        }
        else
        {
            throw std::logic_error("Invalid Term state");
        }
    }

    NameToVariable getFreeVariables(SPtr<Term> term)
    {
        NameToVariable freeVariables;
        std::unordered_set<SPtr<Term>> boundVariables;
        fillFreeVariablesTable(term, freeVariables, boundVariables);
        return freeVariables;
    }

    SPtr<Term> assign(SPtr<Term> term, NameToTermMap variableAssignments)
    {
        NameToVariable freeVariables;
        std::unordered_set<SPtr<Term>> boundVariables;
        fillFreeVariablesTable(term, freeVariables, boundVariables);

        for (auto [name, variableTerm] : variableAssignments)
        {
            assert(boundVariables.empty());
            fillFreeVariablesTable(variableTerm, freeVariables, boundVariables);
        }

        std::unordered_map<SPtr<Term>, SPtr<Term>> variableMap;


        for (const auto& [name, value] : variableAssignments)
        {
            if (freeVariables.count(name) > 0)
            {
                assert(variableMap.count(freeVariables.at(name)) == 0);
                variableMap.insert({ freeVariables.at(name), value });
            }
        }

        for (auto& [var, value] : variableMap)
        {
            *var = *value;
        }

        return term;
    }

    SPtr<Term> headEval(SPtr<Term> term)
    {
        while (betaReduceHead(term))
        {
        }
        return term;
    }

    SPtr<Term> fullEval(SPtr<Term> term)
    {
        throw std::runtime_error("Not implemented yet");
    }
}
