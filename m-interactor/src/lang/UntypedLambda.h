#pragma once

#include "framework/Aliases.h"

#include <cassert>
#include <memory>
#include <string>
#include <variant>
#include <unordered_map>
#include <iosfwd>

namespace lang
{
    struct Abstraction;
    struct Application;
    struct Variable;
    class ConstI;
    using Const = SPtr<ConstI>;

    using Term = std::variant
    <
        Abstraction,
        Application,
        Variable,
        Const
    >;

    std::ostream& operator<<(std::ostream&, const Term&);
    std::string toString(const Term& term);

    struct Variable final
    {
        std::string name; // Note: name doesn't affect semantics
    };

    struct Abstraction final
    {
        Abstraction(SPtr<Term> variable, SPtr<Term> body);

        template <typename F>
        Abstraction(std::string variable_name, F&& bodyFunc)
            : variable(std::make_shared<Term>(Variable { .name=std::move(variable_name) }))
            , body(bodyFunc(variable))
        { }

        SPtr<Term> variable;
        SPtr<Term> body;
    };

    struct Application final
    {
        SPtr<Term> func;
        SPtr<Term> arg;
    };

    // Note, that constants do not contain free variables.
    class ConstI
    {
    public:
        virtual ~ConstI() = default;

        virtual SPtr<Term> apply(SPtr<Term>) const = 0;
        virtual bool isAbstraction() const = 0;
        virtual std::string toString() const = 0;
    };

    #define LAMBDA(var, body)                                                           \
        std::make_shared<::lang::Term>                                                  \
        (                                                                               \
            ::lang::Abstraction(#var, [&](SPtr<::lang::Term> var) { return (body); } )  \
        )

    SPtr<Term> app(SPtr<Term> func, SPtr<Term> arg);
    SPtr<Term> var(std::string name);

    SPtr<Term> substitute(SPtr<Term> term, SPtr<Term> variable, SPtr<Term> value);
    bool isHeadReducible(const SPtr<Term>& term);
    bool betaReduceHead(SPtr<Term> term);

    using NameToTermMap = std::unordered_map<std::string, SPtr<Term>>;
    using NameToVariable = NameToTermMap;

    NameToVariable getFreeVariables(SPtr<Term> term);

    // Modifies variable assignments and term
    SPtr<Term> assign(SPtr<Term> term, NameToTermMap variableAssignments);

    // Changes term in place. Returns same term for convinience.
    SPtr<Term> headEval(SPtr<Term> term);
    SPtr<Term> fullEval(SPtr<Term> term);
}
