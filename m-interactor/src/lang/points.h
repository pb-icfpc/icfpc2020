#pragma once

#include "framework/Points.h"

#include "UntypedLambda.h"

namespace lang
{
    Points points(SPtr<Term> term);
}
