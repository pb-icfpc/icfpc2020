#pragma once

#include "framework/Table.h"

#include <vector>
#include <limits>
#include <iostream>

void drawGrid(Table& table);
Table createNumber(size_t gridSize, bool negative);
void setNumberBit(Table& table, size_t powerOfTwo, char value);
Table numberToImage(int value);
