#include "Logger.h"

#include <iostream>

namespace logger
{
    void log(const std::string& message)
    {
        std::cout << message << std::endl;
    }
}
