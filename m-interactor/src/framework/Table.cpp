#include "Table.h"

#include <vector>
#include <ostream>

Table::Table(size_t rows, size_t columns)
    : data(rows * columns, 0)
    , columns(columns)
    , rows(rows)
{
}

char& Table::get(size_t i, size_t j)
{
    return data[columns * i + j];
}

const char& Table::get(size_t i, size_t j) const
{
    return data[columns * i + j];
}

std::ostream& operator<<(std::ostream& stream, const Table& table)
{
    for (size_t i = 0; i < table.rows; ++i)
    {
        for (size_t j = 0; j < table.columns; ++j)
        {
            if (table.get(i, j) == 0)
            {
                stream << '0';
            }
            else
            {
                stream << '1';
            }
        }
        stream << '\n';
    }

    return stream;
}
