#pragma once

#include <string>

namespace logger
{
    void log(const std::string& message);
}

#define LOG(message) logger::log(std::string{#message} + ": " + message)
