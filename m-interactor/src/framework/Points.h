#pragma once

#include <vector>
#include <utility>

using Point = std::pair<int64_t, int64_t>;
using Points = std::vector<Point>;
