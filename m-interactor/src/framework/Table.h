#pragma once

#include <vector>
#include <ostream>

struct Table
{
    std::vector<char> data;
    size_t columns;
    size_t rows;

    Table(size_t rows, size_t columns);

    char& get(size_t i, size_t j);

    const char& get(size_t i, size_t j) const;
};

std::ostream& operator<<(std::ostream& stream, const Table& table);
