#include "BigInt.h"

BigInt pow2(const unsigned long int exp)
{
    BigInt result = 0;
    mpz_ui_pow_ui(result.get_mpz_t(), 2, exp);
    return result;
}
