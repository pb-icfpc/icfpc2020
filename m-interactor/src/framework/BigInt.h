#pragma once

#include <gmpxx.h>

using BigInt = mpz_class;

BigInt pow2(unsigned long int exp);
