#pragma once

#include <stdexcept>

#define ASSERT(expr, msg) if (not (expr)) throw std::logic_error(msg)
