#pragma once

#include <optional>
#include <functional>
#include <memory>

template <typename T>
using Opt = std::optional<T>;

template <typename T>
using Ref = std::reference_wrapper<T>;

template <typename T>
using OptRef = Opt<Ref<T>>;

template <typename T>
using UPtr = std::unique_ptr<T>;

template <typename T>
using OptUPtr = Opt<UPtr<T>>;

template <typename T>
using SPtr = std::shared_ptr<T>;

template <typename T>
using OptSPtr = Opt<SPtr<T>>;
