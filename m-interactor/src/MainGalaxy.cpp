#include "viz/ge/drawer/Drawer.h"
#include "lang/constants/Constants.h"
#include "lang/ListUtils.h"
#include "lang/Evaluator.h"
#include "lang/toStringGalaxy.h"

#include <iostream>

using namespace lang;
using namespace lang::constants;

SPtr<Term> makeList(std::vector<SPtr<Term>> terms)
{
    SPtr<Term> result = NIL();

    for (auto it = terms.rbegin(); it != terms.rend(); ++it)
    {
        result = app(app(CONS(), *it), result);
    }

    return result;
}

int main(int argc, char* argv[])
{
    using namespace lang::constants;
    using namespace lang;

    ASSERT(argc >= 2, "Not enough arguments. Usage: galaxyui <galaxy-file> [init-state]");
    const std::string galaxyFile(argv[1]);

    SPtr<Term> state = NIL();
    if (argc == 3)
    {
        state = file_evaluator::evaluateBlock("galaxy = " + std::string(argv[2]));
    }

    std::string term = "nil";

    const auto vec = [](int x, int y)
    {
        return app(app(VEC(), makeNumber(x)), makeNumber(y));
    };

    SPtr<Term> galaxy = file_evaluator::evaluateFile(galaxyFile);
    const SPtr<Term> program = app(INTERACT(), galaxy);

    auto input = vec(0, 0);
    std::vector<Point> corners;

    SPtr<Term> prevPrevState = NIL();
    SPtr<Term> prevState = NIL();
    while (true)
    {
        #ifdef WITH_VIZ
            const viz::Drawer& drawer = viz::getDrawerSingleton();
            drawer.clear();
        #endif
        std::vector<SPtr<Term>> result = list::toVector(app(app(program, state), input));
        ASSERT(result.size() == 2, "Expected list of two elements: state and pciture");
        prevPrevState = prevState;
        prevState = state;
        state = result[0];
        list::toVector(result[1]); // Force evaluation
        std::cout << lang::toStringGalaxy(state) << std::endl;

        #ifdef WITH_VIZ
            auto point = drawer.drawAndWaitClick({});
            if (point == drawer.getBottomLeftCorner())
            {
                std::cerr << "using previous state\n";
                state = prevPrevState;
                input = vec(10000, 10000);
            }
            else
            {
                input = vec(point.first, point.second);
            }
        #else
            break;
        #endif
    }

    return 0;
}
