#include "json_example.h"

#include "third_party/meta_stuff/Meta.h"
#include "third_party/meta_stuff/JsonCast.h"

#include <iostream>
#include <iomanip>

struct MovieInfo
{
    std::string name;
    float rating;
};

struct Person
{
    int age;
    std::string name;
    float salary;
    std::unordered_map<std::string, std::vector<MovieInfo>> favouriteMovies;
};

namespace meta
{

template <>
inline auto registerMembers<MovieInfo>()
{
    return members(
        member("name", &MovieInfo::name),
        member("rating", &MovieInfo::rating)
    );
}

template <>
inline auto registerMembers<Person>()
{
    return members
    (
        member("age", &Person::age),
        member("name", &Person::name),
        member("salary", &Person::salary),
        member("favouriteMovies", &Person::favouriteMovies)
    );
}

}

void json_example()
{
    std::string str;
    // write
    {
        Person person;
        person.age = 25;
        person.salary = 3.50f;
        person.name = "Alex";

        person.favouriteMovies["Nostalgia Critic"] = { MovieInfo{ "The Room", 8.5f } };
        person.favouriteMovies["John Tron"] =
        {
            MovieInfo{ "Goosebumps", 10.0f }
            , MovieInfo{ "Talking Cat", 9.0f }
        };

        json root = person;
        std::cout << std::setw(4) << root << std::endl;
    }
    // read
    {
        std::string_view str
        {
            R"(
            {
                "age": 25,
                "favouriteMovies": {
                    "John Tron": [
                        {
                            "name": "Goosebumps",
                            "rating": 10.0
                        },
                        {
                            "name": "Talking Cat",
                            "rating": 9.0
                        }
                    ],
                    "Nostalgia Critic": [
                        {
                            "name": "The Room",
                            "rating": 8.5
                        }
                    ]
                },
                "name": "Alex",
                "salary": 3.5
            }
            )"
        };
        json root = json::parse(str);
        Person person = root.get<Person>();
        std::cout
            << person.age << " "
            << person.name << " "
            << person.salary << std::endl;
    }
}
