#!/usr/bin/env bash

#TODO: need some shell magic to ensure workdir is set to script's location

docker build \
    -t pggbnk.icfpc2020.localenv:latest \
    -f d-dockerfiles/localenv/Dockerfile \
    .
