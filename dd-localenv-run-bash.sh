#!/usr/bin/env bash

# needs absolute path
export CHECKOUT_ROOT_SRC="./REPLACE_ME_WITH_ABSOLUTE_CHECKOUT_DIR"
export BUILD_ROOT_VOLUME="pggbnk.icfpc2020.localenv.build"

export GRADLE_CACHE_VOLUME="pggbnk.icfpc2020.localenv.gradlecache"
export GRADLE_CACHE="/root/.gradle"

export ICFPC_ROOT="/pggbnk.icfpc2020"
export ICFPC_CHECKOUT_ROOT="${ICFPC_ROOT}/checkout-root"
export ICFPC_BUILD_ROOT="${ICFPC_ROOT}/build-root"

docker run -it --rm \
    --mount type=bind,src=${CHECKOUT_ROOT_SRC},dst=${ICFPC_CHECKOUT_ROOT} \
    --mount type=volume,src=${BUILD_ROOT_VOLUME},dst=${ICFPC_BUILD_ROOT} \
    --mount type=volume,src=${GRADLE_CACHE_VOLUME},dst=${GRADLE_CACHE} \
    --entrypoint /bin/bash \
    pggbnk.icfpc2020.localenv:latest \
    -c "cd ${ICFPC_ROOT}; exec /bin/bash"
